<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;
use App\Kategory;
use App\Yorum;

class Video extends Model
{
    use Resizable;

    public function comments()
    {
        return Yorum::where('video_id', $this->id)->where('onaylandi', 1)->get();
    }

    public function url()
    {
    	return url("video/" . $this->id . "/" . str_slug($this->name, "-"));
    }

    public function iframe()
    {
        $url = $this->url_embed;

        if (!$url) return false;

        if (strpos($url, "iframe") > -1) {
            $url = preg_replace("@height='(.*?)'@si", "height='400px'", $url);
            $url = preg_replace('@height="(.*?)"@si', "height='400px'", $url);
            return $url;
        }

        return "<iframe src='$url' frameborder='0' height='400px'></iframe>";
    }

    public function kategori()
    {
    	return Kategory::find($this->kategori_id);
    }

    public function name2()
    {
    	return $this->name;
    }

    public function etiketler()
    {
        $etiketler = explode(",", $this->tags);
        $tmp = [];

        foreach ($etiketler as $etiket) {
            $tmp[] = trim($etiket);
        }
        $etiketler = $tmp;
        unset($tmp);

        return $etiketler;
    }
}
