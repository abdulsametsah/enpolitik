<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class Gazete extends Model
{
    use Resizable;

    public function url()
    {
    	return url("gazete/" . $this->slug);
    }
}
