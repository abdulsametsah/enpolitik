<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Kategory extends Model
{
    public function url()
    {
    	return url("haberler/" . $this->slug);
    }
}
