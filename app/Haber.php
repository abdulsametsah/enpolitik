<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

use App\Kategory;
use App\Yorum;

class Haber extends Model
{
	use Resizable;

    public function comments()
    {
        return Yorum::where('haber_id', $this->id)->where('onaylandi', 1)->get();
	}

    public function url()
    {
    	return url("haber/" . $this->id . "/" . str_slug($this->title, "-"));
    }

    public function iframe()
    {
        $url = $this->url_embed;

        if (!$url) return false;

        if (strpos($url, "iframe") > -1) {
            $url = preg_replace("@height='(.*?)'@si", "height='300px'", $url);
            $url = preg_replace('@height="(.*?)"@si', "height='300px'", $url);
            $url = preg_replace('@width="(.*?)"@si', "width='100%'", $url);
            $url = preg_replace('@width="(.*?)"@si', "width='100%'", $url);
            return $url;
        }

        return "<iframe src='$url' frameborder='0' width='100%' height='300px'></iframe>";
    }

    public function kategori()
    {
    	return Kategory::find($this->kategori_id);
    }

    public function titleShort()
    {
    	return $this->title;
    }

    public function image_url()
    {
        return asset($this->image);
    }

    public function slider_url() {
        $name = $this->image;
        $name = explode(".", $name);
        $name = join('-slider.', $name);
        if (!file_exists(public_path($name))) return false;
        return asset($name);
    }

    public function etiketler()
    {
        $etiketler = explode(",", $this->tags);
        $tmp = [];

        foreach ($etiketler as $etiket) {
            $tmp[] = trim($etiket);
        }
        $etiketler = $tmp;
        unset($tmp);

        return $etiketler;
    }
}
