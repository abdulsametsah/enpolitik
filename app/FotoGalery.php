<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

use App\Photo; 
use App\Kategory;
use App\Yorum;

class FotoGalery extends Model
{
	use Resizable;

    public function comments()
    {
        return Yorum::where('galeri_id', $this->id)->where('onaylandi', 1)->get();
    }

    public function kategori()
    {
    	return Kategory::find($this->kategori_id);
    }

    public function url()
    {
    	return url("galeri/" . $this->id . "/" . str_slug($this->name, "-"));
    }

    public function photos()
    {
    	return Photo::where('foto_galeri_id', $this->id)->get();
    }
}
