<?php

namespace App\Http\Controllers;

use App\Haber;
use Gumlet\ImageResize;
use Illuminate\Http\Request;

class HaberController extends Controller
{
    public function deleteVideo(Haber $haber, Request $req)
    {
        $path = public_path($haber->video);
        unlink($path);
        $haber->video = null;
        $haber->save();
        return back()->with([
            'message'    => 'Video silindi.',
            'alert-type' => 'success',
        ]);
    }

    public function create(Request $req)
    {
        $time = time();

        $category_id = $req->category_id;
        $title = $req->title;
        $summary = $req->summary;
        $sehir = $req->sehir;
        $body = $req->body;
        $tags = $req->tags;
        $editor = (isset($req->editor) && $req->editor == 'on') ? true : false;
        $manset = (isset($req->manset) && $req->manset == 'on') ? true : false;
        $video = null;
        $url_embed = $req->url_embed;



        if ($req->hasFile('video')) {
            $video = $req->file("video");
            $video = $video->storeAs('public/haber_videos', $time . "." . $video->getClientOriginalExtension());
            $video = str_replace("public", "storage", $video);
        }

        if (!$category_id || !$title || !$body) {
            return back()->with([
                'message'    => 'Kategori, başlık ve içerik girmek zorunludur.',
                'alert-type' => 'error',
            ]);
        }
        

        if ($req->hasFile('mansetPhoto')) {
            $photo = $req->file('mansetPhoto');
            $photoPath = $photo->storeAs('public/haber/' . date('dMY'), $time . "." . $photo->getClientOriginalExtension());
            //dd($photoPath);
            $realPhotoPath2 = public_path(str_replace("public", "storage", $photoPath));
            $realPhotoPath = public_path(str_replace("public", "storage", $photoPath));
            $data = json_decode($req->mansetData);
            $image = new ImageResize($realPhotoPath);
            $image->freecrop($data->width, $data->height, $data->x, $data->y);
            $realPhotoPath = explode('.', $realPhotoPath);
            $realPhotoPath[0] = $realPhotoPath[0]."-slider";
            $sliderPath = join('.', $realPhotoPath);
            $image->save($sliderPath);
            unlink($realPhotoPath2);
        }

        if ($req->hasFile('photo')) {
            $photo = $req->file('photo');
            $photoPath = $photo->storeAs('public/haber/' . date('dMY'), $time . "." . $photo->getClientOriginalExtension());
            $photoPath = str_replace('public', 'storage', $photoPath);
        }else {
            return back()->with([
                'message'    => 'Fotoğraf zorunlu.',
                'alert-type' => 'error',
            ]);
        }

        try {
            $haber = new Haber;
            $haber->title = $title;
            $haber->summary = $summary;
            $haber->kategori_id = $category_id;
            $haber->body = $body;
            $haber->tags = $tags;
            $haber->image = $photoPath;
            $haber->sehir = $sehir;
            $haber->editor = $editor;
            $haber->manset = $manset;
            $haber->video = $video;
            $haber->url_embed = $url_embed;
            $haber->hit = 0;
            $haber->yorum = 0;
            $haber->save();
            return back()->with([
                'message'    => 'Haber eklendi.',
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            return back()->with([
                'message'    => 'Bir hata oluştu',
                'alert-type' => 'error',
            ]);
        }
    }
    public function update(Request $req)
    {
        $haber = Haber::find($req->id);

        $time = time();

        $category_id = $req->category_id;
        $title = $req->title;
        $summary = $req->summary;
        $sehir = $req->sehir;
        $body = $req->body;
        $tags = $req->tags;
        $editor = (isset($req->editor) && $req->editor == 'on') ? true : false;
        $manset = (isset($req->manset) && $req->manset == 'on') ? true : false;

        $video = null;
        $url_embed = $req->url_embed;

        if ($req->hasFile('video')) {
            $video = $req->file("video");
            $video = $video->storeAs('public/haber_videos', $time . "." . $video->getClientOriginalExtension());
            $video = str_replace("public", "storage", $video);
            $haber->video = $video;
        }

        if (!$category_id || !$title || !$body) {
            return back()->with([
                'message'    => 'Kategori, başlık ve içerik girmek zorunludur.',
                'alert-type' => 'error',
            ]);
        }

        $oldPath = explode('/', $haber->image);
        $oldTime = explode('.', end($oldPath))[0];
        unset($oldPath[count($oldPath) - 1]);
        $oldPath = join('/', $oldPath);
        $oldPath = str_replace("storage", "public", $oldPath);

        if ($req->hasFile('mansetPhoto')) {
            $photo = $req->file('mansetPhoto');
            $photoPath = $photo->storeAs($oldPath, $oldTime . "." . $photo->getClientOriginalExtension());
            //dd($photoPath);
            $realPhotoPath2 = public_path(str_replace("public", "storage", $photoPath));
            $realPhotoPath = public_path(str_replace("public", "storage", $photoPath));
            $data = json_decode($req->mansetData);
            $image = new ImageResize($realPhotoPath);
            $image->freecrop($data->width, $data->height, $data->x, $data->y);
            $realPhotoPath = explode('.', $realPhotoPath);
            $realPhotoPath[0] = $realPhotoPath[0]."-slider";
            $sliderPath = join('.', $realPhotoPath);
            $image->save($sliderPath);
            unlink($realPhotoPath2);
        }

        if ($req->hasFile('photo')) {
            $photo = $req->file('photo');
            $photoPath = $photo->storeAs($oldPath, $oldTime . "." . $photo->getClientOriginalExtension());
            $photoPath = str_replace('public', 'storage', $photoPath);
        }

        try {
            $haber->title = $title;
            $haber->summary = $summary;
            $haber->kategori_id = $category_id;
            $haber->body = $body;
            $haber->tags = $tags;
            $haber->sehir = $sehir;
            $haber->editor = $editor;
            $haber->url_embed = $url_embed;
            $haber->manset = $manset;
            $haber->hit = 0;
            $haber->yorum = 0;
            $haber->save();
            return back()->with([
                'message'    => 'Haber güncellendi.',
                'alert-type' => 'success',
            ]);
        } catch (\Exception $e) {
            return back()->with([
                'message'    => 'Bir hata oluştu',
                'alert-type' => 'error',
            ]);
        }
    }
}
