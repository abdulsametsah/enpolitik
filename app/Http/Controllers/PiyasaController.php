<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PiyasaController extends Controller
{
    public function get()
    {
    	if (Cache::has('piyasa')) return Cache::get('piyasa');

    	$data = file_get_contents("http://api.bigpara.hurriyet.com.tr/doviz/headerlist/anasayfa");
    	$data = json_decode($data);
    	Cache::put('piyasa', $data->data, 60 * 2);
    	return $data->data;
    }
}
