<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use PHPHtmlParser\Dom;

class FutbolController extends Controller
{
    public function getData()
    {
    	$ligId = "482ofyysbdbeoxauk19yg7tdt"; // Süper Lig

    	if (Cache::has('futbol-puan-raw') && Cache::has('futbol-fikstur-raw')) 
    	{
    		$veri = Cache::get('futbol-puan-raw');
    		return iconv("windows-1254", "utf-8", $veri);
    	}

    	$puan = file_get_contents("https://www.sporx.com/_ajax/kategori_puandurumu.php?lig=$ligId");
    	$fikstur = file_get_contents("https://www.sporx.com/_ajax/kategori_fikstur.php?lig=$ligId");

    	Cache::put("futbol-puan-raw", $puan, 60 * 6); // 6 Saat
    	Cache::put("futbol-fikstur-raw", $fikstur, 60 * 6); // 6 Saat

    	return Cache::get('futbol-puan-raw');
    }

    public function getPoints()
    {
    	$getdata = $this->getData();
		$points = iconv("windows-1254", "utf-8", Cache::get('futbol-puan-raw'));
		$points = "<html><body>$points</body></html>";
		$dom = new Dom;
		$dom->loadStr($points, []);

		$puanDurumu = [];
		$i = 0;
		foreach ($dom->find('li') as $takim) {
			if ($i == 0) {$i++; continue;}
			$puanDurumu[] = [
				'takim' => $takim->find('span')[1]->find('a')[0]->text,
				'o' => $takim->find('span')[2]->text,
				'g' => $takim->find('span')[3]->text,
				'b' => $takim->find('span')[4]->text,
				'm' => $takim->find('span')[5]->text,
				'a' => $takim->find('span')[6]->text,
				'y' => $takim->find('span')[7]->text,
				'av' => $takim->find('span')[8]->text,
				'p' => $takim->find('span')[9]->text,
			];
		}
		
		return $puanDurumu;
    }

    public function getFikstur()
    {
    	$getdata = $this->getData();
    	$fikstur = iconv("windows-1254", "utf-8", Cache::get('futbol-fikstur-raw'));

    	$maclar = [];

    	preg_match_all('@<li class="(.*?)">(.*?)</li>@si', $fikstur, $li_list);

        $lastDate = null;

        for ($i=0; $i < count($li_list[1]); $i++) { 
            if ($li_list[1][$i] == "fsdate") {
                $lastDate = $li_list[2][$i];
            }else {
                preg_match_all('@<li class="fsmatch">\n<span class="fsteam"><a href="/futbol-(.*?)" target="_blank">(.*?)</a></span>\n<span class="(.*?)">(.*?)</span>\n<span class="fsteam"><a href="/futbol-(.*?)" target="_blank">(.*?)</a></span>\n</li>@si', $li_list[0][$i], $matches);

                $maclar[$lastDate][] = [
                    'home' => $matches[2][0],
                    'away' => $matches[6][0],
                    'date' => $matches[4][0],
                ];
            }
        }
    	return $maclar;
    }
}
