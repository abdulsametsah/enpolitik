<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    function data ($data, Request $req) {
        $text = $data . ",tr";

        $w = file_get_contents("https://api.openweathermap.org/data/2.5/weather?q=".
                    $text ."&appid=" . env('WEATHER_API_KEY')."&units=metric&lang=tr");
        $w = json_decode($w);

        $sonuc = [];
        $sonuc['Id'] = "18";
        $sonuc['Temp'] = $w->main->temp;
        $sonuc['Description'] = $w->weather[0]->description;
        $sonuc['Pressure'] = $w->main->pressure;
        $sonuc['Humidity'] = $w->main->humidity;
        $sonuc['TempMin'] = $w->main->temp_min;
        $sonuc['TempMax'] = $w->main->temp_max;
        $sonuc['Icon'] = $w->weather[0]->icon;
        $sonuc['Code'] = $w->cod;
        $sonuc['UpdateDate'] = "2019-03-08T04:09:44.5349598+02:00";
        $sonuc['Province'] = [
            'Id' => 18,
            'ProvinceName' => $w->name
        ];

        return json_encode($sonuc);
    }
}
