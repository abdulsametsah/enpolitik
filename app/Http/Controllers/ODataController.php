<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Kategory;
use App\Haber;
use App\CornerPost;
use App\User;
use App\Sayfa;
use function GuzzleHttp\json_decode;

class ODataController extends Controller
{
    public function Categories() {
        return menu('main', '_json');
    }

    public function getHaber($haber)
    {
        return [
            "id" => $haber->id,
            "title" => $haber->title,
            "summary" => $haber->summary,
            "text" => $haber->body,
            "url" => $haber->id . "/" . Str::slug($haber->title),
            "tags" => $haber->tags,
            "imageUrl" => $haber->image_url(),
            "videoUrl" => null,
            "date" => $haber->created_at,
            "updateDate" => $haber->updated_at,
            "breakingNews" => $haber->manset ? true : false,
            "userSelection" => $haber->editor ? true : false,
            "province" => $haber->sehir,
            "readCount" => $haber->hit,
            "commentCount" => $haber->yorum,
            "reference" => null,
            "status" => 1,
            "type" => 0,
            "categoryId" => $haber->kategori_id,
            "relatedNewsId" => null,
            "userId" => 205,
            "category" => null,
            "comments" => null,
            "headline" => null,
            "user" => null,
            "province" => null,
            "relatedNews" => null,
            "images" => null,
            "updates" => null,
            "urlPrefix" => "haber"
        ];
    }
    
    public function renderHaber($haberler)
    {
        $tmp = [];
        foreach ($haberler as $haber) {
            $tmp[] = $this->getHaber($haber);
        }

        return $tmp;
    }

    public function categoryHaber(Request $req)
    {
        $cat = Kategory::where('name', $req->categoryName)->firstOrFail();

        $haberler = Haber::where('kategori_id', $cat->id)->latest();

        if ($req->count) {
            $count = $req->count;
        }else if ($req->top) {
            $count = $req->top;
        }else {
            $count = 10;
        }

        if ($req->skip) {
            $haberler = $haberler->skip($req->skip);
        }

        $haberler = $haberler->limit($count);
        

        $haberler = $haberler->get();

        

        return $this->renderHaber($haberler);
    }

    public function MostRead(Request $req)
    {
        $haberler = Haber::orderBy('hit', 'desc');

        if ($req->count) {
            $count = $req->count;
        } else if ($req->top) {
            $count = $req->top;
        } else {
            $count = 10;
        }

        if ($req->skip) {
            $haberler = $haberler->skip($req->skip);
        }

        $haberler = $haberler->limit($count);


        $haberler = $haberler->get();



        return $this->renderHaber($haberler);
    }

    public function MostCommented(Request $req)
    {
        $haberler = Haber::orderBy('yorum', 'desc');

        if ($req->count) {
            $count = $req->count;
        } else if ($req->top) {
            $count = $req->top;
        } else {
            $count = 10;
        }

        if ($req->skip) {
            $haberler = $haberler->skip($req->skip);
        }

        $haberler = $haberler->limit($count);


        $haberler = $haberler->get();



        return $this->renderHaber($haberler);
    }

    public function Articles(Request $req)
    {
        $articles = CornerPost::latest();

        if ($req->count) {
            $count = $req->count;
        } else if ($req->top) {
            $count = $req->top;
        } else {
            $count = 10;
        }

        if ($req->skip) {
            $articles = $articles->skip($req->skip);
        }

        $articles = $articles->limit($count);


        $articles = $articles->get();

        return $this->renderArticles($articles);
    }

    public function manset(Request $req)
    {
        $page = str_replace("api/Manset", null, $req->path());

        $haberler = Haber::where('manset', 1)->latest()->skip(($page-1)*15)->limit(15)->get();
        $mansetler = [];

        foreach ($haberler as $haber) {
            $mansetler[] = [
                'id' => $haber->id,
                'title1' => $haber->title,
                'title2' => null,
                'image' => $haber->image_url(),
                'position' => $page,
                'date' => $haber->created_at,
                'news' => $this->getHaber($haber)
            ];
        }

        return $mansetler;
    }

    public function Leagues()
    {
        return [[
            "Id" => 0,
            "name" => "Spor Toto Süper Lig",
            "shownName" => "Spor Toto Süper Lig",
            "isTournament" => false,
            "activeLeagueTournamentId" => 158,
            "logo" => null,
            "updateDate" => "0001-01-01T00:00:00"
        ]];
    }

    public function LeagueStage()
    {
        $futbol = new FutbolController();
        $points = $futbol->getPoints();
        $puanlar = [];
        $i = 1;
        foreach ($points as $p) {
            $p = (object) $p;
            $puanlar[] = [
                "Id" => 0,
                "rank" => $i++,
                "name" => $p->takim,
                "played" => $p->o,
                "wins" => $p->g,
                "draws" => $p->b,
                "defeits" => $p->m,
                "average" => $p->av,
                "points" => $p->p,
                "leagueStageId" => 0,
                "leagueStage" => null
            ];
        }
        
        $fikstur = $futbol->getFikstur();
        $maclar = [];

        foreach ($fikstur as $tarih => $musabakalar) {
            foreach ($musabakalar as $musabaka) {
                $homeScore = explode('-', $musabaka['date'])[0];
                $awayScore = explode('-', $musabaka['date'])[1];

                $maclar[] = [
                    "Id" => 1326111,
                    "name" => $musabaka['home'] . " - " . $musabaka['away'],
                    "HomeTeamName" => $musabaka['home'],
                    "AwayTeamName" => $musabaka['away'],
                    "HomeTeamScore" => is_numeric($homeScore) ? $homeScore:null,
                    "AwayTeamScore" => is_numeric($awayScore) ? $awayScore:null,
                    "status_type" => "notstarted",
                    "startDate" => $tarih,
                    "round" => ceil($puanlar[0]['played']).". Hafta",
                    "leagueStageId" => 0,
                    "leagueStage" => null
                ];
            }
        }

        return [
            'Item1' => $puanlar,
            'Item2' => $maclar
        ];
    }

    public function Weather(Request $req)
    {
        $text = \App\Provinces::where('Id', $req->key)->first()->ProvinceName . ",tr";

        $w = file_get_contents("https://api.openweathermap.org/data/2.5/weather?q=" .
            $text . "&appid=" . env('WEATHER_API_KEY') . "&units=metric&lang=tr");
        $w = json_decode($w);


        $sonuc = [];
        $sonuc['Id'] = "18";
        $sonuc['Temp'] = $w->main->temp;
        $sonuc['Description'] = $w->weather[0]->description;
        $sonuc['Pressure'] = $w->main->pressure;
        $sonuc['Humidity'] = $w->main->humidity;
        $sonuc['TempMin'] = $w->main->temp_min;
        $sonuc['TempMax'] = $w->main->temp_max;
        $sonuc['Icon'] = $w->weather[0]->icon;

        // İcon değişimi.
        $weatherIcons = json_decode('{"200":{"label":"thunderstorm with light rain","icon":"storm-showers"},"201":{"label":"thunderstorm with rain","icon":"storm-showers"},"202":{"label":"thunderstorm with heavy rain","icon":"storm-showers"},"210":{"label":"light thunderstorm","icon":"storm-showers"},"211":{"label":"thunderstorm","icon":"thunderstorm"},"212":{"label":"heavy thunderstorm","icon":"thunderstorm"},"221":{"label":"ragged thunderstorm","icon":"thunderstorm"},"230":{"label":"thunderstorm with light drizzle","icon":"storm-showers"},"231":{"label":"thunderstorm with drizzle","icon":"storm-showers"},"232":{"label":"thunderstorm with heavy drizzle","icon":"storm-showers"},"300":{"label":"light intensity drizzle","icon":"sprinkle"},"301":{"label":"drizzle","icon":"sprinkle"},"302":{"label":"heavy intensity drizzle","icon":"sprinkle"},"310":{"label":"light intensity drizzle rain","icon":"sprinkle"},"311":{"label":"drizzle rain","icon":"sprinkle"},"312":{"label":"heavy intensity drizzle rain","icon":"sprinkle"},"313":{"label":"shower rain and drizzle","icon":"sprinkle"},"314":{"label":"heavy shower rain and drizzle","icon":"sprinkle"},"321":{"label":"shower drizzle","icon":"sprinkle"},"500":{"label":"light rain","icon":"rain"},"501":{"label":"moderate rain","icon":"rain"},"502":{"label":"heavy intensity rain","icon":"rain"},"503":{"label":"very heavy rain","icon":"rain"},"504":{"label":"extreme rain","icon":"rain"},"511":{"label":"freezing rain","icon":"rain-mix"},"520":{"label":"light intensity shower rain","icon":"showers"},"521":{"label":"shower rain","icon":"showers"},"522":{"label":"heavy intensity shower rain","icon":"showers"},"531":{"label":"ragged shower rain","icon":"showers"},"600":{"label":"light snow","icon":"snow"},"601":{"label":"snow","icon":"snow"},"602":{"label":"heavy snow","icon":"snow"},"611":{"label":"sleet","icon":"sleet"},"612":{"label":"shower sleet","icon":"sleet"},"615":{"label":"light rain and snow","icon":"rain-mix"},"616":{"label":"rain and snow","icon":"rain-mix"},"620":{"label":"light shower snow","icon":"rain-mix"},"621":{"label":"shower snow","icon":"rain-mix"},"622":{"label":"heavy shower snow","icon":"rain-mix"},"701":{"label":"mist","icon":"sprinkle"},"711":{"label":"smoke","icon":"smoke"},"721":{"label":"haze","icon":"day-haze"},"731":{"label":"sand, dust whirls","icon":"cloudy-gusts"},"741":{"label":"fog","icon":"fog"},"751":{"label":"sand","icon":"cloudy-gusts"},"761":{"label":"dust","icon":"dust"},"762":{"label":"volcanic ash","icon":"smog"},"771":{"label":"squalls","icon":"day-windy"},"781":{"label":"tornado","icon":"tornado"},"800":{"label":"clear sky","icon":"sunny"},"801":{"label":"few clouds","icon":"cloudy"},"802":{"label":"scattered clouds","icon":"cloudy"},"803":{"label":"broken clouds","icon":"cloudy"},"804":{"label":"overcast clouds","icon":"cloudy"},"900":{"label":"tornado","icon":"tornado"},"901":{"label":"tropical storm","icon":"hurricane"},"902":{"label":"hurricane","icon":"hurricane"},"903":{"label":"cold","icon":"snowflake-cold"},"904":{"label":"hot","icon":"hot"},"905":{"label":"windy","icon":"windy"},"906":{"label":"hail","icon":"hail"},"951":{"label":"calm","icon":"sunny"},"952":{"label":"light breeze","icon":"cloudy-gusts"},"953":{"label":"gentle breeze","icon":"cloudy-gusts"},"954":{"label":"moderate breeze","icon":"cloudy-gusts"},"955":{"label":"fresh breeze","icon":"cloudy-gusts"},"956":{"label":"strong breeze","icon":"cloudy-gusts"},"957":{"label":"high wind, near gale","icon":"cloudy-gusts"},"958":{"label":"gale","icon":"cloudy-gusts"},"959":{"label":"severe gale","icon":"cloudy-gusts"},"960":{"label":"storm","icon":"thunderstorm"},"961":{"label":"violent storm","icon":"thunderstorm"},"962":{"label":"hurricane","icon":"cloudy-gusts"}}', true);
        
        $prefix = 'wi wi-';
        $code = $w->cod;
        $icon = $weatherIcons[$code]['icon'];
        // If we are not in the ranges mentioned above, add a day/night prefix.
        if (!($code > 699 && $code < 800) && !($code > 899 && $code < 1000)) {
            $icon = 'day-' . $icon;
        }
        $icon = $prefix . $icon;
        $sonuc['Icon'] = $icon;

        $sonuc['Code'] = $w->cod;
        $sonuc['UpdateDate'] = "2019-03-08T04:09:44.5349598+02:00";
        $sonuc['Province'] = [
            'Id' => 18,
            'ProvinceName' => $w->name
        ];

        return json_encode($sonuc);
    }
    
    public function Provinces()
    {
        $pros =  \App\Provinces::all();
        $tmp = [];

        foreach ($pros as $p) {
            $tmp[] = [
                'id' => $p->Id,
                'provinceName' => $p->ProvinceName,
                'weather' => null,
                'news' => null,
            ];
        }

        unset($pros);

        return $tmp;
    }

    public function getModel($model, Request $req)
    {
        if ($model == "News") {
            $data = Haber::latest();
        } else if ($model == "Articles") {
            $data = CornerPost::latest();
        } else if ($model == "Authors") {
            $data = User::latest();
        } else if ($model == "Categories") {
            $data = Kategory::latest();
        } else if ($model == "Headlines") {
            $data = Haber::latest()->limit(15);
        } else if ($model == "Pages") {
            $data = Sayfa::latest();
        }

        $params = [];

        foreach ($req->all() as $key => $value) {
            $params[str_replace("$", null, $key)] = $value;
        }


        if (isset($params['skip'])) {
            $data = $data->skip($params['skip']);
        }

        if (isset($params['top'])) {
            $data = $data->limit($params['top']);
        }

        $data = $data->get();

        switch ($model) {
            case 'News':
                $value = $this->renderHaber($data);
                break;

            case 'Articles':
                $value = $this->renderArticles($data);
                break;

            case 'Authors':
                $value = $this->renderUsers($data);
                break;
            
            case 'Categories':
                $value = $this->renderCategories($data);
                break;

            case 'Headlines':
                $value = $this->renderHeadlines($data);
                break;

            case 'Pages':
                $value = $this->renderPages($data);
                break;
            default:
                $value = null;
                break;
        }

        $odata = [
            '@odata.context' => url('odata/$metadata'),
            'value' => $value
        ];

        return $odata;
    }

    public function renderPages($pages)
    {
        $tmp = [];
        foreach($pages as $page){
            $tmp[] = [
                "Id" => $page->id,
                "PageName" => $page->title,
                "Text" => $page->body,
                "Url" => $page->slug,
                "Link" => "/sayfa/".$page->slug,
                "Order" => $page->created_at,
                "TopMenu" => $page->top_menu ? true:false,
                "BottomMenu" => $page->bottom_menu ? true:false,
                "UserId" => 210
            ];
        }
        return $tmp;
    }

    public function renderHeadlines($haberler)
    {
        $tmp = [];
        foreach ($haberler as $haber) {
            $tmp[] = [
                "Id" => $haber->id,
                "Title1" => $haber->title,
                "Title2" => null,
                "Image" => $haber->image_url(),
                "Position" => "Headline1",
                "Date" => $haber->created_at
            ];
        }
        return $tmp;
    }

    public function renderCategories($categories)
    {
        $tmp = [];

        foreach ($categories as $cat) {
            $tmp[] = [
                "Id" => $cat->id,
                "CategoryName" => $cat->name,
                "Description" => null,
                "Url" => $cat->slug,
                "Color" => null,
                "Order" => $cat->sira,
                "ParentCategoryId" => null,
                "TopMenu" => true,
                "BottomMenu" => true,
                "Type" => "Haber"
            ];
        }

        return $tmp;
    }

    public function renderArticles($articles)
    {
        $tmp = [];
        foreach ($articles as $a) {
            $tmp[] = [
                'Id' => $a->id,
                'Title' => $a->title,
                'Text' => $a->body,
                'Date' => $a->created_at,
                'UpdateDate' => null,
                'ReadCount' => 0,
                'Tags' => $a->etiketler,
                'Status' => "Aktif",
                'Url' => $a->slug,
                'UserId' => $a->user_id,
                'User' => [
                    "Name" => $a->yazar()->name,
                    "Bio" => "",
                    "ImageUrl" => $a->yazar()->avatar,
                    "Facebook" => "",
                    "Twitter" => "",
                    "LinkedIn" => "",
                    "Youtube" => "",
                    "Google" => "",
                    "RegistrationDate" => $a->yazar()->created_at,
                    "Gender" => "Bay",
                    "Birthday" => "0001-01-01T00:00:00Z",
                    "Web" => "",
                    "Id" => $a->yazar()->id,
                    "UserName" => $a->yazar()->email,
                    "NormalizedUserName" => null,
                    "Email" => $a->yazar()->email,
                    "NormalizedEmail" => null,
                    "EmailConfirmed" => false,
                    "PasswordHash" => null,
                    "SecurityStamp" => null,
                    "ConcurrencyStamp" => "fb9d9c00-466b-43fb-91b5-c667e412989c",
                    "PhoneNumber" => "",
                    "PhoneNumberConfirmed" => false,
                    "TwoFactorEnabled" => false,
                    "LockoutEnd" => null,
                    "LockoutEnabled" => false,
                    "AccessFailedCount" => 0
                ]
            ];
        }

        return $tmp;
    }

    public function renderUsers($users)
    {
        $tmp = [];

        foreach ($users as $u) {
            $tmp[] = [
                "Name" => $u->name,
                "Bio" => "",
                "ImageUrl" => $u->avatar,
                "Facebook" => "",
                "Twitter" => "",
                "LinkedIn" => "",
                "Youtube" => "",
                "Google" => "",
                "RegistrationDate" => $u->created_at,
                "Gender" => "Bay",
                "Birthday" => "0001-01-01T00:00:00Z",
                "Web" => "",
                "Id" => $u->id,
                "UserName" => $u->email,
                "NormalizedUserName" => null,
                "Email" => $u->email,
                "NormalizedEmail" => null,
                "EmailConfirmed" => false,
                "PasswordHash" => null,
                "SecurityStamp" => null,
                "ConcurrencyStamp" => "fb9d9c00-466b-43fb-91b5-c667e412989c",
                "PhoneNumber" => "",
                "PhoneNumberConfirmed" => false,
                "TwoFactorEnabled" => false,
                "LockoutEnd" => null,
                "LockoutEnabled" => false,
                "AccessFailedCount" => 0
            ];
        }

        return $tmp;
    }
}
