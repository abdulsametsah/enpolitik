<?php

namespace App\Http\Controllers;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Kategory;
use App\Video;
use App\Haber;
use App\CornerPost;
use App\Gazete;
use App\FotoGalery;
use App\Sayfa;
use Illuminate\Support\Facades\Cache;


class PagesController extends Controller
{
    public $is_mobile = false;

    function __construct()
    {
        $agent = new Agent();
        if ($agent->isMobile() or $agent->isTablet())
            $this->is_mobile = true;
    }

    public function home(Request $req)
    {

        if ($req->clearCache == "ok") $clearCache = true;
        else $clearCache = false;
        
        if (Cache::has('sliderHaberler') && !$clearCache)
            $sliderHaberler = Cache::get('sliderHaberler');
        else {
            $sliderHaberler = Haber::where('manset', 1)->orderBy('id', 'desc')->limit(15)->get();
            Cache::put('sliderHaberler', $sliderHaberler, 10);
        }
        

        if (Cache::has('topHaberler') && !$clearCache)
            $topHaberler = Cache::get('topHaberler');
        else {
            $topHaberler = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(5)->get();
            Cache::put('topHaberler', $topHaberler, 10);
        }

        if (Cache::has('sporHaberleri') && !$clearCache)
            $sporHaberleri = Cache::get('sporHaberleri');
        else {
            $sporHaberleri = Haber::where('kategori_id', 4)->orderBy('id', 'desc')->limit(10)->get();
            Cache::put('sporHaberleri', $sporHaberleri, 10);
        }

        if (Cache::has('editorHaberleri') && !$clearCache)
            $editorHaberleri = Cache::get('editorHaberleri');
        else {
            $editorHaberleri = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('editorHaberleri', $editorHaberleri, 10);
        }

        if (Cache::has('ekonomiHaberleri') && !$clearCache)
            $ekonomiHaberleri = Cache::get('ekonomiHaberleri');
        else {
            $ekonomiHaberleri = Haber::where('kategori_id', 1)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('ekonomiHaberleri', $ekonomiHaberleri, 10);
        }

        if (Cache::has('politikaHaberleri') && !$clearCache)
            $politikaHaberleri = Cache::get('politikaHaberleri');
        else {
            $politikaHaberleri = Haber::where('kategori_id', 3)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('politikaHaberleri', $politikaHaberleri, 10);
        }

        if (Cache::has('dunyaHaberleri') && !$clearCache)
            $dunyaHaberleri = Cache::get('dunyaHaberleri');
        else {
            $dunyaHaberleri = Haber::where('kategori_id', 7)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('dunyaHaberleri', $dunyaHaberleri, 10);
        }

        if (Cache::has('yerelHaberler3') && !$clearCache)
            $yerelHaberler3 = Cache::get('yerelHaberler3');
        else {
            $yerelHaberler3 = Haber::whereNotNull('sehir')->latest()->limit(3)->get();
            Cache::put('yerelHaberler3', $yerelHaberler3, 10);
        }

        if (Cache::has('yerelHaberlerDigerleri') && !$clearCache)
            $yerelHaberlerDigerleri = Cache::get('yerelHaberlerDigerleri');
        else {
            $yerelHaberlerDigerleri = Haber::whereNotNull('sehir')->latest()->skip(3)->limit(6)->get();
            Cache::put('yerelHaberlerDigerleri', $yerelHaberlerDigerleri, 10);
        }

        if (Cache::has('cokOkunanHaberler') && !$clearCache)
            $cokOkunanHaberler = Cache::get('cokOkunanHaberler');
        else {
            $cokOkunanHaberler = Haber::orderBy('hit', 'desc')->limit(5)->get();
            Cache::put('cokOkunanHaberler', $cokOkunanHaberler, 240);
        }

        if (Cache::has('cokYorumlananHaberler') && !$clearCache)
            $cokYorumlananHaberler = Cache::get('cokYorumlananHaberler');
        else {
            $cokYorumlananHaberler = Haber::orderBy('yorum', 'desc')->limit(5)->get();
            Cache::put('cokYorumlananHaberler', $cokYorumlananHaberler, 240);
        }

        $fotoGaleriler = FotoGalery::latest()->limit(5)->get();
        $videolar = Video::latest()->limit(6)->get();

        $gazeteler = Gazete::latest()->get();

        $piyasa = new PiyasaController();
        $piyasa = $piyasa->get();
    	
        $viewName = "anasayfa";
        if ($this->is_mobile) $viewName = "mobil.anasayfa";

    	return view($viewName, compact("sliderHaberler", "topHaberler", "sporHaberleri", "editorHaberleri",
                "ekonomiHaberleri", "politikaHaberleri", "dunyaHaberleri",
                "cokOkunanHaberler", "gazeteler", "cokYorumlananHaberler", "fotoGaleriler", 
                "piyasa", "videolar", "yerelHaberler3", "yerelHaberlerDigerleri"));
    }

    public function updateHome()
    {
        $clearCache = true;

        if (Cache::has('sliderHaberler') && !$clearCache)
            $sliderHaberler = Cache::get('sliderHaberler');
        else {
            $sliderHaberler = Haber::where('manset', 1)->orderBy('id', 'desc')->limit(15)->get();
            Cache::put('sliderHaberler', $sliderHaberler, 10);
        }

        if (Cache::has('topHaberler') && !$clearCache)
            $topHaberler = Cache::get('topHaberler');
        else {
            $topHaberler = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(5)->get();
            Cache::put('topHaberler', $topHaberler, 10);
        }

        if (Cache::has('sporHaberleri') && !$clearCache)
            $sporHaberleri = Cache::get('sporHaberleri');
        else {
            $sporHaberleri = Haber::where('kategori_id', 4)->orderBy('id', 'desc')->limit(10)->get();
            Cache::put('sporHaberleri', $sporHaberleri, 10);
        }

        if (Cache::has('editorHaberleri') && !$clearCache)
            $editorHaberleri = Cache::get('editorHaberleri');
        else {
            $editorHaberleri = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('editorHaberleri', $editorHaberleri, 10);
        }

        if (Cache::has('ekonomiHaberleri') && !$clearCache)
            $ekonomiHaberleri = Cache::get('ekonomiHaberleri');
        else {
            $ekonomiHaberleri = Haber::where('kategori_id', 1)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('ekonomiHaberleri', $ekonomiHaberleri, 10);
        }

        if (Cache::has('politikaHaberleri') && !$clearCache)
            $politikaHaberleri = Cache::get('politikaHaberleri');
        else {
            $politikaHaberleri = Haber::where('kategori_id', 3)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('politikaHaberleri', $politikaHaberleri, 10);
        }

        if (Cache::has('dunyaHaberleri') && !$clearCache)
            $dunyaHaberleri = Cache::get('dunyaHaberleri');
        else {
            $dunyaHaberleri = Haber::where('kategori_id', 7)->orderBy('id', 'desc')->limit(6)->get();
            Cache::put('dunyaHaberleri', $dunyaHaberleri, 10);
        }

        if (Cache::has('yerelHaberler3') && !$clearCache)
            $yerelHaberler3 = Cache::get('yerelHaberler3');
        else {
            $yerelHaberler3 = Haber::whereNotNull('sehir')->latest()->limit(3)->get();
            Cache::put('yerelHaberler3', $yerelHaberler3, 10);
        }

        if (Cache::has('yerelHaberlerDigerleri') && !$clearCache)
            $yerelHaberlerDigerleri = Cache::get('yerelHaberlerDigerleri');
        else {
            $yerelHaberlerDigerleri = Haber::whereNotNull('sehir')->latest()->skip(3)->limit(6)->get();
            Cache::put('yerelHaberlerDigerleri', $yerelHaberlerDigerleri, 10);
        }

        if (Cache::has('cokOkunanHaberler') && !$clearCache)
            $cokOkunanHaberler = Cache::get('cokOkunanHaberler');
        else {
            $cokOkunanHaberler = Haber::orderBy('hit', 'desc')->limit(5)->get();
            Cache::put('cokOkunanHaberler', $cokOkunanHaberler, 240);
        }

        if (Cache::has('cokYorumlananHaberler') && !$clearCache)
            $cokYorumlananHaberler = Cache::get('cokYorumlananHaberler');
        else {
            $cokYorumlananHaberler = Haber::orderBy('yorum', 'desc')->limit(5)->get();
            Cache::put('cokYorumlananHaberler', $cokYorumlananHaberler, 240);
        }

        return "Ok";
    }

    public function yazarlar(Request $req)
    {
        $yazarlar = CornerPost::latest()->paginate(20);
        $title = "Yazarlar";

        $viewName = "yazarlar";
        if ($this->is_mobile) $viewName = "mobil.yazarlar";

        return view($viewName, compact("title", "yazarlar"));
    }

    public function koseYazisi($id, $slug, Request $req)
    {
        $kose = CornerPost::find($id);
        $digerKoseYazilari = CornerPost::where('user_id', $kose->yazar()->id)->get();

        $viewName = "kose-yazisi";
        if ($this->is_mobile) $viewName = "mobil.koseYazisi";

        return view($viewName, compact("kose", "digerKoseYazilari"));
    }

    public function galeri($id, $slug, Request $req)
    {
        $p = $req->p ?? 1;
        $p--;

        $galeri = FotoGalery::findOrFail($id);
        $foto = $galeri->photos();

        if (!isset($foto[$p])) abort(404);

        $foto = $foto[$p];

        $sonHaberler = Haber::limit(28)->get();

        $viewName = "galeri";
        if ($this->is_mobile) $viewName = "mobil.galeri";

        return view($viewName, compact("galeri", "foto", "p", "sonHaberler"));
    }

    public function haber($id, $slug, Request $req)
    {
        $haber = Haber::findOrFail($id);
        //$haber->hit++;
        //$haber->save();

        $digerHaberler = Haber::latest()->limit(10)->get();

        $viewName = "haber";
        if ($this->is_mobile) $viewName = "mobil.haber";
        return view($viewName, compact("haber", 'digerHaberler'));
    }

    public function updateHit(Haber $haber)
    {
        $haber->hit++;
        $haber->save();
        return [
            'status' => 'success',
            'newHit' => $haber->hit
        ];
    }

    public function haberler($kategori = false, Request $req)
    {
        $title = "Haberler";
        $haberler = Haber::latest();

        if ($kategori) {
            $kategori = Kategory::where('slug', $kategori)->firstOrFail();
            $haberler = $haberler->where('kategori_id', $kategori->id);
            $title = $kategori->name;
        }

        if ($req->search) {
            $q = $req->search;
            $haberler = $haberler->where('title', "like", "%".$q."%")->orWhere('body', 'like', '%'.$q.'%');
            $title = "Arama Sonuçları: " . $q;
        }

        $viewName = "haberler";
        if ($this->is_mobile) $viewName = "mobil.haberler";

        $haberler = $haberler->paginate(30);
        return view($viewName, compact("haberler", "title"));
    }

    public function videolar($kategori = false, Request $req)
    {
        $title = "Videolar";
        $videolar = Video::latest();

        if ($kategori) {
            $kategori = Kategory::where('slug', $kategori)->firstOrFail();
            $videolar = $videolar->where('kategori_id', $kategori->id);
            $title = $kategori->name;
        }

        if ($req->search) {
            $q = $req->search;
            $videolar = $videolar->where('title', "like", "%".$q."%")->orWhere('body', 'like', '%'.$q.'%');
            $title = "Arama Sonuçları: " . $q;
        }

        $videolar = $videolar->paginate(30);

        $viewName = "videolar";
        if ($this->is_mobile) $viewName = "mobil.videolar";

        return view($viewName, compact("videolar", "title"));
    }

    public function video($id, $slug, Request $req)
    {
        $video = Video::findOrFail($id);

        $sonHaberler = Haber::limit(28)->get();

        $viewName = "video";
        if ($this->is_mobile) $viewName = "mobil.video";

        return view($viewName, compact("video", "sonHaberler"));
    }

    public function galeriler($kategori = false, Request $req)
    {
        $title = "Galeriler";
        $galeriler = FotoGalery::latest();

        if ($kategori) {
            $kategori = Kategory::where('slug', $kategori)->firstOrFail();
            $galeriler = $galeriler->where('kategori_id', $kategori->id);
            $title = $kategori->name;
        }

        if ($req->search) {
            $q = $req->search;
            $galeriler = $galeriler->where('name', "like", "%".$q."%");
            $title = "Arama Sonuçları: " . $q;
        }

        $viewName = "galeriler";
        if ($this->is_mobile) $viewName = "mobil.galeriler";

        $galeriler = $galeriler->paginate(30);
        return view($viewName, compact("galeriler", "title"));
    }

    public function gazeteler()
    {
        $gazeteler = Gazete::all();
        $title = "Gazete Manşetleri";

        return view("gazeteler", compact("gazeteler", "title"));
    }

    public function gazete($slug)
    {
        $gazeteler = Gazete::all();
        $gazete = Gazete::where('slug', $slug)->firstOrFail();
        $title = "Gazete Manşetleri";
        return view("gazete", compact("gazeteler", "gazete", "title"));
    }

    public function sayfa($slug)
    {
        $sayfa = Sayfa::where("slug", $slug)->firstOrFail();

        $viewName = "sayfa";
        if ($this->is_mobile) $viewName = "mobil.sayfa";

        return view($viewName, compact("sayfa"));
    }

    public function etiket($etiket)
    {
        $haberler = Haber::where('tags', 'like', '%'.$etiket.'%')->paginate(30);
        $title = "Etiket: "  . $etiket;


        return view("etiket", compact("haberler", "title"));
    }
}
