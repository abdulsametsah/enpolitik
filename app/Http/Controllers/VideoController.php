<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function store(Request $req)
    {
        if (!$req->title || !$req->body || !$req->tags || !$req->hasFile('photo')) {
            return back()->with([
                'message'    => 'Başlık, içerik ve etiket girmek zorunludur.',
                'alert-type' => 'error',
            ]);
        }
        $video = null;
        $time = time();

        $photo = $req->file('photo');
        $photoPath = $photo->storeAs('public/haber/' . date('dMY'), $time . "." . $photo->getClientOriginalExtension());
        $photoPath = str_replace('public', 'storage', $photoPath);

        if ($req->hasFile('video')) {
            $video = $req->file("video");
            $video = $video->storeAs('public/haber_videos', $time . "." . $video->getClientOriginalExtension());
            $video = str_replace("public", "storage", $video);
        }

        $v = new Video;
        $v->name = $req->title;
        $v->body = $req->body;
        $v->image = $photoPath;
        $v->tags = $req->tags;
        $v->url_embed = $req->url_embed;
        $v->video = $video;
        $v->save();
        return redirect(url('admin/videos'))->with([
            'message'    => 'Eklendi.',
            'alert-type' => 'success',
        ]);

    }
    public function update(Request $req)
    {
        $v = Video::find($req->video_id);

        if (!$req->title || !$req->body || !$req->tags) {
            return back()->with([
                'message'    => 'Başlık, içerik ve etiket girmek zorunludur.',
                'alert-type' => 'error',
            ]);
        }
        $video = null;
        $time = time();

        if ($req->hasFile('photo')) {
            $photo = $req->file('photo');
            $photoPath = $photo->storeAs('public/haber/' . date('dMY'), $time . "." . $photo->getClientOriginalExtension());
            $photoPath = str_replace('public', 'storage', $photoPath);
            $v->image = $photoPath;
        }

        if ($req->hasFile('video')) {
            $video = $req->file("video");
            $video = $video->storeAs('public/haber_videos', $time . "." . $video->getClientOriginalExtension());
            $video = str_replace("public", "storage", $video);
            $v->video = $video;
        }
        $v->name = $req->title;
        $v->body = $req->body;
        $v->tags = $req->tags;
        $v->url_embed = $req->url_embed;
        $v->save();
        return back()->with([
            'message'    => 'Güncellendi.',
            'alert-type' => 'success',
        ]);

    }
}
