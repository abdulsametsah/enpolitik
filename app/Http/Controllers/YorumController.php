<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Haber;
use App\Video;
use App\FotoGalery;
use App\CornerPost;
use App\Yorum;

class YorumController extends Controller
{
    public function store($content_id, Request $req)
    {

        $google = $req->{"g-recaptcha-response"};
        $isim = $req->Comment_Name;
        $mail = $req->Comment_Email;
        $text = $req->Comment_Text;

        $type = $req->type;

        if (!$google) {
            $req->session()->flash('error', "Lütfen robot olmadğınızı doğrulayınız.");
            return back();
        }

        $googleApiUrl = 'https://www.google.com/recaptcha/api/siteverify';

        $post = $this->post($googleApiUrl, array(
            'response' => $google,
            'secret' => env('reCAPTCHA_SECRET')
        ));

        $post = json_decode($post);

        if (!$post->success) {
            $req->session()->flash('error', "Lütfen robot olmadğınızı doğrulayınız.");
            return back();
        }


        if (!$isim || !$mail || !$text) {
            $req->session()->flash('error', "Lütfen boş alan bırakmayınız.");
            return back();
        }

        try {
            $yorum = new Yorum;

            if ($type == "haber") {
                $haber = Haber::findOrFail($content_id);
                $yorum->haber_id = $haber->id;

                $haber->yorum++;
                $haber->save();

            }else if ($type == "video") {
                $video = Video::findOrFail($content_id);
                $yorum->video_id = $video->id;
            }else if ($type == "kose") {
                $kose = CornerPost::findOrFail($content_id);
                $yorum->corner_post_id = $kose->id;
            }else if ($type == "galeri") {
                $galeri = FotoGalery::findOrFail($content_id);
                $yorum->galeri_id = $galeri->id;
            }

            $yorum->isim = $isim;
            $yorum->mail = $mail;
            $yorum->yorum = $text;
            $yorum->save();
            $req->session()->flash('success', "Başarıyla yorum yaptınız.");
            return back();

        } catch (\Exception $e) {
            $req->session()->flash('error', "Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
            return back();
        }
    }

    public function post($url, $postVars = array())
    {
        //Transform our POST array into a URL-encoded query string.
        $postStr = http_build_query($postVars);
        //Create an $options array that can be passed into stream_context_create.
        $options = array(
            'http' =>
                array(
                    'method' => 'POST', //We are using the POST HTTP method.
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $postStr //Our URL-encoded query string.
                )
        );
        //Pass our $options array into stream_context_create.
        //This will return a stream context resource.
        $streamContext = stream_context_create($options);
        //Use PHP's file_get_contents function to carry out the request.
        //We pass the $streamContext variable in as a third parameter.
        $result = file_get_contents($url, false, $streamContext);
        //If $result is FALSE, then the request has failed.
        if ($result === false) {
            //If the request failed, throw an Exception containing
            //the error.
            $error = error_get_last();
            throw new Exception('POST request failed: ' . $error['message']);
        }
        //If everything went OK, return the response.
        return $result;
    }
}
