<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Yorum;

class CornerPost extends Model
{
    public function yazar()
    {
        $user = User::findOrFail($this->user_id);
    	return $user;
    }

    public function comments()
    {
        return Yorum::where('corner_post_id', $this->id)->where('onaylandi', 1)->get();
    }

    public function url()
    {
    	return url('kose-yazisi/' . $this->id . '/' . str_slug($this->title, "-"));
    }

    public function tags()
    {
        $etiketler = explode(",", $this->etiketler);
        $tmp = [];

        foreach ($etiketler as $etiket) {
            $tmp[] = trim($etiket);
        }
        $etiketler = $tmp;
        unset($tmp);

        return $etiketler;
    }
}
