<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Haber;

class Yorum extends Model
{
    public function haber()
    {
    	return Haber::find($this->haber_id);
    }
}
