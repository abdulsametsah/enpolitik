<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Haber;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        function updateHome($haber) {
            // $pc = new \App\Http\Controllers\PagesController;
            // $pc->updateHome();
            
            if ($haber->manset) {
                $sliderHaberler = Haber::where('manset', 1)->orderBy('id', 'desc')->limit(15)->get();
                Cache::put('sliderHaberler', $sliderHaberler, 10);
            }

            if ($haber->editor) {
                $topHaberler = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(5)->get();
                Cache::put('topHaberler', $topHaberler, 10);
            }

            if ($haber->kategori_id == 4) {
                $sporHaberleri = Haber::where('kategori_id', 4)->orderBy('id', 'desc')->limit(10)->get();
                Cache::put('sporHaberleri', $sporHaberleri, 10);
            }

            if ($haber->editor) {
                $editorHaberleri = Haber::where('editor', 1)->orderBy('id', 'desc')->limit(6)->get();
                Cache::put('editorHaberleri', $editorHaberleri, 10);
            }

            if ($haber->kategori_id == 1) {
                $ekonomiHaberleri = Haber::where('kategori_id', 1)->orderBy('id', 'desc')->limit(6)->get();
                Cache::put('ekonomiHaberleri', $ekonomiHaberleri, 10);
            }

            if ($haber->kategori_id == 3) {
                $politikaHaberleri = Haber::where('kategori_id', 3)->orderBy('id', 'desc')->limit(6)->get();
                Cache::put('politikaHaberleri', $politikaHaberleri, 10);
            }

            if ($haber->kategori_id == 7) {
                $dunyaHaberleri = Haber::where('kategori_id', 7)->orderBy('id', 'desc')->limit(6)->get();
                Cache::put('dunyaHaberleri', $dunyaHaberleri, 10);
            }

            if ($haber->sehir) {
                $yerelHaberlerDigerleri = Haber::whereNotNull('sehir')->latest()->skip(3)->limit(6)->get();
                Cache::put('yerelHaberlerDigerleri', $yerelHaberlerDigerleri, 10);
                $yerelHaberler3 = Haber::whereNotNull('sehir')->latest()->limit(3)->get();
                Cache::put('yerelHaberler3', $yerelHaberler3, 10);
            }

            return $haber;
        }

        Haber::created(function ($haber) {
            updateHome($haber);
        });

        Haber::deleted(function ($haber) {
            updateHome($haber);
        });
        
        Haber::updated(function ($haber) {
            updateHome($haber);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
