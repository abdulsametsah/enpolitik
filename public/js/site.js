﻿$(function () {

    //Header Start
    var a = 0;
    $("header nav a").each(function () {
        var rect = $(this)[0].getBoundingClientRect();
        if (rect.width) {
            a += rect.width;
        } else {
            a += rect.right - rect.left;
        }
    });
    $("header nav a, footer nav a").css({
        "margin-right": (($("header nav .container").width() - a) / $("header nav a").length) - 1
    });
    //Header End

    //Manşet 1 Start

    $('.manset-1 img').on('dragstart', function (event) {
        event.preventDefault();
    });

    $(".manset-1").hover(function () {
        $(this).data("disabled", true);
    }, function () {
        $(this).data("disabled", false);
    });

    $(".manset-1 .controls .c").hover(function () {
        var i = $(this).data("id");
        $(this).closest(".manset-1").data("id", i);
        manset1Animate(i, $(this).closest(".manset-1"));
    });

    $(".manset-1 .controls .left").click(function () {
        manset1Left($(this));
    });

    $(".manset-1 .controls .right").click(function () {
        manset1Right($(this));
    });

    var manset1Left = function (t) {
        var i = $(t).closest(".manset-1").data("id");
        i--;
        if (i < 0) i = $(t).closest(".manset-1").find(".c").length - 1;
        $(t).closest(".manset-1").data("id", i);
        manset1Animate(i, $(t).closest(".manset-1"));
    };

    var manset1Right = function (t) {
        var i = $(t).closest(".manset-1").data("id");
        i++;
        if (i > $(t).closest(".manset-1").find(".c").length - 1) i = 0;
        $(t).closest(".manset-1").data("id", i);
        manset1Animate(i, $(t).closest(".manset-1"));
    };

    var manset1Animate = function (i, manset) {
        $(manset).find(".inner").clearQueue();
        $(manset).find(".inner").animate({
            left: i * $(manset).width() * -1
        }, 100, function () {
            $(manset).find("img.lazy").lazy({
                bind: "event"
            });
        });
        $(manset).find(".controls .c.active").removeClass("active");
        $(manset).find(".controls .c").eq(i).addClass("active");
    }

    setInterval(function () {
        $(".manset-1").each(function () {
            var manset = $(this);
            if ($(manset).data("disabled") === false) {
                var i = $(manset).data("id");
                i++;
                if (i > $(manset).find(".c").length - 1) i = 0;
                $(manset).data("id", i);
                manset1Animate(i, manset);
            }
        });
    }, 10000);

    $(".manset-1").each(function () {
        var manset = $(this);
        manset.hammer().on("swipeleft", function (event) {
            manset1Right(manset);
            return false;

        });
        $(manset).hammer().on("swiperight", function (event) {
            manset1Left(manset);
            return false;

        });
    });

    //Manşet 1 End

    //Scroller Start

    $(".scroller").hover(function () {
        $(this).data("disabled", true);
    }, function () {
        $(this).data("disabled", false);
    });

    $(".scroller .left").click(function () {
        var scroller = $(this).closest(".scroller");
        var size = $(scroller).data("size");
        $(scroller).find(".inner").clearQueue();
        for (var i = 0; i < size; i++) {
            $(scroller).find(".inner .content").last().prependTo($(scroller).find(".inner"));
        }
        $(scroller).find(".inner").css("left", "-" + $(scroller).width() + "px");
        $(scroller).find(".inner").animate({
            left: "+=" + $(scroller).width() + "px"
        }, 100);
    });

    $(".scroller .right").click(function () {
        var scroller = $(this).closest(".scroller");
        scrollerAnimate(scroller);
    });

    $(".scroller .down").click(function () {
        var scroller = $(this).closest(".scroller");
        scrollerAnimate(scroller);
    });

    $(".scroller .up").click(function () {
        var scroller = $(this).closest(".scroller");
        var size = $(scroller).data("size");
        $(scroller).find(".inner").clearQueue();
        $(scroller).find(".inner").css("top", "-" + $(scroller).find(".outer").height() + "px");
        for (var i = 0; i < size; i++) {
            $(scroller).find(".inner .content").last().prependTo(scroller.find(".inner"));
        }
        $(scroller).find(".inner").animate({
            "top": "+=" + $(scroller).find(".outer").height() + "px"
        }, 100);
    });

    var scrollerAnimate = function (scroller) {
        $(scroller).find(".inner").clearQueue();
        if ($(scroller).data("direction") === "vertical") {
            $(scroller).find(".inner").animate({
                top: "-=" + $(scroller).find(".outer").height() + "px"
            }, 100, function () {
                $(scroller).find('img.lazy').lazy({
                    bind: "event"
                });
                var size = $(scroller).data("size");
                for (var i = 0; i < size; i++) {
                    $(scroller).find(".inner .content").first().appendTo($(scroller).find(".inner"));
                }
                $(scroller).find(".inner").css("top", "0px");
            });
        } else {
            $(scroller).find(".inner").animate({
                left: "-=" + $(scroller).width() + "px"
            }, 100, function () {
                $(scroller).find('img.lazy').lazy({
                    bind: "event"
                });
                var size = $(scroller).data("size");
                for (var i = 0; i < size; i++) {
                    $(scroller).find(".inner .content").first().appendTo($(scroller).find(".inner"));
                }
                $(scroller).find(".inner").css("left", "0px");
            });
        }
    };

    setInterval(function () {
        $(".scroller").each(function () {
            if ($(this).data("disabled") === false) {
                scrollerAnimate($(this));
            }
        });
    }, 10000);

    //Scroller End

    $("#weatherlist").change(function () {
        var v = $(this).val();
        $(".havadurumu .loading").show();
        $.get("/odata/Weather(" + v + ")?$expand=Province").then(function (response) {
            $(".havadurumu .il").text(response.Province.ProvinceName);
            $(".havadurumu .image i").attr("class", response.Icon);
            $(".havadurumu .description").text(response.Description);
            $(".havadurumu .temp").text(parseFloat(response.Temp).toFixed(0));
            $(".havadurumu .loading").hide();
        });
    });

    $("[data-open]").click(function () {
        var target = $(this).data("open");
        $(target).toggleClass("opened");
        return false;
    });

    $("form.recaptcha").on("submit", function () {
        if ($(this).valid() && grecaptcha.getResponse() == "") {
            $(".validation-summary-errors ul").html("");
            $(".validation-summary-errors ul")
                .append("<li>Lütfen Formu Doğrulayınız</li>");
            return false;
        } else {
            return true;
        }
        return false;
    });

    $("img.lazy").lazy();

    var fs = 16;

    if ($.cookie && $.cookie("fontSize")) {
        var fs = parseFloat($.cookie("fontSize"));
        $(".news .text").css("font-size", fs);
    }

    $(".font-minus").click(function () {
        $(".news .text").css("font-size", fs -= 2);
        $.cookie('fontSize', fs, {
            expires: 365,
            path: '/'
        });
    });

    $(".font-plus").click(function () {
        $(".news .text").css("font-size", fs += 2);
        $.cookie('fontSize', fs, {
            expires: 365,
            path: '/'
        });
    });

    $(".font-reset").click(function () {
        $(".news .text").css("font-size", fs = 14);
        $.cookie('fontSize', fs, {
            expires: 365,
            path: '/'
        });
    });

    $(".print").click(function () {
        window.print();
    });

    const player = new Plyr('#player');

    setInterval(function () {
        var width = "-=" + ($(".sondakika .content").first().width() + 20);
        $(".sondakika .inner").animate({
            marginLeft: width
        }, 500, function () {
            $(".sondakika .content").last().after($(".sondakika .content").first());
            $(".sondakika .inner").css({
                marginLeft: "0px"
            })
        })
    }, 5000);

    $(document).on("change", "#leagues", function () {
        var v = $(this).val();
        $(".ligtablosu .loading").show();
        $.get("/site/league?Id=" + v).then(function (response) {
            $(".ligtablosu").replaceWith(response);
            $(".ligtablosu .loading").hide();
        });
    });

    $(document).on("change", "#names", function () {
        var v = $(this).val();
        $(".ligtablosu .loading").show();
        $.get("/site/league?Id=" + $("#leagues").val() + "&Id2=" + v).then(function (response) {
            $(".ligtablosu").replaceWith(response);
            $(".ligtablosu .loading").hide();
        });
    });

    $(".koseyazarlari .content").first().css("border-left", "5px solid #f28c03");

    $(".show-others").click(function () {
        var target = $(this).data("show");
        $(target).removeClass("hidden");
        $(this).hide();
    });
});