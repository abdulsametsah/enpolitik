﻿$(function () {

    var currentDate = new Date();

    // $("textarea.tinymce").tinymce({
    //     script_url: "/lib/tinymce/tinymce.min.js",
    //     selector: "textarea.tinymce",
    //     theme: "modern",
    //     language: "tr_TR",
    //     height: "300px",
    //     plugins: "link image media preview code fullscreen paste",
    //     toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | link unlink removeformat | image media | preview code fullscreen",
    //     valid_elements: "p,strong/b,strong/h2,img[src],br,iframe[*]",
    //     invalid_elements: "div,table,thead,tbody,tr,th,td",
    //     menubar: false
    // });

    // ckeditor.replace('textarea.tinymce');

    $('textarea.tinymce').redactor({
        lang: 'tr',
        toolbarFixedTopOffset: 50,
        formatting: ['p'],
        buttonsHide: ['format'],
        buttonsAddBefore: {
            before: 'bold',
            buttons: ['undo', 'redo']
        },
        // pastePlainText: true,
        // pasteImages: true,
        // pasteClean: true,
        pasteBlockTags: ['pre', 'table', 'tbody', 'thead', 'tfoot', 'th', 'tr', 'td', 'ul', 'ol', 'li', 'blockquote', 'p', 'figure', 'figcaption'],
        plugins: ['fontsize', 'alignment', 'table', 'imagemanager', 'filemanager', 'widget', 'video'],
        imageUpload: '/site/uploadfile',
        imageManagerJson: '/site/getimages',
        imagePosition: true,
        imageResizable: true,
        imageData: {
            type: "Image"
        },
        fileUpload: '/site/uploadfile',
        fileManagerJson: '/site/getfiles',
        fileData: {
            type: "File"
        },
        multipleUpload: false,
        minHeight: "200px",
        formattingAdd: [{
            tag: "table",
            class: "table table-bordered"
        }]
    });

    $(".masked-date").mask("99.99.9999 99:99:99");

    $("a.remove").click(function () {
        if (window.confirm('Kayıt Kalıcı Olarak Silinecek. Emin misiniz?')) {
            return true
        };

        return false;
    });

    $('.fileinput').on('change.bs.fileinput', function (e) {
        if (!$(this).data("no-crop")) {
            var cropper = $(this);
            var preview = $(this).find(".fileinput-preview");
            var img = $(this).find(".fileinput-preview img");
            var width = preview.data("width");
            var height = preview.data("height");
            var name = preview.data("name");
            img.cropper({
                aspectRatio: width / height,
                viewMode: 1,
                dragMode: 'move',
                cropBoxResizable: false,
                crop: function (event) {
                    event.detail['resizeWidth'] = width;
                    event.detail['resizeHeight'] = height;
                    cropper.find("input[name='" + name + "']").val(JSON.stringify(event.detail));
                }
            });
        }
    });

    $(".has-other").change(function (r) {
        if ($(this).children("option:selected").text() == "Diğer") {
            $(this).next().removeClass("hidden");
        }
        else {
            $(this).next().addClass("hidden");
        }
    });

    $(".tagsinput").tagsinput({ confirmKeys: [13, 188] });

    $(".headline.has-details").change(function (r) {
        var details = $(this).data("details");
        if ($(this).val() > 0) {
            $(details).show();
        }
        else {
            $(details).hide();
        }
    });

    $("input").on("keydown", function (event) {
        var x = event.which;
        if (x === 13) {
            event.preventDefault();
        }
    });

    $("button.copy").click(function () {
        var from = $(this).data("from");
        var to = $(this).data("to");
        $("input[name='" + to + "']").val($("input[name='" + from + "']").val());
    });

    $("button.saveandpublish").click(function () {
        var change = $(this).data("change");
        $("select[name='" + change + "']").val(1);
        $(this).closest("form").submit();
    })
});