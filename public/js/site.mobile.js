$(function () {
    'use strict';

    // Left Sidebar
    $('#menu-left').sideNav({
        menuWidth: 240, // Default is 240
        edge: 'left',
        closeOnClick: true // Closes side-nav on <a> clicks
    });
    // Right Sidebar
    $('#menu-right').sideNav({
        menuWidth: 240, // Default is 240
        edge: 'right',
        closeOnClick: false // Closes side-nav on <a> clicks
    });

    $(".news-slider").owlCarousel({
        autoplay: true,
        autoplayTimeout: 10000,
        lazyLoad: true,
        lazyLoadEager: 1,
        loop: true,
        items: 1
    });

    $('.manset-2').owlCarousel({
        autoplay: true,
        autoplayTimeout: 10000,
        margin: 10,
        lazyLoad: true,
        loop: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 2,
                lazyLoadEager: 2
            },
            768: {
                items: 4,
                lazyLoadEager: 4
            }
        }
    });

    $(".scroller").owlCarousel({
        autoplay: true,
        autoplayTimeout: 10000,
        lazyLoad: true,
        loop: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 1,
                lazyLoadEager: 1
            },
            768: {
                margin: 10,
                items: 3,
                lazyLoadEager: 3
            }
        }
    });
    $('.widget-item-slider').owlCarousel({
        margin: 10,
        lazyLoad: true,
        loop: true,
        nav: true,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 2,
                lazyLoadEager: 2
            },
            768: {
                items: 4,
                lazyLoadEager: 4
            }
        }
    });

    // Swipebox gallery
    $('.swipebox').swipebox();

    // Right sidebar tabs
    $('.sidebar-tabs ul.tabs').tabs();

    $('.ligtablosu ul.tabs').tabs();

    // Scroll to top
    var winScroll = $(window).scrollTop();
    if (winScroll > 1) {
        $('#to-top').css({
            bottom: "10px"
        });
    } else {
        $('#to-top').css({
            bottom: "-100px"
        });
    }
    $(window).on("scroll", function () {
        winScroll = $(window).scrollTop();

        if (winScroll > 1) {
            $('#to-top').css({
                opacity: 1,
                bottom: "30px"
            });
        } else {
            $('#to-top').css({
                opacity: 0,
                bottom: "-100px"
            });
        }
    });
    $('#to-top').click(function () {
        $('html, body').animate({
            scrollTop: '0px'
        }, 800);
        return false;
    });

    const player = new Plyr('#player');

    $("img.lazy").lazy();

    $("#weatherlist").change(function () {
        var v = $(this).val();
        var t = $(this).find("option:selected").text();
        $(".havadurumu .loading").show();
        $.get("/odata/Weather(" + v + ")?$expand=Province").then(function (response) {
            $(".havadurumu .il").text(t);
            $(".havadurumu .image i").attr("class", response.Icon);
            $(".havadurumu .description").text(response.Description);
            $(".havadurumu .temp").text(parseFloat(response.Temp).toFixed(0));
            $(".havadurumu .loading").hide();
        });
    });

    $(document).on("change", "#leagues", function () {
        var v = $(this).val();
        $(".ligtablosu .loading").show();
        $.get("/site/LeagueMobile?Id=" + v).then(function (response) {
            $(".ligtablosu").replaceWith(response);
            $('.ligtablosu ul.tabs').tabs();
            $(".ligtablosu .loading").hide();
        });
    });

    $(document).on("change", "#names", function () {
        var v = $(this).val();
        $(".ligtablosu .loading").show();
        $.get("/site/leagueMobile?Id=" + $("#leagues").val() + "&Id2=" + v).then(function (response) {
            $(".ligtablosu").replaceWith(response);
            $('.ligtablosu ul.tabs').tabs();
            $(".ligtablosu .loading").hide();
        });
    });
});