<?php


$old_db = new PDO("mysql:host=localhost;dbname=enpolitik_old;charset=utf8", "root", "");
$new_db = new PDO("mysql:host=localhost;dbname=enpolitik;charset=utf8", "root", "");
?>

<style>
    body {
        background: #1f4f1f;
        color: #fff;
        word-break: break-all;
    }

    pre {
        word-break: break-all !important;
        font-size: 21px;
    }
</style>
