<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include __DIR__ . "/settings.php";

$haberler = $new_db->query("SELECT * FROM habers ORDER BY id desc LIMIT 110,5", PDO::FETCH_ASSOC);

foreach ($haberler as $h) {
    $photo = $h['image'];
    echo $h['id'] . '--' . $h['slug'] . "\n";
    $photo = explode(".", $photo);
    $path  = $photo[0];
    $ext   = $photo[1];
    $slashes = explode("/", $path);
    $end  = end($slashes);
    $base     = "/var/www/enpolitik.tk/public";
    $new_base = "/var/www/enpolitik.tk/public/storage/haberler/old_photos/" . $h['id'];

    if (!is_dir($new_base)) mkdir($new_base);
    
    $newName =  $new_base . "/" . $end . "." . $ext;
    rename($base . $path . "." . $ext, $newName);

    $update = $new_db->prepare("UPDATE habers SET image = ? WHERE id = ?");
    $update->execute(['haberler/old_photos/' . $h['id'] . '/' . $end . '.' . $ext, $h['id']]);
}
