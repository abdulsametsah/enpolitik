<?php

set_time_limit(0);

include __DIR__ . "/settings.php";

$limit = 10000;

$step = $_GET['page'] ?? 1;
$start = ($step - 1) * $limit;

$count = $old_db->query('select count(*) news from news')->fetch();
$count = $count["news"];


$news = $old_db->query("SELECT * FROM news LIMIT $start, $limit", PDO::FETCH_ASSOC);
$effected = 0;

foreach ($news as $new) {
    $id         = $new['Id'];
    $title      = $new['Title'];
    $slug       = $new['Url'];
    $summary    = $new['Summary'];
    $body       = $new['Text'];
    $tags       = $new['Tags'];
    $hit        = $new['ReadCount'];
    $category_id= $new['CategoryId'];
    $yorum      = $new['CommentCount'];
    $created_at = $new['Date'];
    $updated_at = $new['UpdateDate'];
    $image      = $new['ImageUrl'];
    $sehir_id   = $new['ProvinceId'];
    
    if ($sehir_id) {
        $sehir = $old_db->query("SELECT * FROM provinces WHERE Id=$sehir_id")->fetch();
        $sehir = $sehir['ProvinceName'];
    }else {
        $sehir = null;
    }

    
    
    $sql = "INSERT INTO habers (id,title,summary,body,kategori_id,tags,created_at,updated_at,image,sehir,hit,yorum) 
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    $ekle = $new_db->prepare($sql);
    $ekle->execute([$id, $title, $summary,$body,$category_id,$tags,$created_at,$updated_at,$image,$sehir,$hit,$yorum]);
    if ($ekle)
        $effected++;
    else {
        echo "\n". $title . " eklenirken bir hata oluştu.";
        $ekle->errorInfo();
    }

}

echo "\n\nEklenen\t$effected\nToplam\t" .number_format( $count) . "\nBaş\t". $start . "\nLimit\t". $limit . "\nKalan\t" . number_format($count - $limit - $start);

header("location=news.php?page=". ( $step + 1 ));