@extends('layouts.app')

@section('content')
<div class="section-2">
    <div class="leftside">
    <div class="article">
       	<h1 class="title">{{ $title }}</h1>
        <div class="yazarlar">
        	@foreach($yazarlar as $kose)
            <a class=content href="{{ $kose->url() }}">
			    <div class=image><img src="{{ Voyager::Image($kose->yazar()->avatar) }}"></div>
			    <div class=title>{{ $kose->yazar()->name }}</div>
			    <div class=article>{{ $kose->title }}</div>
			</a> 
            @endforeach
            <div class="clearfix"></div>
        </div>
        <div class="spacer-5"></div>
        <div class="text-center">
            {{ $yazarlar->links() }}
        </div>
        <div class="spacer-20"></div>
    </div>
</div>
	@include("layouts.aside")
</div>
@stop