@extends('layouts.app')

@section('content')
<div class="section-2">
    <div class="leftside">
    <div class="category">
        <h3 class="custom-title">
            <span>{{ $title }}</span>
            <div class="index"><a target="_blank" href="/rss.xml?id=88"> <i class="fas fa-rss"></i> </a></div>
        </h3>
        <div class="box-1">
        	@foreach($haberler as $haber)
            <a class="content" href="{{ $haber->url() }}">
                <div class="image">
                	<img class="lazy image-center" data-src="{{ Voyager::Image($haber->thumbnail('kare')) }}" style="display: block;">
                </div>
                <div class="news-title">
					{{ $haber->titleShort() }}
                </div>
            </a>
            @endforeach
            <div class="clearfix"></div>
        </div>
        <div class="spacer-5"></div>
        <div class="text-center">
            {{ $haberler->links() }}
        </div>
        <div class="spacer-20"></div>
    </div>
</div>
	@include("layouts.aside")
</div>
@stop