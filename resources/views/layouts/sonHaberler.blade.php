<?php

    if (\Illuminate\Support\Facades\Cache::has('sonHaberlerLayout'))
        $sonHaberlerLayout = \Illuminate\Support\Facades\Cache::get('sonHaberlerLayout');
    else {
        $sonHaberlerLayout = \App\Haber::latest()->limit(20)->get();
        \Illuminate\Support\Facades\Cache::put('sonHaberlerLayout', $sonHaberlerLayout, 10);
    }
?>

<div class="spacer-20"></div>
<div class="box-1 style-5">
	@foreach($sonHaberlerLayout as $haber)
    <div class="content">
        <div class="image">
        	<a href="{{ $haber->url() }}"> 
        		<img src="{{ $haber->image_url() }}" class="image-center"> 
        	</a>
        </div>
        <div class="news-title">
        	<a href="{{ $haber->url() }}">
        		{{ $haber->titleShort() }}
            </a>
        </div>
    </div>
    @endforeach
    <div class="clearfix"></div>
</div>
<div class="spacer-5"></div>