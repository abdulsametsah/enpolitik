<?php

    if (\Illuminate\Support\Facades\Cache::has('sonKoseYazilariLayout'))
        $sonKoseYazilariLayout = \Illuminate\Support\Facades\Cache::get('sonKoseYazilariLayout');
    else {
        $sonKoseYazilariLayout = \App\CornerPost::whereNotNull('user_id')->latest()->limit(12)->get();
        \Illuminate\Support\Facades\Cache::put('sonKoseYazilariLayout', $sonKoseYazilariLayout, 10);
    }
?>

@foreach($sonKoseYazilariLayout as $kose)
<a class=content href="{{ $kose->url() }}">
    <div class=image><img src="{{ Voyager::Image($kose->yazar()->avatar) }}"></div>
    <div class=title>{{ $kose->yazar()->name }}</div>
    <div class=article>{{ $kose->title }}</div>
</a> 
@endforeach