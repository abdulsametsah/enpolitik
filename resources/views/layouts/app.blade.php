<!DOCTYPE html>
<html lang=tr>
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1.0">
    <title>@yield('title', "Anasayfa - ") Enpolitik.com</title>
    <link rel=stylesheet href="/css/site.min.css?v=Z6fyqMxaMewcEft143EpByNaN-OIXADLE08Z2hhKMxo">
    <link rel=stylesheet href="{{ asset('css/app.css?v=1.'.rand(0,9999)) }}">
    <meta name=description content="">
    <meta name=keywords content="haber,haberler,sondakika,haberportal,portal,güncel,magazin haberleri,sondakika haberler">
    <meta name=twitter:card content=summary_large_image>
    <meta name=twitter:title content=@yield('contentTitle', "ENPOLİTİK")>
    <meta name=twitter:description content="@yield('aciklama', "ENPOLİTİK")">
    <meta itemprop=name content=@yield('contentTitle', "ENPOLİTİK")>
    <link rel=canonical href="@yield('contentUrl', env('APP_URL'))">
    <link rel=alternate type=application/rss+xml title=RSS href=//www.enpolitik.com/rss.xml>
    <meta name=robots content="index, follow">
    <meta name=rating content=All>
    <meta name=news_keywords
          content="haber, haberler, sondakika, haberportal, portal, güncel, magazin haberleri, sondakika haberler">
    <meta property=og:site_name content=@yield('contentTitle', "ENPOLİTİK")>
    <meta property=og:title content=@yield('contentTitle', "ENPOLİTİK")>
    <meta property=og:description content="">
    <meta property=og:image content=@yield('image', "http://www.aygazete.com/images/logo.png")>
    <meta property=og:locale content=tr_TR>
    <meta property=og:locale:alternate content=tr_TR>
    <meta http-equiv=Refresh content=600>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<body>
<header>

    <div class=container>
        <div class=logo>
            <div class=pull-left><a href="/"> <img src="{{ Voyager::Image(setting('site.logo')) }}"> </a></div>
            <div class=pull-right>
                <div class=advertisement><a href=//www.mapyolgayrimenkul.com target=_blank><img
                                src=//www.enpolitik.com/files/uploads/mapyol.gif width=728 height=90
                                alt="Mapyol Gayrimenkul" border=0></a></div>
            </div>
            <div class=clearfix></div>
        </div>
    </div>
    <nav class="yeniMenu">
        <div class=container>
            @foreach (json_decode(menu('site', '_json')) as $menu)
                <a href="{{ url($menu->url) }}">
                    @if ($menu->url == '/')
                        <i class="fas fa-home fa-fw"></i>
                    @endif
                    {{ $menu->title }}
                </a>
            @endforeach
            <a href=# data-open=.submenu><i class="fas fa-bars fa-fw"></i></a>
        </div>
    </nav>
    <div class=submenu>
        <div class=container>
            @foreach(\App\Kategory::all() as $kategori)
                <a href="{{ $kategori->url() }}">{{ $kategori->name }}</a>
            @endforeach
            <hr>
            <a href=/sayfa/yazarlar>Yazarlar</a> <a href=/sayfa/galeriler>Galeriler</a> <a
                    href=/sayfa/videolar>Videolar</a> <a href=/sayfa/kunye>K&#xFC;nye</a> <a href=/sayfa/iletisim>&#x130;leti&#x15F;im</a>
            <div class=spacer-5></div>
        </div>
    </div>
</header>
<div class=content-bg></div>
<div class=bg-grey>
    <div class=advertisement-container>
        <div class=advertisement-left>
            <div class=advertisement>
                <script async src=//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js></script>
                <ins class=adsbygoogle style="display:inline-block;width:120px;height:600px"
                     data-ad-client="ca-pub-1858947299980333" data-ad-slot="1798148709"></ins>
                <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
            </div>
        </div>
        <div class=advertisement-right>
            <div class=advertisement>
                <script async src=//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js></script>
                <ins class=adsbygoogle style=display:inline-block;width:120px;height:600px
                     data-ad-client=ca-pub-1858947299980333 data-ad-slot=1798148709></ins>
                <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
            </div>
        </div>
    </div>
    <div class=container>
        @yield("content")
    </div>
</div>
<footer>
    <div class=container>
        <nav>
            <div class=container>
                @foreach (json_decode(menu('site', '_json')) as $menu)
                    <a href="{{ url($menu->url) }}">
                        @if ($menu->url == '/')
                            <i class="fas fa-home fa-fw"></i>
                        @endif
                        {{ $menu->title }}
                    </a>
                @endforeach
                <a href="javascript:void(0)"><i class="fas fa-bars fa-fw"></i></a>
            </div>
        </nav>
        <div class=submenu>
            <div class=container>
                <div class=spacer-5></div>
            </div>
        </div>
    </div>
    <div class="container content">
        <div class=left>
            <div class=logo><img src="{{ Voyager::Image(setting('site.logo')) }}"></div>
            <div class=social><a target=_blank href=//www.facebook.com/enpolitik class=facebook> <i
                            class="fab fa-facebook-f"></i> </a> <a target=_blank href=//twitter.com/enpolitik
                                                                   class=twitter> <i class="fab fa-twitter"></i> </a>
            </div>
        </div>
        <div class=middle>
            <div class=pages><a href=/yazarlar>Yazarlar</a> <a href=/galeriler>Galeriler</a> <a
                        href=/videolar>Videolar</a> <a href=/sayfa/kunye>K&#xFC;nye</a> <a href=/sayfa/iletisim>&#x130;leti&#x15F;im</a>
            </div>
            <div class=categories>
                @foreach(\App\Kategory::limit(14)->get() as $kat)

                    <a href=/haberler/{{ $kat->slug }}>{{ $kat->name }}</a>
                @endforeach
            </div>
        </div>
        <div class=right><img src=/images/appstore.png> <img src=/images/googleplay.png></div>
        <div class=clearfix></div>
    </div>
    <div class=container>EnPolitik.com Haber Portalı 5846 sayılı Fikir ve Sanat Eserleri Kanunu'na %100 uygun olarak
        yayınlanmaktadır. Ajanslardan alınan haberlerin yeniden yayımı ve herhangi bir ortamda basılması, ilgili
        ajansların bu yöndeki politikasına bağlı olarak önceden yazılı izin gerektirir.
    </div>
    <div class=spacer-30></div>
</footer>
<div class=advertisement-popup>
    <div class=modal tabindex=-1 role=dialog>
        <div class=modal-dialog role=document>
            <div class=modal-content>
                <div class=modal-body></div>
            </div>
        </div>
        <span class=close> <i class="fas fa-times-circle"></i> Kapat </span></div>
</div>
<script async src="//www.googletagmanager.com/gtag/js?id=UA-72726744-1"></script>
<script>window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-72726744-1');</script>
<script src=/js/assets.min.js></script>
<script src=/lib/hammerjs/hammer.min.js></script>
<script src=/lib/jquery-hammerjs/jquery.hammer.js></script>
<script src="/js/site.min.js?v=2"></script>
@yield('js')
<script>if ($(".advertisement-popup .modal-body").html() !== "") {
        $(".advertisement-popup .modal .close").click(function () {
            $(".advertisement-popup .modal").modal("hide");
        });
        setTimeout(function () {
            if ($.cookie) {
                if ($.cookie("popup", Boolean) != true) {
                    $(".advertisement-popup .modal").modal("show");
                }
                $.cookie('popup', true, {expires: 1 / 24, path: '/'});
            }
        }, 1000);
    }</script>