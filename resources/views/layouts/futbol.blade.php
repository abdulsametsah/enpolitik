<div class="ligtablosu">
    <div class="loading"><object data="/images/loading.svg" type="image/svg+xml"></object></div>
    <h3 class="widget-title"><span class="green">Lig Tablosu</span></h3>
    <div class="select-container">
        <select id="leagues">
            <option value="0">Spor Toto Süper Lig</option>
        </select>
    </div>
    <ul class="tabs" role="tablist">
        <li role="presentation" class="active"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Puan Durumu</a></li>
        <li role="presentation"><a href="#fixture" aria-controls="fixture" role="tab" data-toggle="tab">Fikstür</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="table">
            <div class="content">
                <table class="ltable">
                        <tr>
                            <th>Takım</th>
                            <th>O</th>
                            <th>G</th>
                            <th>B</th>
                            <th>M</th>
                            <th>AV</th>
                            <th>P</th>
                        </tr>
                        @php
                         	$futbol = new \App\Http\Controllers\FutbolController();
                         	$i = 1;
                        @endphp
                        @foreach($futbol->getPoints() as $takim)
                        <tr>
                            <td>{{ $i++ }} {{ $takim['takim'] }}</td>
                            <td>{{ $takim['o'] }}</td>
                            <td>{{ $takim['g'] }}</td>
                            <td>{{ $takim['b'] }}</td>
                            <td>{{ $takim['m'] }}</td>
                            <td>{{ $takim['av'] }}</td>
                            <td>{{ $takim['p'] }}</td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="fixture">
            <table class="fixture">
                    @foreach($futbol->getFikstur() as $date => $matches)
                        <tr class="date">
                            <td colspan="5">{{ $date }}</td>
                        </tr>
                        @foreach($matches as $match)
                        <tr class="result">
                            <td>{{ $match['home'] }}</td>
                            <td></td>
                            <td>{{ $match['date'] }}</td>
                            <td></td>
                            <td>{{ $match['away'] }}</td>
                        </tr>
                        @endforeach
                    @endforeach
            </table>
        </div>
    </div>
</div>