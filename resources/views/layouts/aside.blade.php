<aside>
    <div class="search">
        <form action="/haberler">
            <h3 class="custom-title"><span>Haberlerde Ara</span></h3>
            <div class="content">
                <input type="search" name="search" placeholder="Arama Yapmak İçin Tıklayın"> <button> <i class="fas fa-search"></i> </button>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
    <div class="spacer-20"></div>
    <div class="koseyazarlari2 scroller" data-disabled="false" data-size="3" data-direction="vertical">
        <h3 class="custom-title">
            <span>Köşe Yazarları</span>
            <div class="index">
                <div class="up"><i class="fas fa-chevron-up"></i></div>
                <div class="down"><i class="fas fa-chevron-down"></i></div>
            </div>
        </h3>
        <div class="outer">
            <div class="inner" style="top: 0px;">
                @include('layouts.koseYazilari')
            </div>
        </div>
    </div>
    @include('layouts.futbol')
    <div class="gununkarikaturu">
        <h3 class="custom-title"><span>Günün Karikatürü</span></h3>
        <div class="content">
            <img src="{{ Voyager::Image(setting('site.karikatur')) }}" class="lazy">
        </div>
    </div>
    <div class="spacer-20"></div>
</aside>
<div class="clearfix"></div>