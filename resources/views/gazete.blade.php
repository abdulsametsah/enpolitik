@extends('layouts.app')

@section('content')
<div class="section-2">
    <div class="leftside">
    <div class="category">
        <h3 class="custom-title">
            <span>{{ $title }}</span>
            <div class="index"><a target="_blank" href="/rss.xml?id=88"> <i class="fas fa-rss"></i> </a></div>
        </h3>
        <div class="newspaper">
        	<img src="{{ Voyager::Image($gazete->image) }}" alt="">
        </div>
        <div class="newspapers">
        	@foreach($gazeteler as $gazete)
            <a class="content" href="{{ $gazete->url() }}">
                <div class="title">
					{{ $gazete->name }}
                </div>
                <div class="image">
                	<img class="lazy" data-src="{{ Voyager::Image($gazete->thumbnail('kucuk')) }}" style="display: block;">
                </div>
            </a>
            @endforeach
            <div class="clearfix"></div>
        </div>
        <div class="spacer-5"></div>
        <div class="spacer-20"></div>
    </div>
</div>
	@include("layouts.aside")
</div>
@stop