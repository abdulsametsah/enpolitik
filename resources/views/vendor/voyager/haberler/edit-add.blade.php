@php
    if (!isset($dataTypeContent)) $dataTypeContent = null;

    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.css">
    <style>
        label, input, small {
            color: #76838f !important;
        }
    </style>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <div style="margin: 10px">
                        @if ($add)
                            <form action="/yeni-haber-ekle" class="form" enctype="multipart/form-data" method="post" id="createForm">
                                @csrf
                                <div class="form-group">
                                    <label for="">Haber kategorisi</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="0">Seçiniz</option>
                                        @foreach(\App\Kategory::orderBy('name')->get() as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Başlık</label>
                                    <input type="text" name="title" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Özet</label>
                                    <textarea name="summary" rows="5" class="form-control"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Şehir</label>
                                    <input type="text" name="sehir" class="form-control" placeholder="Yerel haber ise şehir yazınız. Değilse boş bırakınız">
                                </div>

                                <div class="form-group">
                                    <label for="">Haber İçeriği</label>
                                    <textarea name="body" id="body"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Etiketler</label>
                                    <input type="text" name="tags" class="form-control" placeholder="Etiket1, etiket2, etiket3, ...">
                                </div>

                                <div class="form-group">
                                    <label for="">Fotoğraf</label>
                                    <input type="file" name="photo">
                                    <div id="imageView" style="max-width: 600px; margin-top: 10px; padding: 10px; background: #ddd; display: none">
                                        <img src="" alt="" style="max-width: 100%">
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Manşet Fotoğrafı</label>
                                    <input type="file" name="mansetPhoto">
                                    <input type="hidden" name="mansetData" id="mansetData">
                                    <small>Eğer manşet haberi değilse boş bırakılabilir.</small>
                                    <div id="mansetImageView" style="max-width: 800px; margin-top: 10px; display: none">
                                        <img src="" alt="" style="max-width: 100%">
                                        <small>Mouse tekerleği ile yakınlaşıp uzaklaşabilirsiniz.</small>
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="editor">
                                        <input type="checkbox" id="editor" name="editor">
                                        Editörün Seçtiği ?
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="manset">
                                        <input type="checkbox" id="manset" name="manset">
                                        Manşette Gözüksün?
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="">Url/Embed</label>
                                    <input type="text" class="form-control" name="url_embed">
                                </div>

                                <div class="form-group">
                                    <label for="">Video</label>
                                    <input type="file" name="video">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-success" id="createButton">Kaydet</button>
                                </div>
                            </form>
                        @endif
                        @if ($edit)
                            @php $haber = $dataTypeContent; @endphp

                            <form action="/yeni-haber-guncelle" class="form" enctype="multipart/form-data" method="post" id="createForm">
                                <input type="hidden" name="id" value="{{ $haber->id }}">
                                @csrf
                                <div class="form-group">
                                    <label for="">Haber kategorisi</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="0">Seçiniz</option>
                                        @foreach(\App\Kategory::orderBy('name')->get() as $cat)
                                            <option value="{{ $cat->id }}" {{ $haber->kategori_id == $cat->id ? 'selected':'' }}>{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Başlık</label>
                                    <input type="text" name="title" value="{{ $haber->title }}" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Özet</label>
                                    <textarea name="summary" rows="5" class="form-control">{{ $haber->summary }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Şehir</label>
                                    <input type="text" name="sehir" value="{{ $haber->sehir }}" class="form-control" placeholder="Yerel haber ise şehir yazınız. Değilse boş bırakınız">
                                </div>

                                <div class="form-group">
                                    <label for="">Haber İçeriği</label>
                                    <textarea name="body" id="body">{!! $haber->body !!}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Etiketler</label>
                                    <input type="text" name="tags" value="{{ $haber->tags }}" class="form-control" placeholder="Etiket1, etiket2, etiket3, ...">
                                </div>

                                <div class="form-group">
                                    <label for="">Fotoğraf</label>
                                    <input type="file" name="photo">
                                    <div id="imageView" style="max-width: 600px; margin-top: 10px; padding: 10px; background: #ddd;">
                                        <img src="{{ $haber->image_url() }}" alt="" style="max-width: 100%">
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Manşet Fotoğrafı</label>
                                    <input type="file" name="mansetPhoto">
                                    <input type="hidden" name="mansetData" id="mansetData">
                                    <small>Eğer manşet haberi değilse boş bırakılabilir.</small>
                                    <div id="mansetImageView" style="max-width: 800px; margin-top: 10px; {{ $haber->slider_url() ? '': 'display:none' }}">
                                        <img src="{{ $haber->slider_url() }}" alt="" style="max-width: 100%">
                                        <small>Mouse tekerleği ile yakınlaşıp uzaklaşabilirsiniz.</small>
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="editor">
                                        <input type="checkbox" id="editor" name="editor" {{ $haber->editor ? 'checked':'' }}>
                                        Editörün Seçtiği ?
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="manset">
                                        <input type="checkbox" id="manset" name="manset" {{ $haber->manset ? 'checked':'' }}>
                                        Manşette Gözüksün?
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="">Url/Embed</label>
                                    <input type="text" class="form-control" value="{{ $haber->url_embed }}" name="url_embed">
                                </div>

                                <div class="form-group">
                                    <label for="">Video</label>
                                    @if ($haber->video)
                                    <br>
                                    <video src="{{ asset($haber->video) }}" controls></video>
                                    <br>
                                    <a id="deleteVideo" href="/delete-video/{{ $haber->id }}">Videoyu Sil</a>
                                    @endif
                                    <input type="file" name="video">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-success" id="createButton">Kaydet</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/cke/ckeditor.js?2') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>
    <script>
        let deleteVideo = document.getElementById('deleteVideo');
        deleteVideo.addEventListener('click', (e) => {
            e.preventDefault();

            let r = confirm("Videoyu silmek istediğinizden emin misiniz?");
            if (r == true) {
                window.location.href = e.target.getAttribute('href');
            }
        });

        let createButton = document.getElementById('createButton');
        let createForm = document.getElementById('createForm');
        let mansetData = document.getElementById('mansetData');

        createButton.addEventListener('click', (e) => {
            e.preventDefault();
            if (window.cropper) {
                mansetData.value = JSON.stringify(window.cropper.getData());
            }

            createForm.submit();
        });
    </script>
    <script>
        CKEDITOR.replace('body');

        let photo = document.querySelector('input[name="photo"]');
        let imageView = document.getElementById('imageView');
        let removeImage = document.querySelector('#imageView button');
        photo.addEventListener('change', (e) => {
            console.log("saa");
            let reader = new FileReader();
            reader.onload = function () {
                let output = document.querySelector('#imageView img');
                output.src = reader.result;
                imageView.style.display = "block";
            };
            reader.readAsDataURL(e.target.files[0]);

        });

        removeImage.addEventListener('click', (e) => {
            e.preventDefault();
            imageView.style.display = "none";
            photo.value = "";
        });

        // Manset
        let photoManset = document.querySelector('input[name="mansetPhoto"]');
        let imageViewManset = document.getElementById('mansetImageView');
        let removeImageManset = document.querySelector('#mansetImageView button');
        photoManset.addEventListener('change', (e) => {
            console.log("saa");
            let reader = new FileReader();
            reader.onload = function () {
                let image = document.querySelector('#mansetImageView img');
                image.src = reader.result;
                imageViewManset.style.display = "block";
                window.cropper = new Cropper(image, {
                    dragMode: 'move',
                    aspectRatio: 980 / 400,
                    restore: false,
                    guides: false,
                    center: false,
                    highlight: false,
                    cropBoxMovable: false,
                    cropBoxResizable: false,
                    toggleDragModeOnDblclick: false,

                });
            };
            reader.readAsDataURL(e.target.files[0]);

        });

        removeImageManset.addEventListener('click', (e) => {
            e.preventDefault();
            imageViewManset.style.display = "none";
            photoManset.value = "";
        });
    </script>
@endsection
