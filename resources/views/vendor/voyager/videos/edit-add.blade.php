@php
    if (!isset($dataTypeContent)) $dataTypeContent = null;

    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.css">
    <style>
        label, input, small {
            color: #76838f !important;
        }
    </style>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <div style="margin: 10px">
                        @if ($add)
                            <form action="/yeni-video-ekle" class="form" enctype="multipart/form-data" method="post" id="createForm">
                                @csrf
                                <div class="form-group">
                                    <label for="">Haber kategorisi</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="0">Seçiniz</option>
                                        @foreach(\App\Kategory::orderBy('name')->get() as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Başlık</label>
                                    <input type="text" class="form-control" name="title">
                                </div>

                                <div class="form-group">
                                    <label for="">İçeriği</label>
                                    <textarea name="body" id="body"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Etiketler</label>
                                    <input type="text" name="tags" class="form-control" placeholder="Etiket1, etiket2, etiket3, ...">
                                </div>

                                <div class="form-group">
                                    <label for="">Fotoğraf</label>
                                    <input type="file" name="photo">
                                    <div id="imageView" style="max-width: 600px; margin-top: 10px; padding: 10px; background: #ddd; display: none">
                                        <img src="" alt="" style="max-width: 100%">
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Url/Embed</label>
                                    <input type="text" class="form-control" name="url_embed">
                                </div>

                                <div class="form-group">
                                    <label for="">Video</label>
                                    <input type="file" name="video">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-success" id="createButton">Kaydet</button>
                                </div>
                            </form>
                        @endif
                        @if ($edit)
                            @php $video = $dataTypeContent; @endphp

                            <form action="/yeni-video-guncelle" class="form" enctype="multipart/form-data" method="post" id="createForm">
                                <input type="hidden" name="video_id" value="{{ $video->id }}">
                                @csrf
                                <div class="form-group">
                                    <label for="">Haber kategorisi</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="0">Seçiniz</option>
                                        @foreach(\App\Kategory::orderBy('name')->get() as $cat)
                                            <option value="{{ $cat->id }}" {{ $video->category_id == $cat->id ? 'selected':'' }}>{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Başlık</label>
                                    <input type="text" class="form-control" value="{{ $video->name }}" name="title">
                                </div>

                                <div class="form-group">
                                    <label for="">İçeriği</label>
                                    <textarea name="body" id="body">{!! $video->body !!}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="">Etiketler</label>
                                    <input type="text" name="tags" class="form-control" value="{{ $video->tags }}" placeholder="Etiket1, etiket2, etiket3, ...">
                                </div>

                                <div class="form-group">
                                    <label for="">Fotoğraf</label>
                                    <input type="file" name="photo">
                                    <div id="imageView" style="max-width: 600px; margin-top: 10px; padding: 10px; background: #ddd;">
                                        <img src="{{ asset($video->image) }}" alt="" style="max-width: 100%">
                                        <button class="btn btn-info btn-sm">Fotoğrafı Kaldır</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Url/Embed</label>
                                    <input type="text" value="{{ $video->url_embed }}" class="form-control" name="url_embed">
                                </div>

                                <div class="form-group">
                                    <label for="">Video</label>
                                    <br>
                                    @if ($video->video)
                                        <video src="{{ asset($video->video) }}" controls></video>
                                    @endif
                                    <input type="file" name="video">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-success" id="createButton">Kaydet</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/cke/ckeditor.js?2') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>
    <script>
        let deleteVideo = document.getElementById('deleteVideo');
        deleteVideo.addEventListener('click', (e) => {
            e.preventDefault();

            let r = confirm("Videoyu silmek istediğinizden emin misiniz?");
            if (r == true) {
                window.location.href = e.target.getAttribute('href');
            }
        });

        let createButton = document.getElementById('createButton');
        let createForm = document.getElementById('createForm');
        let mansetData = document.getElementById('mansetData');

        createButton.addEventListener('click', (e) => {
            e.preventDefault();
            if (window.cropper) {
                mansetData.value = JSON.stringify(window.cropper.getData());
            }

            createForm.submit();
        });
    </script>
    <script>
        CKEDITOR.replace('body');

        let photo = document.querySelector('input[name="photo"]');
        let imageView = document.getElementById('imageView');
        let removeImage = document.querySelector('#imageView button');
        photo.addEventListener('change', (e) => {
            console.log("saa");
            let reader = new FileReader();
            reader.onload = function () {
                let output = document.querySelector('#imageView img');
                output.src = reader.result;
                imageView.style.display = "block";
            };
            reader.readAsDataURL(e.target.files[0]);

        });

        removeImage.addEventListener('click', (e) => {
            e.preventDefault();
            imageView.style.display = "none";
            photo.value = "";
        });


    </script>
@endsection
