@extends('layouts.app')

@section('content')
<div class=manset-2>
    @foreach($topHaberler as $haber)
    <a class=content href="{{ $haber->url() }}">
        <div class=image><img src="{{ $haber->image_url() }}" class="image-center"></div>
        <div class=news-title>{{ $haber->title }}</div>
    </a>
    @endforeach
</div>
<div class=manset-1 data-disabled=false data-id=0>
    <div class=inner>
        @foreach($sliderHaberler as $haber)
        <a class=content href="{{ $haber->url() }}">
            <div class=image><img src="{{ $haber->slider_url() ? $haber->slider_url() : $haber->image_url() }}" class="image-center"></div>
            <div class=title>
                {{ $haber->title }}
            </div>
        </a>
        @endforeach
    </div>
    <div class=controls>
        <div class="control left"><i class="fas fa-chevron-left"></i></div>
        @php
        $i = 1;
        @endphp
        @foreach($sliderHaberler as $haber)
            <div class="control c {{ $i==1 ? 'active':'' }}" data-id={{ $i-1 }}>
                <a href="{{ $haber->url() }}">{{ $i++ }}</a>
            </div>
        @endforeach
        <div class="control right"><i class="fas fa-chevron-right"></i></div>
        <div class=control><a href={{ url('tum-haberler') }}>Tümü</a></div>
    </div>
</div>
<div class=spacer-20></div>
<div class="koseyazarlari scroller" data-disabled=false data-size=3>
    <h3 class=custom-title><span>Köşe Yazarları</span>
        <div class=index>
            <div class=left><i class="fas fa-chevron-left"></i></div>
            <div class=right><i class="fas fa-chevron-right"></i></div>
        </div>
    </h3>
    <div class=inner>
        @include('layouts.koseYazilari')
    </div>
</div>
<div class=spacer-20></div>
<div class=advertisement>
    <script async src=//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js></script>
    <script>(adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-1858947299980333",
            enable_page_level_ads: true
        });</script>
</div>
<div class=section-1>
    <div class=havadurumu>
        <div class=loading>
            <object data=/images/loading.svg type=image/svg+xml></object>
        </div>
        <div class=il>&#x130;stanbul</div>
        <div class=tumu><select id=weatherlist>
                <option value="İl Seçiniz">İl Seçiniz</option>
                <option value="Adana">Adana</option>
                <option value="Ad&#x131;yaman">Ad&#x131;yaman</option>
                <option value="Afyonkarahisar">Afyonkarahisar</option>
                <option value="A&#x11F;r&#x131;">A&#x11F;r&#x131;</option>
                <option value="Amasya">Amasya</option>
                <option value="Ankara">Ankara</option>
                <option value="Antalya">Antalya</option>
                <option value="Artvin">Artvin</option>
                <option value="Ayd&#x131;n">Ayd&#x131;n</option>
                <option value="Bal&#x131;kesir">Bal&#x131;kesir</option>
                <option value="Bilecik">Bilecik</option>
                <option value="Bing&#xF6;l">Bing&#xF6;l</option>
                <option value="Bitlis">Bitlis</option>
                <option value="Bolu">Bolu</option>
                <option value="Burdur">Burdur</option>
                <option value="Bursa">Bursa</option>
                <option value="&#xC7;anakkale">&#xC7;anakkale</option>
                <option value="&#xC7;ank&#x131;r&#x131;">&#xC7;ank&#x131;r&#x131;</option>
                <option value="&#xC7;orum">&#xC7;orum</option>
                <option value="Denizli">Denizli</option>
                <option value="Diyarbak&#x131;r">Diyarbak&#x131;r</option>
                <option value="Edirne">Edirne</option>
                <option value="Elaz&#x131;&#x11F;">Elaz&#x131;&#x11F;</option>
                <option value="Erzincan">Erzincan</option>
                <option value="Erzurum">Erzurum</option>
                <option value="Eski&#x15F;ehir">Eski&#x15F;ehir</option>
                <option value="Gaziantep">Gaziantep</option>
                <option value="Giresun">Giresun</option>
                <option value="G&#xFC;m&#xFC;&#x15F;hane">G&#xFC;m&#xFC;&#x15F;hane</option>
                <option value="Hakk&#xE2;ri">Hakk&#xE2;ri</option>
                <option value="Hatay">Hatay</option>
                <option value="Isparta">Isparta</option>
                <option value="&#x130;&#xE7;el">&#x130;&#xE7;el</option>
                <option value="&#x130;stanbul">&#x130;stanbul</option>
                <option value="&#x130;zmir">&#x130;zmir</option>
                <option value="Kars">Kars</option>
                <option value="Kastamonu">Kastamonu</option>
                <option value="Kayseri">Kayseri</option>
                <option value="K&#x131;rklareli">K&#x131;rklareli</option>
                <option value="K&#x131;r&#x15F;ehir">K&#x131;r&#x15F;ehir</option>
                <option value="Kocaeli">Kocaeli</option>
                <option value="Konya">Konya</option>
                <option value="K&#xFC;tahya">K&#xFC;tahya</option>
                <option value="Malatya">Malatya</option>
                <option value="Manisa">Manisa</option>
                <option value="Kahramanmara&#x15F;">Kahramanmara&#x15F;</option>
                <option value="Mardin">Mardin</option>
                <option value="Mu&#x11F;la">Mu&#x11F;la</option>
                <option value="Mu&#x15F;">Mu&#x15F;</option>
                <option value="Nev&#x15F;ehir">Nev&#x15F;ehir</option>
                <option value="Ni&#x11F;de">Ni&#x11F;de</option>
                <option value="Ordu">Ordu</option>
                <option value="Rize">Rize</option>
                <option value="Sakarya">Sakarya</option>
                <option value="Samsun">Samsun</option>
                <option value="Siirt">Siirt</option>
                <option value="Sinop">Sinop</option>
                <option value="Sivas">Sivas</option>
                <option value="Tekirda&#x11F;">Tekirda&#x11F;</option>
                <option value="Tokat">Tokat</option>
                <option value="Trabzon">Trabzon</option>
                <option value="Tunceli">Tunceli</option>
                <option value="&#x15E;anl&#x131;urfa">&#x15E;anl&#x131;urfa</option>
                <option value="U&#x15F;ak">U&#x15F;ak</option>
                <option value="Van">Van</option>
                <option value="Yozgat">Yozgat</option>
                <option value="Zonguldak">Zonguldak</option>
                <option value="Aksaray">Aksaray</option>
                <option value="Bayburt">Bayburt</option>
                <option value="Karaman">Karaman</option>
                <option value="K&#x131;r&#x131;kkale">K&#x131;r&#x131;kkale</option>
                <option value="Batman">Batman</option>
                <option value="&#x15E;&#x131;rnak">&#x15E;&#x131;rnak</option>
                <option value="Bart&#x131;n">Bart&#x131;n</option>
                <option value="Ardahan">Ardahan</option>
                <option value="I&#x11F;d&#x131;r">I&#x11F;d&#x131;r</option>
                <option value="Yalova">Yalova</option>
                <option value="Karab&#xFC;k">Karab&#xFC;k</option>
                <option value="Kilis">Kilis</option>
                <option value="Osmaniye">Osmaniye</option>
                <option value="D&#xFC;zce">D&#xFC;zce</option>
                <option value="Budape&#x15F;te">Budape&#x15F;te</option>
                <option value="&#x130;slamabad">&#x130;slamabad</option>
                <option value="Budape&#x15F;te">Budape&#x15F;te</option>
                <option value="Ta&#x15F;kent">Ta&#x15F;kent</option>
                <option value="Hakkari">Hakkari</option>
                <!--
                <option value="Meks&#x131;ko">Meks&#x131;ko</option>
                <option value="Gazze">Gazze</option>
                <option value="Utrecht">Utrecht</option>
                <option value="Mersin">Mersin</option>
                <option value="Moskova">Moskova</option>
                <option value="Wash&#x131;ngton">Wash&#x131;ngton</option>
                <option value="St. Petersburg">St. Petersburg</option>
                <option value="Jakarta">Jakarta</option>
                <option value="Pennsylvan&#x131;a">Pennsylvan&#x131;a</option>
                <option value="Yeni Delhi">Yeni Delhi</option>
                <option value="Lefko&#x15F;a">Lefko&#x15F;a</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Frankfurt">Frankfurt</option>
                <option value="Basra">Basra</option>
                <option value="Sofya">Sofya</option>
                <option value="Pri&#x15F;tine">Pri&#x15F;tine</option>
                <option value="Riyad">Riyad</option>
                <option value="Roma">Roma</option>
                <option value="New Jersey">New Jersey</option>
                <option value="Tokyo">Tokyo</option>
                <option value="Pekin">Pekin</option>
                <option value="Br&#xFC;ksel">Br&#xFC;ksel</option>
                <option value="Mexico City">Mexico City</option>
                <option value="Kopenhag">Kopenhag</option>
                <option value="Lima">Lima</option>
                <option value="Kara&#xE7;i">Kara&#xE7;i</option>
                <option value="Bak&#xFC;">Bak&#xFC;</option>
                <option value="Krasnodar">Krasnodar</option>
                <option value="Los Angeles">Los Angeles</option>
                <option value="Tiflis">Tiflis</option>
                <option value="So&#xE7;i">So&#xE7;i</option>
                <option value="Sana">Sana</option>
                <option value="Oslo">Oslo</option>
                <option value="Addis Ababa">Addis Ababa</option>
                <option value="Hamburg">Hamburg</option>
                <option value="Erbil">Erbil</option>
                <option value="Strasbourg">Strasbourg</option>
                <option value="Kuala Lumpur">Kuala Lumpur</option>
                <option value="Stuttgart">Stuttgart</option>
                <option value="Paris">Paris</option>
                <option value="Amman">Amman</option>
                <option value="Hawaii">Hawaii</option>
                <option value="Atina">Atina</option>
                <option value="Dublin">Dublin</option>
                <option value="Londra">Londra</option>
                <option value="Prag">Prag</option>
                <option value="Muskat">Muskat</option>
                <option value="&#x130;dlib">&#x130;dlib</option>
                <option value="Flor&#x131;da">Flor&#x131;da</option>
                <option value="W&#x131;scons&#x131;n">W&#x131;scons&#x131;n</option>
                <option value="Dusseldorf">Dusseldorf</option>
                <option value="Buenos A&#x131;res">Buenos A&#x131;res</option>
                <option value="Cerablus">Cerablus</option>
                <option value="Sidney">Sidney</option>
                <option value="&#x15E;am">&#x15E;am</option>
                <option value="Bras&#x131;l&#x131;a">Bras&#x131;l&#x131;a</option>
                <option value="Berlin">Berlin</option>
                <option value="Dubai">Dubai</option>
                <option value="Tunus">Tunus</option>
                <option value="Toronto">Toronto</option>
                <option value="Colombo">Colombo</option>
                <option value="Var&#x15F;ova">Var&#x15F;ova</option>
                <option value="Man&#x131;la">Man&#x131;la</option>
                <option value="Cenevre">Cenevre</option>
                <option value="Tel Aviv">Tel Aviv</option>
                <option value="California">California</option>
                <option value="K&#xF6;ln">K&#xF6;ln</option>
                <option value="V&#x131;rg&#x131;n&#x131;a">V&#x131;rg&#x131;n&#x131;a</option>
                <option value="Kerbela">Kerbela</option>
                <option value="Kabil">Kabil</option>
                <option value="Sichuan">Sichuan</option>
                <option value="Sr&#x131;lanka">Sr&#x131;lanka</option>
                <option value="Doha">Doha</option>
                <option value="Chongq&#x131;ng">Chongq&#x131;ng</option>
                <option value="Melbourne">Melbourne</option>
                <option value="Mumbai">Mumbai</option>
                <option value="Nanjing">Nanjing</option>
                <option value="Mekke">Mekke</option>
                <option value="Sicilya">Sicilya</option>
                <option value="Salt Lake C&#x131;ty">Salt Lake C&#x131;ty</option>
                <option value="Girne">Girne</option>
                <option value="Gelsen Kirchen">Gelsen Kirchen</option>
                <option value="Melilla">Melilla</option>
                <option value="Breda">Breda</option>
                <option value="Azez">Azez</option>
                <option value="Heidelberg">Heidelberg</option>
                <option value="Lille">Lille</option>
                <option value="Vientiane">Vientiane</option>
                <option value="&#xDC;sk&#xFC;p">&#xDC;sk&#xFC;p</option>
                <option value="Hartum">Hartum</option>
                <option value="Genk">Genk</option>
                <option value="Dakka">Dakka</option>
                <option value="Duhok">Duhok</option>
                <option value="Cezayir">Cezayir</option>
                <option value="Tahran">Tahran</option>
                <option value="Astana">Astana</option>
                <option value="Genth">Genth</option>
                <option value="A&#x15F;kabat">A&#x15F;kabat</option>
                <option value="Barselona">Barselona</option>
                <option value="Guyana">Guyana</option>
                <option value="L&#x131;brev&#x131;lle">L&#x131;brev&#x131;lle</option>
                <option value="Donetsk">Donetsk</option>
                <option value="Saarb&#xFC;chen">Saarb&#xFC;chen</option>
                <option value="New York">New York</option>
                <option value="Kin&#x15F;asa">Kin&#x15F;asa</option>
                <option value="Canberra">Canberra</option>
                <option value="Stockholm">Stockholm</option>
                <option value="Stockholm">Stockholm</option>
                <option value="Valletta">Valletta</option>
                <option value="Palermo">Palermo</option>
                <option value="Singapur">Singapur</option>
                <option value="Utlar Prades">Utlar Prades</option>
                <option value="Edinburg">Edinburg</option>
                <option value="Phnom Penh">Phnom Penh</option>
                <option value="Selanik">Selanik</option>
                <option value="Cidde">Cidde</option>
                <option value="Harare">Harare</option>
                <option value="Kerk&#xFC;k">Kerk&#xFC;k</option>
                <option value="Deyr Ez Zur">Deyr Ez Zur</option>
                <option value="Madrid">Madrid</option>
                <option value="Bochum">Bochum</option>
                <option value="Kud&#xFC;s">Kud&#xFC;s</option>
                <option value="Dortmund">Dortmund</option>
                <option value="Ch&#x131;cago">Ch&#x131;cago</option>
                <option value="Havana">Havana</option>
                <option value="Bamako">Bamako</option>
                <option value="Bat&#x131; &#x15E;eria">Bat&#x131; &#x15E;eria</option>
                <option value="Saraybosna">Saraybosna</option>
                <option value="Tijuana">Tijuana</option>
                <option value="La Paz">La Paz</option>
                <option value="Port Au Pr&#x131;nce">Port Au Pr&#x131;nce</option>
                <option value="Krakow">Krakow</option>
                <option value="Lahor">Lahor</option>
                <option value="San Franc&#x131;sco">San Franc&#x131;sco</option>
                <option value="Vat&#x131;kan">Vat&#x131;kan</option>
                <option value="Amsterdam">Amsterdam</option>
                <option value="Auckland">Auckland</option>
                <option value="K&#x131;r&#x131;m">K&#x131;r&#x131;m</option>
                <option value="Bangkok">Bangkok</option>
                <option value="Mogadi&#x15F;u">Mogadi&#x15F;u</option>
                <option value="Naypyidaw">Naypyidaw</option>
                <option value="Kahire">Kahire</option>
                <option value="Ludw&#x131;gshafen">Ludw&#x131;gshafen</option>
                <option value="L&#x131;ege">L&#x131;ege</option>
                <option value="Ottawa">Ottawa</option>
                <option value="Kiev">Kiev</option>
                <option value="Mannheim">Mannheim</option>
                <option value="Rabat">Rabat</option>
                <option value="Well&#x131;ngton">Well&#x131;ngton</option>
                <option value="Alaska">Alaska</option>
                <option value="Asuncion">Asuncion</option>
                <option value="Karakas">Karakas</option>
                <option value="Ar&#x131;zona">Ar&#x131;zona</option>
                <option value="Manama">Manama</option>
                <option value="Abuja">Abuja</option>
                <option value="Beyrut">Beyrut</option>
                <option value="Teksas">Teksas</option>
                <option value="Arakan">Arakan</option>
                <option value="Yemen">Yemen</option>
                <option value="Milano">Milano</option>
                <option value="Bi&#x15F;kek">Bi&#x15F;kek</option>
                <option value="Ulan Batur">Ulan Batur</option>
                <option value="Santo Dom&#x131;ngo">Santo Dom&#x131;ngo</option>
                <option value="Seul">Seul</option>
                <option value="New Mex&#x131;co">New Mex&#x131;co</option>
                <option value="Erivan">Erivan</option>
                <option value="Ramallah">Ramallah</option>
                <option value="Podgor&#x131;ca">Podgor&#x131;ca</option>
                <option value="Halep">Halep</option>
                <option value="Canderra">Canderra</option>
                <option value="Abu Dabi">Abu Dabi</option>
                <option value="Bogota">Bogota</option>
                <option value="Padang">Padang</option>
                <option value="Viyana">Viyana</option>
                <option value="Sao Paulo">Sao Paulo</option>
                <option value="Juba">Juba</option>
                <option value="Nablus">Nablus</option>
                <option value="Medellin">Medellin</option>
                <option value="Bosna Hersek">Bosna Hersek</option>
                <option value="Ke&#x15F;mir">Ke&#x15F;mir</option>
                <option value="Lizbon">Lizbon</option>
                <option value="M&#x131;ndanao">M&#x131;ndanao</option>
                <option value="Afrin">Afrin</option>
                <option value="Z&#xFC;rih">Z&#xFC;rih</option>
                <option value="Hargeisa">Hargeisa</option>
                <option value="Pyongyang">Pyongyang</option>
                <option value="Nairobi">Nairobi</option>
                <option value="Tel Abyad">Tel Abyad</option>
                <option value="El Halil">El Halil</option>
                <option value="Antananar&#x131;vo">Antananar&#x131;vo</option>
                <option value="Roterdam">Roterdam</option>
                <option value="Marake&#x15F;">Marake&#x15F;</option>
                <option value="Malta">Malta</option>
                <option value="Belgrat">Belgrat</option>
                <option value="Trablus">Trablus</option>
                <option value="Bratislava">Bratislava</option>
                <option value="M&#xFC;nbi&#xE7;">M&#xFC;nbi&#xE7;</option>
                <option value="Da&#x11F;&#x131;stan">Da&#x11F;&#x131;stan</option>
                <option value="Ramadi">Ramadi</option>
                <option value="Wuhan">Wuhan</option>
                <option value="Santiago">Santiago</option>
                <option value="Katmandu">Katmandu</option>
                <option value="Musul">Musul</option>
                <option value="Kuzey Carol&#x131;na">Kuzey Carol&#x131;na</option>
                <option value="Taipei">Taipei</option>
                <option value="R&#x131;o De Janer&#x131;o">R&#x131;o De Janer&#x131;o</option>
                <option value="Orlando">Orlando</option>
                <option value="&#x15E;anghay">&#x15E;anghay</option>
                <option value="Prizren">Prizren</option>
                <option value="Boston">Boston</option>
                <option value="Heylongciang">Heylongciang</option>
                <option value="Kentucky">Kentucky</option>
                <option value="Pretor&#x131;a">Pretor&#x131;a</option>
                <option value="Johannesburg">Johannesburg</option>
                <option value="Lyon">Lyon</option>
                <option value="San Salvador">San Salvador</option>
                <option value="Niamey">Niamey</option>
                <option value="Ba&#x11F;dat">Ba&#x11F;dat</option>
                <option value="Yangon">Yangon</option>
                <option value="Malavi">Malavi</option>
                <option value="Ondurman">Ondurman</option>
                <option value="Qu&#x131;to">Qu&#x131;to</option>
                <option value="Gazima&#x11F;osa">Gazima&#x11F;osa</option>
                <option value="Managua">Managua</option>
                <option value="Ncamena">Ncamena</option>
                <option value="Nantes">Nantes</option>
                <option value="Davos">Davos</option>
                <option value="Bern">Bern</option>
                <option value="Lazkiye">Lazkiye</option>
                <option value="Aachen">Aachen</option>
                <option value="Rakka">Rakka</option>
                <option value="Kuveyt">Kuveyt</option>
                <option value="Lahey">Lahey</option>
                <option value="Helsinki">Helsinki</option>
                <option value="Belgrad">Belgrad</option>
                <option value="B&#xFC;kre&#x15F;">B&#xFC;kre&#x15F;</option>
                <option value="Odessa">Odessa</option>
                <option value="Anvers">Anvers</option>
                <option value="Queensland">Queensland</option>
                <option value="Belfast">Belfast</option>
                <option value="Michigan">Michigan</option>
                <option value="Karlsruhe">Karlsruhe</option>
                <option value="Zagreb">Zagreb</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Mallorca">Mallorca</option>
                <option value="&#x15E;ibirgan">&#x15E;ibirgan</option>
                <option value="Cape Town">Cape Town</option>
                <option value="Zahedan">Zahedan</option>
                <option value="Akra">Akra</option>
                <option value="M&#xFC;nih">M&#xFC;nih</option>
                <option value="Br&#x131;sbane">Br&#x131;sbane</option>
                <option value="Maldivler">Maldivler</option>
                <option value="Rhode Island">Rhode Island</option>
                <option value="Hama">Hama</option>
                -->
            </select></div>
        <div class=content>
            <div class=image>
                <img width="50" src="http://openweathermap.org/img/w/01n.png" alt="">
            </div>
            <div class=title><span class=description>hafif ya&#x11F;mur</span>
                <div class=subtitle><span class=temp>3,5</span> °C</div>
            </div>
        </div>
    </div>
    <div class=piyasa>

        <table>
            <tr>
                <td>DOLAR
                <td>{{ number_format($piyasa[6]->ALIS, 4) }}
                <td class={{ $piyasa[6]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[6]->YUZDEDEGISIM }}
                    <i class="fas fa-chevron-{{ $piyasa[6]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                <td>EURO
                <td>{{ number_format($piyasa[3]->ALIS, 4) }}
                <td class={{ $piyasa[3]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[3]->YUZDEDEGISIM }}
                    <i class="fas fa-chevron-{{ $piyasa[3]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                <td>BİST
                <td>{{ number_format($piyasa[1]->KAPANIS) }}
                <td class={{ $piyasa[1]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[1]->YUZDEDEGISIM }}
                    <i class="fas fa-chevron-{{ $piyasa[1]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                <td>ALTIN
                <td>{{ number_format($piyasa[5]->ALIS, 4) }}
                <td class={{ $piyasa[5]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[5]->YUZDEDEGISIM }}
                    <i class="fas fa-chevron-{{ $piyasa[5]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
        </table>
        <div class=advertisement><a href="//www.kizilay.org.tr/" target=_blank><img alt=kizilay_banner_728X090
                                                                                    src=//www.kizilay.org.tr/Upload/Banner/Sponsor/95438057_kizilay_banner_728x090.gif></a>
        </div>
    </div>
</div>
<div class=spacer-20></div>
<div class=section-2>
    <div class=leftside><h3 class=custom-title><span class=green>Spor</span></h3>
        <div class="manset-1 style-2" data-disabled=false data-id=0>
            <div class=inner>
                @foreach($sporHaberleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img src="{{ $haber->image_url() }}" class="image-center">
                    </div>
                    <div class=title>
                        {{ $haber->title }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=controls>
                <div class="control left"><i class="fas fa-chevron-left"></i></div>
                @php
                    $i = 1;
                @endphp
                @foreach($sporHaberleri as $haber)
                <div class="control c {{ $i==1?'active':'' }}" data-id={{ $i-1 }}>
                    <a href={{ $haber->url() }}>{{ $i++ }}</a>
                </div>
                @endforeach

                <div class="control right"><i class="fas fa-chevron-right"></i></div>
                <div class=control>Tümü</div>
            </div>
        </div>
        <div class=spacer-20></div>
        <div class="gununmansetleri box-2 scroller" data-disabled=false data-size=5>
            <h3 class=custom-title><a href=/gazeteler> <span>Günün Manşetleri</span> </a>
                <div class=index>
                    <div class=left><i class="fas fa-chevron-left"></i></div>
                    <div class=right><i class="fas fa-chevron-right"></i></div>
                </div>
            </h3>
            <div class=inner>
                @foreach($gazeteler as $gazete)
                <a href="{{ $gazete->url() }}" class=content>
                    <div class=title>{{ $gazete->name }}</div>
                    <div class=image><img class=lazy data-src="{{ Voyager::Image($gazete->thumbnail('kucuk')) }}"></div>
                </a>
                @endforeach
            </div>
        </div>
        <div class=spacer-20></div>
        <div class=kategorihaberleri><h3 class=custom-title><span> Editörün Seçtikleri </span></h3>
            <div class=box-1>
                @foreach($editorHaberleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img src="{{ $haber->image_url() }}" class="lazy image-center">
                        <div class=category>

                            {{ $haber->kategori() ? $haber->kategori()->name : '' }}
                        </div>
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                    <div class=summary>
                        {{ $haber->summary }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-5></div>
        <div class=kategorihaberleri><h3 class=custom-title><span> Ekonomi </span></h3>
            <div class=box-1>
                @foreach($ekonomiHaberleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img src="{{ $haber->image_url() }}" class="lazy image-center">
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                    <div class=summary>
                        {{ $haber->summary }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-5></div>
    </div>
    <aside>
        <div class=search>
            <form action=/haberler><h3 class=custom-title><span>Haberlerde Ara</span></h3>
                <div class=content><input type=search name=search placeholder="Arama Yapmak İçin Tıklayın">
                    <button><i class="fas fa-search"></i></button>
                    <div class=clearfix></div>
                </div>
            </form>
        </div>
        <div class=spacer-20></div>
        <div class=advertisement>
            <img src="{{ Voyager::Image('otherUploads/XimBtjfs2PoELhwzbSv5w29NuiKveL6MXTq27p3p.gif') }}">
        </div>
        @include('layouts.futbol')
        <div class=gununkarikaturu><h3 class=custom-title><span>Günün Karikatürü</span></h3>
            <div class=content>
                <img src="{{ Voyager::Image(setting('site.karikatur')) }}" class="lazy">
            </div>
        </div>
        <div class=spacer-20></div>
    </aside>
    <div class=clearfix></div>
</div>
<div class=section-2>
    <div class=leftside>
        <div class=kategorihaberleri><h3 class=custom-title><span> Politika </span></h3>
            <div class=box-1>
                @foreach($politikaHaberleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img src="{{ $haber->image_url() }}" class="lazy image-center">
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                    <div class=summary>
                        {{ $haber->summary }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-5></div>
        <div class=kategorihaberleri><h3 class=custom-title><span> Dünya </span></h3>
            <div class=box-1>
                @foreach($dunyaHaberleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img src="{{ $haber->image_url() }}" class="lazy image-center">
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                    <div class=summary>
                        {{ $haber->summary }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-5></div>
        <h3 class=custom-title><span>Foto Galeri</span></h3>
        <div class="manset-1 style-3" data-disabled=false data-id=0>
            <div class=inner>
                @foreach($fotoGaleriler as $galeri)
                <a class=content href="{{ $galeri->url() }}">
                    <div class=image>
                        <img src="{{ Voyager::Image($galeri->thumbnail('cropped', 'kapak')) }}" class="image-center lazy">
                    </div>
                    <div class=title>
                        {{ $galeri->name }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=controls>
                @php
                $i = 0;
                @endphp
                @foreach($fotoGaleriler as $galeri)
                <div class="control c {{ $i == 0 ? 'active' : '' }}" data-id="{{ $i++ }}">
                    <a href="{{ $galeri->url() }}">
                        <img src="{{ Voyager::Image($galeri->thumbnail('cropped', 'kapak')) }}" class="lazy image-center">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class=spacer-20></div>
        <div class=kategorihaberleri><h3 class=custom-title><span> Video Galeri </span></h3>
            <div class=box-1>
                @foreach($videolar as $video)
                    <a class="content" href="{{ $video->url() }}">
                        <div class="image">
                            <img class="lazy image-center" data-src="{{ Voyager::Image($video->image) }}" style="display: block;">
                        </div>
                        <div class="news-title">
                            {{ $video->name }}
                        </div>
                    </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-5></div>
        <div class=yerelhaberler><h3 class=custom-title><span>Yerel Haberler</span></h3>
            <div class=box-1>
                @foreach($yerelHaberler3 as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img data-src="{{ $haber->image_url() }}" class="lazy image-center">
                        <div class=category>{{ $haber->sehir }}</div>
                    </div>
                    <div class=news-title>
                        {{ $haber->titleShort() }}
                    </div>
                    <div class=summary></div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
            <div class=spacer-5></div>
            <div class=box-5>
                @foreach($yerelHaberlerDigerleri as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=date>{{ date("H:i", strtotime($haber->created_at)) }}</div>
                    <div class=category><span>{{ $haber->sehir }}</span></div>
                    <div class=title>{{ $haber->title }}</div>
                    <div class=arrow><i class="far fa-arrow-alt-circle-right"></i></div>
                </a>
                @endforeach
            </div>
            <div class=clearfix></div>
        </div>
        <div class=spacer-20></div>
    </div>
    <aside>
        <div class=encokyorumlananlar><h3 class=custom-title><span>En Çok Okunanlar</span></h3>
            <div class=box-3>
                @foreach($cokOkunanHaberler as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img class=image-center
                        src="{{ $haber->image_url() }}">
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                </a>
                @endforeach
            </div>
            <div class=spacer-10></div>
        </div>
        <div class=encokyorumlananlar><h3 class=custom-title><span>En Çok Yorumlananlar</span></h3>
            <div class=box-3>
                @foreach($cokYorumlananHaberler as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img class=image-center
                        src="{{ $haber->image_url() }}">
                    </div>
                    <div class=news-title>
                        {{ $haber->title }}
                    </div>
                </a>
                @endforeach
            </div>
        </div>
        <div class=spacer-10></div>
    </aside>
</div>
@stop
