@extends('layouts.app')

@section('content')
<div class="section-2">
    <div class="leftside">
    	<div class="news">
		    <h1 class="title">{{ $sayfa->title }}</h1>
		    <div class="spacer-20"></div>
		    <ol class="breadcrumb">
		        <li><a href="/">Anasayfa</a></li>
		        <li class="active">{{ $sayfa->title }}</li>
		    </ol>
		    <div class="spacer-20"></div>
		    <div class="text">
	        	{!! $sayfa->body !!}
		    </div>
		</div>
    </div>
	@include("layouts.aside")
</div>
@stop