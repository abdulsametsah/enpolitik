@extends("mobil.template")

@section("content")
    <div class="content-container animated fadeInUp"><h3 class="widget-title"><span>{{ $title }}</span></h3>
        <div class="widget-box">
            @foreach($galeriler as $galeri)
                <a href="{{ $galeri->url()  }}"
                   class="small-listing-item">
                    <div class="entry-thumb"><img class="lazy"
                                                  data-src="{{ Voyager::Image($galeri->thumbnail('cropped', "kapak")) }}"
                                                  alt="{{ $galeri->name }}">
                    </div>
                    <div class="entry-content"><h2>{{ $galeri->name  }}</h2></div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
        <div class="pagination">
            {{ $galeriler->links()  }}
        </div>
    </div>
@stop