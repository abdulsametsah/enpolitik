@extends("mobil.template")

@section('content')
    <div class="content-container animated fadeInUp">
        <div class="entry-main">
            <h1 class="entry-title">
                {{ $sayfa->title }}
            </h1>
            <div class="entry-content">
                {!! $sayfa->body !!}
            </div>
        </div>
    </div>
@stop