@extends("mobil.template")

@section("content")
    <div class="content-container animated fadeInUp"><h3 class="widget-title"><span>{{ $title }}</span></h3>
        <div class="widget-box">
            @foreach($haberler as $haber)
                <a href="{{ $haber->url()  }}"
                   class="small-listing-item">
                    <div class="entry-thumb"><img class="lazy"
                                                  data-src="{{ $haber->image_url() }}"
                                                  alt="{{ $haber->title }}">
                    </div>
                    <div class="entry-content"><h2>{{ $haber->title  }}</h2></div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
        <div class="pagination">
            {{ $haberler->links()  }}
        </div>
    </div>
@stop