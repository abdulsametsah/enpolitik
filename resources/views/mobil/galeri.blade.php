@extends("mobil.template")

@section("content")
    <div class="featured-gallery-slider animated fadeInRight">
        @foreach($galeri->photos() as $photo)
            <div class="featured-item">
                <div class="thumb"><img src="{{ Voyager::Image($photo->img) }}" style="width: 100%">
                    <div class="caption">
                        {{ $photo->desc }}
                    </div>
                </div>
                <div class="fullscreen">
                    <a href="{{ Voyager::Image($photo->img) }}" class="swipebox" title="{{ $photo->desc }}">
                        <i class="fa fa-expand"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="content-container animated fadeInUp">
        <div class="entry-main">
            <h1 class="entry-title">{{ $galeri->name }}</h1>
            <div class="entry-meta"><span class="date"> {{ date('d.m.Y H:i', strtotime($galeri->created_at)) }} </span></div>
            <div class="entry-footer">
                <div class="social-share"><a class="facebook" target="_blank"
                                             href="//www.facebook.com/sharer.php?u={{ $galeri->url() }}">
                        <i class="fab fa-facebook-square"></i> </a> <a class="twitter" target="_blank"
                                                                       href="//twitter.com/intent/tweet?url={{ $galeri->url() }}&amp;text={{ $galeri->name }}">
                        <i class="fab fa-twitter-square"></i> </a> <a class="gplus" target="_blank"
                                                                      href="//plus.google.com/share?url={{ $galeri->url() }}&amp;text={{ $galeri->name }}&amp;hl=tr">
                        <i class="fab fa-google-plus-square"></i> </a> <a class="whatsapp"
                                                                          href="whatsapp://send?text={{ $galeri->name }}: {{ $galeri->url() }}">
                        <i class="fab fa-whatsapp-square"></i> </a></div>
            </div>
            <div class="entry-content">
                {!! $galeri->body !!}
            </div>
        </div>
        <div class="widget-box">
            @foreach($sonHaberler as $haber)
                <a href="{{ $haber->url()  }}" class="small-listing-item">
                    <div class="entry-thumb">
                        <img src="{{ Voyager::Image($haber->thumbnail('yorum')) }}" alt="{{ $haber->title }}”">
                    </div>
                    <div class="entry-content">
                        <h2>{{ $haber->title }}</h2>
                    </div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
@stop