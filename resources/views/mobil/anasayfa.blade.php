@extends("mobil.template")

@section('content')
    <div class="news-slider owl-carousel manset-1">
        @foreach($sliderHaberler as $haber)
            <a class=news-slider-item href="{{ $haber->url() }}">
                <img class="responsive-image owl-lazy" data-src="{{ $haber->slider_url() ? $haber->slider_url() : $haber->image_url() }}" alt="{{ $haber->title }}">
                <h3>{{ $haber->title }}</h3>
            </a>
        @endforeach
    </div>
    <div class="content-container animated fadeInUp">
        <div class=koseyazarlari>
            <h3 class=widget-title>
                <span>Köşe Yazarları</span> <a href=/mobil/yazarlar class=index> <i class="fa fa-list-ul"></i> </a>
            </h3>
            <div class="scroller owl-carousel arrow-middle">
                @include("layouts.koseYazilari")
            </div>
        </div>
        <div class=spacer-20></div>
        <div class="manset-2 owl-carousel arrow-middle">
            @foreach($topHaberler as $haber)
                <a class=content href="{{ $haber->url() }}">
                    <div class=image>
                        <img data-src="{{ $haber->image_url() }}" class="image-center owl-lazy">
                    </div>
                    <div class=news-title>
                        {{ $haber->titleShort() }}
                    </div>
                </a>
            @endforeach
        </div>
        <div class=row>
            <div class="col s12 m6">
                <div class=havadurumu>
                    <div class=loading><object data=/images/loading.svg type=image/svg+xml></object></div>
                    <div class=il>&#xC7;ank&#x131;r&#x131;</div>
                    <div class=tumu>
                        <select id=weatherlist>
                            <option value=18>İl Seçiniz
                            <option value=1>Adana
                            <option value=2>Ad&#x131;yaman
                            <option value=3>Afyonkarahisar
                            <option value=4>A&#x11F;r&#x131;
                            <option value=5>Amasya
                            <option value=6>Ankara
                            <option value=7>Antalya
                            <option value=8>Artvin
                            <option value=9>Ayd&#x131;n
                            <option value=10>Bal&#x131;kesir
                            <option value=11>Bilecik
                            <option value=12>Bing&#xF6;l
                            <option value=13>Bitlis
                            <option value=14>Bolu
                            <option value=15>Burdur
                            <option value=16>Bursa
                            <option value=17>&#xC7;anakkale
                            <option value=18>&#xC7;ank&#x131;r&#x131;
                            <option value=19>&#xC7;orum
                            <option value=20>Denizli
                            <option value=21>Diyarbak&#x131;r
                            <option value=22>Edirne
                            <option value=23>Elaz&#x131;&#x11F;
                            <option value=24>Erzincan
                            <option value=25>Erzurum
                            <option value=26>Eski&#x15F;ehir
                            <option value=27>Gaziantep
                            <option value=28>Giresun
                            <option value=29>G&#xFC;m&#xFC;&#x15F;hane
                            <option value=30>Hakk&#xE2;ri
                            <option value=31>Hatay
                            <option value=32>Isparta
                            <option value=33>&#x130;&#xE7;el
                            <option value=34>&#x130;stanbul
                            <option value=35>&#x130;zmir
                            <option value=36>Kars
                            <option value=37>Kastamonu
                            <option value=38>Kayseri
                            <option value=39>K&#x131;rklareli
                            <option value=40>K&#x131;r&#x15F;ehir
                            <option value=41>Kocaeli
                            <option value=42>Konya
                            <option value=43>K&#xFC;tahya
                            <option value=44>Malatya
                            <option value=45>Manisa
                            <option value=46>Kahramanmara&#x15F;
                            <option value=47>Mardin
                            <option value=48>Mu&#x11F;la
                            <option value=49>Mu&#x15F;
                            <option value=50>Nev&#x15F;ehir
                            <option value=51>Ni&#x11F;de
                            <option value=52>Ordu
                            <option value=53>Rize
                            <option value=54>Sakarya
                            <option value=55>Samsun
                            <option value=56>Siirt
                            <option value=57>Sinop
                            <option value=58>Sivas
                            <option value=59>Tekirda&#x11F;
                            <option value=60>Tokat
                            <option value=61>Trabzon
                            <option value=62>Tunceli
                            <option value=63>&#x15E;anl&#x131;urfa
                            <option value=64>U&#x15F;ak
                            <option value=65>Van
                            <option value=66>Yozgat
                            <option value=67>Zonguldak
                            <option value=68>Aksaray
                            <option value=69>Bayburt
                            <option value=70>Karaman
                            <option value=71>K&#x131;r&#x131;kkale
                            <option value=72>Batman
                            <option value=73>&#x15E;&#x131;rnak
                            <option value=74>Bart&#x131;n
                            <option value=75>Ardahan
                            <option value=76>I&#x11F;d&#x131;r
                            <option value=77>Yalova
                            <option value=78>Karab&#xFC;k
                            <option value=79>Kilis
                            <option value=80>Osmaniye
                            <option value=81>D&#xFC;zce
                            <option value=82>Budape&#x15F;te
                            <option value=83>&#x130;slamabad
                            <option value=84>Budape&#x15F;te
                            <option value=85>Ta&#x15F;kent
                            <option value=86>Hakkari
                            <option value=87>Meks&#x131;ko
                            <option value=88>Gazze
                            <option value=89>Utrecht
                            <option value=90>Mersin
                            <option value=91>Moskova
                            <option value=92>Wash&#x131;ngton
                            <option value=93>St. Petersburg
                            <option value=94>Jakarta
                            <option value=95>Pennsylvan&#x131;a
                            <option value=96>Yeni Delhi
                            <option value=97>Lefko&#x15F;a
                            <option value=98>Hong Kong
                            <option value=99>Frankfurt
                            <option value=100>Basra
                            <option value=101>Sofya
                            <option value=102>Pri&#x15F;tine
                            <option value=103>Riyad
                            <option value=104>Roma
                            <option value=105>New Jersey
                            <option value=106>Tokyo
                            <option value=107>Pekin
                            <option value=108>Br&#xFC;ksel
                            <option value=109>Mexico City
                            <option value=110>Kopenhag
                            <option value=111>Lima
                            <option value=112>Kara&#xE7;i
                            <option value=113>Bak&#xFC;
                            <option value=114>Krasnodar
                            <option value=115>Los Angeles
                            <option value=116>Tiflis
                            <option value=117>So&#xE7;i
                            <option value=118>Sana
                            <option value=119>Oslo
                            <option value=120>Addis Ababa
                            <option value=121>Hamburg
                            <option value=122>Erbil
                            <option value=123>Strasbourg
                            <option value=124>Kuala Lumpur
                            <option value=125>Stuttgart
                            <option value=126>Paris
                            <option value=127>Amman
                            <option value=128>Hawaii
                            <option value=129>Atina
                            <option value=130>Dublin
                            <option value=131>Londra
                            <option value=132>Prag
                            <option value=133>Muskat
                            <option value=134>&#x130;dlib
                            <option value=135>Flor&#x131;da
                            <option value=136>W&#x131;scons&#x131;n
                            <option value=137>Dusseldorf
                            <option value=138>Buenos A&#x131;res
                            <option value=139>Cerablus
                            <option value=140>Sidney
                            <option value=141>&#x15E;am
                            <option value=142>Bras&#x131;l&#x131;a
                            <option value=143>Berlin
                            <option value=144>Dubai
                            <option value=145>Tunus
                            <option value=146>Toronto
                            <option value=147>Colombo
                            <option value=148>Var&#x15F;ova
                            <option value=149>Man&#x131;la
                            <option value=150>Cenevre
                            <option value=151>Tel Aviv
                            <option value=152>California
                            <option value=153>K&#xF6;ln
                            <option value=154>V&#x131;rg&#x131;n&#x131;a
                            <option value=155>Kerbela
                            <option value=156>Kabil
                            <option value=157>Sichuan
                            <option value=158>Sr&#x131;lanka
                            <option value=159>Doha
                            <option value=160>Chongq&#x131;ng
                            <option value=161>Melbourne
                            <option value=162>Mumbai
                            <option value=163>Nanjing
                            <option value=164>Mekke
                            <option value=165>Sicilya
                            <option value=166>Salt Lake C&#x131;ty
                            <option value=167>Girne
                            <option value=168>Gelsen Kirchen
                            <option value=169>Melilla
                            <option value=170>Breda
                            <option value=171>Azez
                            <option value=172>Heidelberg
                            <option value=173>Lille
                            <option value=174>Vientiane
                            <option value=175>&#xDC;sk&#xFC;p
                            <option value=176>Hartum
                            <option value=177>Genk
                            <option value=178>Dakka
                            <option value=179>Duhok
                            <option value=180>Cezayir
                            <option value=181>Tahran
                            <option value=182>Astana
                            <option value=183>Genth
                            <option value=184>A&#x15F;kabat
                            <option value=185>Barselona
                            <option value=186>Guyana
                            <option value=187>L&#x131;brev&#x131;lle
                            <option value=188>Donetsk
                            <option value=189>Saarb&#xFC;chen
                            <option value=190>New York
                            <option value=191>Kin&#x15F;asa
                            <option value=192>Canberra
                            <option value=193>Stockholm
                            <option value=194>Stockholm
                            <option value=195>Valletta
                            <option value=196>Palermo
                            <option value=197>Singapur
                            <option value=198>Utlar Prades
                            <option value=199>Edinburg
                            <option value=200>Phnom Penh
                            <option value=201>Selanik
                            <option value=202>Cidde
                            <option value=203>Harare
                            <option value=204>Kerk&#xFC;k
                            <option value=205>Deyr Ez Zur
                            <option value=206>Madrid
                            <option value=207>Bochum
                            <option value=208>Kud&#xFC;s
                            <option value=209>Dortmund
                            <option value=210>Ch&#x131;cago
                            <option value=211>Havana
                            <option value=212>Bamako
                            <option value=213>Bat&#x131; &#x15E;eria
                            <option value=214>Saraybosna
                            <option value=215>Tijuana
                            <option value=216>La Paz
                            <option value=217>Port Au Pr&#x131;nce
                            <option value=218>Krakow
                            <option value=219>Lahor
                            <option value=220>San Franc&#x131;sco
                            <option value=221>Vat&#x131;kan
                            <option value=222>Amsterdam
                            <option value=223>Auckland
                            <option value=224>K&#x131;r&#x131;m
                            <option value=225>Bangkok
                            <option value=226>Mogadi&#x15F;u
                            <option value=227>Naypyidaw
                            <option value=228>Kahire
                            <option value=229>Ludw&#x131;gshafen
                            <option value=230>L&#x131;ege
                            <option value=231>Ottawa
                            <option value=232>Kiev
                            <option value=233>Mannheim
                            <option value=234>Rabat
                            <option value=235>Well&#x131;ngton
                            <option value=236>Alaska
                            <option value=237>Asuncion
                            <option value=238>Karakas
                            <option value=239>Ar&#x131;zona
                            <option value=240>Manama
                            <option value=241>Abuja
                            <option value=242>Beyrut
                            <option value=243>Teksas
                            <option value=244>Arakan
                            <option value=245>Yemen
                            <option value=246>Milano
                            <option value=247>Bi&#x15F;kek
                            <option value=248>Ulan Batur
                            <option value=249>Santo Dom&#x131;ngo
                            <option value=250>Seul
                            <option value=251>New Mex&#x131;co
                            <option value=252>Erivan
                            <option value=253>Ramallah
                            <option value=254>Podgor&#x131;ca
                            <option value=255>Halep
                            <option value=256>Canderra
                            <option value=257>Abu Dabi
                            <option value=258>Bogota
                            <option value=259>Padang
                            <option value=260>Viyana
                            <option value=261>Sao Paulo
                            <option value=262>Juba
                            <option value=263>Nablus
                            <option value=264>Medellin
                            <option value=265>Bosna Hersek
                            <option value=266>Ke&#x15F;mir
                            <option value=267>Lizbon
                            <option value=268>M&#x131;ndanao
                            <option value=269>Afrin
                            <option value=270>Z&#xFC;rih
                            <option value=271>Hargeisa
                            <option value=272>Pyongyang
                            <option value=273>Nairobi
                            <option value=274>Tel Abyad
                            <option value=275>El Halil
                            <option value=276>Antananar&#x131;vo
                            <option value=277>Roterdam
                            <option value=278>Marake&#x15F;
                            <option value=279>Malta
                            <option value=280>Belgrat
                            <option value=281>Trablus
                            <option value=282>Bratislava
                            <option value=283>M&#xFC;nbi&#xE7;
                            <option value=284>Da&#x11F;&#x131;stan
                            <option value=285>Ramadi
                            <option value=286>Wuhan
                            <option value=287>Santiago
                            <option value=288>Katmandu
                            <option value=289>Musul
                            <option value=290>Kuzey Carol&#x131;na
                            <option value=291>Taipei
                            <option value=292>R&#x131;o De Janer&#x131;o
                            <option value=293>Orlando
                            <option value=294>&#x15E;anghay
                            <option value=295>Prizren
                            <option value=296>Boston
                            <option value=297>Heylongciang
                            <option value=298>Kentucky
                            <option value=299>Pretor&#x131;a
                            <option value=300>Johannesburg
                            <option value=301>Lyon
                            <option value=302>San Salvador
                            <option value=303>Niamey
                            <option value=304>Ba&#x11F;dat
                            <option value=305>Yangon
                            <option value=306>Malavi
                            <option value=307>Ondurman
                            <option value=308>Qu&#x131;to
                            <option value=309>Gazima&#x11F;osa
                            <option value=310>Managua
                            <option value=311>Ncamena
                            <option value=312>Nantes
                            <option value=313>Davos
                            <option value=314>Bern
                            <option value=315>Lazkiye
                            <option value=316>Aachen
                            <option value=317>Rakka
                            <option value=318>Kuveyt
                            <option value=319>Lahey
                            <option value=320>Helsinki
                            <option value=321>Belgrad
                            <option value=322>B&#xFC;kre&#x15F;
                            <option value=323>Odessa
                            <option value=324>Anvers
                            <option value=325>Queensland
                            <option value=326>Belfast
                            <option value=327>Michigan
                            <option value=328>Karlsruhe
                            <option value=329>Zagreb
                            <option value=330>Rajasthan
                            <option value=331>Mallorca
                            <option value=332>&#x15E;ibirgan
                            <option value=333>Cape Town
                            <option value=334>Zahedan
                            <option value=335>Akra
                            <option value=336>M&#xFC;nih
                            <option value=337>Br&#x131;sbane
                            <option value=338>Maldivler
                            <option value=339>Rhode Island
                            <option value=340>Hama
                            <option value=341>Dakar
                            <option value=342>Hanoi
                            <option value=343>Ki&#x15F;inev
                            <option value=344>&#x15E;arm El &#x15E;eyh
                            <option value=345>Port Moresby
                            <option value=346>Lou&#x131;s&#x131;ana
                            <option value=347>Cenin
                            <option value=348>Tiran
                            <option value=349>Maryland
                            <option value=350>Burgaz
                        </select>
                    </div>
                    <div class=content>
                        <div class=image><i class="wi wi-day-sunny"></i></div>
                        <div class=title>
                            <span class=description>a&#xE7;&#x131;k</span>
                            <div class=subtitle><span class=temp>-7,9</span> °C</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6">
                <div class=piyasa>
                    <table>
                        <tr>
                            <td>DOLAR
                            <td>{{ number_format($piyasa[6]->ALIS, 4) }}
                            <td class={{ $piyasa[6]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[6]->YUZDEDEGISIM }}
                                <i class="fas fa-chevron-{{ $piyasa[6]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                        </tr>
                        <tr>
                            <td>EURO
                            <td>{{ number_format($piyasa[3]->ALIS, 4) }}
                            <td class={{ $piyasa[3]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[3]->YUZDEDEGISIM }}
                                <i class="fas fa-chevron-{{ $piyasa[3]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                        </tr>
                        <tr>
                            <td>BİST
                            <td>{{ number_format($piyasa[1]->KAPANIS) }}
                            <td class={{ $piyasa[1]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[1]->YUZDEDEGISIM }}
                                <i class="fas fa-chevron-{{ $piyasa[1]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                        </tr>
                        <tr>
                            <td>ALTIN
                            <td>{{ number_format($piyasa[5]->ALIS, 4) }}
                            <td class={{ $piyasa[5]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}>%{{ $piyasa[5]->YUZDEDEGISIM }}
                                <i class="fas fa-chevron-{{ $piyasa[5]->YUZDEDEGISIM > 0 ? 'up' : 'down' }}"></i>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class=widget-box>
            <h3 class=widget-title>
                <span>Spor</span>
                <a href=/haberler/spor class=index> <i class="fa fa-list-ul"></i> </a>
            </h3>
            <div class="news-slider owl-carousel">
                @foreach($sporHaberleri as $haber)
                    <a class=news-slider-item href="{{ $haber->url() }}">
                        <img class="responsive-image owl-lazy" data-src="{{ $haber->image_url() }}" alt="{{ $haber->title }}">
                        <h3>{{ $haber->title }}</h3>
                    </a>
                @endforeach
            </div>
        </div>
        <div class=ligtablosu>
            <div class=loading><object data=/images/loading.svg type=image/svg+xml></object></div>
            <h3 class=widget-title><span class=green>Lig Tablosu</span></h3>
            <div class=select-container>
                <select id=leagues>
                    <option value=0>Spor Toto S&#xFC;per Lig
                </select>
            </div>
            <ul class=tabs>
                <li class="tab col s3"><a class=active href=#table>Puan Durumu</a>
                <li class="tab col s3"><a href=#fixture>Fikstür</a>
            </ul>
            <div id=table class="col s12">
                <table class=ltable>
                    <tr>
                        <th>Takım</th>
                        <th>O</th>
                        <th>G</th>
                        <th>B</th>
                        <th>M</th>
                        <th>AV</th>
                        <th>P</th>

                    @php
                        $futbol = new \App\Http\Controllers\FutbolController();
                        $i = 1;
                    @endphp
                    @foreach($futbol->getPoints() as $takim)
                    <tr>
                        <td>{{ $i++ }} {{ $takim['takim'] }}</td>
                        <td>{{ $takim['o'] }}</td>
                        <td>{{ $takim['g'] }}</td>
                        <td>{{ $takim['b'] }}</td>
                        <td>{{ $takim['m'] }}</td>
                        <td>{{ $takim['av'] }}</td>
                        <td>{{ $takim['p'] }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div id=fixture class="col s12">
                <table class="fixture">
                    @foreach($futbol->getFikstur() as $date => $matches)
                        <tr class="date">
                            <td colspan="5">{{ $date }}</td>
                        </tr>
                        @foreach($matches as $match)
                            <tr class="result">
                                <td>{{ $match['home'] }}</td>
                                <td></td>
                                <td>{{ $match['date'] }}</td>
                                <td></td>
                                <td>{{ $match['away'] }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </table>
            </div>
        </div>
        <div class=widget-box>
            <h3 class=widget-title>
                <span>Ekonomi</span>
                <a href=/mobil/haberler/ekonomi class=index> <i class="fa fa-list-ul"></i> </a>
            </h3>
            @foreach($ekonomiHaberleri as $haber)
                <a href="{{ $haber->url() }}" class=small-listing-item>
                    <div class=entry-thumb>
                        <img class=lazy data-src="{{ Voyager::Image($haber->thumbnail('yorum')) }}"
                             alt="{{ $haber->title }}"></div>
                    <div class=entry-content>
                        <h2>{{ $haber->title }}</h2>
                    </div>
                </a>
            @endforeach
            <div class=clear></div>
        </div>
        <div class=widget-box>
            <h3 class=widget-title>
                <span>Fotoğraf Galerisi</span> <a href=/mobil/galeriler class=index> <i class="fa fa-list-ul"></i> </a>
            </h3>
            <div class="widget-item-slider owl-carousel">
                @foreach($fotoGaleriler as $galeri)
                    <a href={{ $galeri->url() }} class=items>
                        <div class=thumb>
                            <div class=icon-container>
                                <div class=icons><i class="fa fa-camera"></i></div>
                            </div>
                            <img class="owl-lazy image-center" data-src="{{ Voyager::Image($galeri->thumbnail('cropped', 'kapak')) }}" alt="{{ $galeri->name }}">
                        </div>
                        <div class=news-title>{{ $galeri->name }}</div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class=widget-box>
            <h3 class=widget-title><span>Video Galeri</span> <a href=/mobil/videolar class=index> <i class="fa fa-list-ul"></i> </a></h3>
            <div class="widget-item-slider owl-carousel">
                @foreach($videolar as $video)
                    <a href="{{ $video->url() }}" class=items>
                        <div class=thumb>
                            <div class=icon-container>
                                <div class=icons><i class="fa fa-play"></i></div>
                            </div>
                            <img class="owl-lazy image-center" data-src="{{ Voyager::Image($video->image) }}" alt="{{ $video->name }}">
                        </div>
                        <div class=news-title>{{ $video->name }}</div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@stop