@extends("mobil.template")

@section('content')
    <div class="content-container animated fadeInUp">
        <div class="entry-main">
            <h1 class="entry-title">
                {{ $kose->title }}
            </h1>
            <div class="entry-meta">
                <span class="date"> {{ date('d.m.Y H:i', strtotime($kose->created_at)) }} </span>
                <span class="separator">-</span>
                <span class="author">{{ $kose->yazar()->name }}</span>
            </div>
            <div class="entry-footer">
                <div class="social-share">
                    <a class="facebook" target="_blank" href="//www.facebook.com/sharer.php?u={{ $kose->url()  }}">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a class="twitter" target="_blank" href="//twitter.com/intent/tweet?url={{ $kose->url()  }}&amp;text={{ $kose->title  }}">
                        <i class="fab fa-twitter-square"></i>
                    </a>
                    <a class="gplus" target="_blank" href="//plus.google.com/share?url={{ $kose->url()  }}&amp;text={{ $kose->title  }}&#10;&amp;hl=tr">
                        <i class="fab fa-google-plus-square"></i>
                    </a>
                    <a class="whatsapp" href="whatsapp://send?text={{ $kose->title  }}&#10;: {{ $kose->url()  }}">
                        <i class="fab fa-whatsapp-square"></i>
                    </a>
                </div>
            </div>
            <div class="entry-content">
                {!! $kose->body !!}
            </div>
        </div>
        <div class="widget-box">
            <h3 class="widget-title"><span>Yazarın Diğer Yazıları</span></h3>
            @foreach($digerKoseYazilari as $kose)
                <a href="{{ $kose->url()  }}" class="small-listing-item">
                    <div class="entry-content">
                        <h2>{{ $kose->title }}</h2>
                    </div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
@stop