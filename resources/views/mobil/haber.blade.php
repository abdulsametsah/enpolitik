@extends("mobil.template")

@section('content')
    <div class="post-featured animated fadeInRight">
        <img src="{{ $haber->image_url()  }}" alt="{{ $haber->title  }}">
    </div>
    <div class="content-container animated fadeInUp">
        <div class="entry-main">
            <h1 class="entry-title">
                Bakan Dönmez'den Madenlerle İlgili Açıklama
            </h1>
            <div class="entry-meta">
                <span class="date"> {{ date('d.m.Y H:i', strtotime($haber->created_at)) }} </span>
            </div>
            <div class="entry-footer">
                <div class="social-share">
                    <a class="facebook" target="_blank" href="//www.facebook.com/sharer.php?u={{ $haber->url()  }}">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a class="twitter" target="_blank" href="//twitter.com/intent/tweet?url={{ $haber->url()  }}&amp;text={{ $haber->title  }}">
                        <i class="fab fa-twitter-square"></i>
                    </a>
                    <a class="gplus" target="_blank" href="//plus.google.com/share?url={{ $haber->url()  }}&amp;text={{ $haber->title  }}&#10;&amp;hl=tr">
                        <i class="fab fa-google-plus-square"></i>
                    </a>
                    <a class="whatsapp" href="whatsapp://send?text={{ $haber->title  }}&#10;: {{ $haber->url()  }}">
                        <i class="fab fa-whatsapp-square"></i>
                    </a>
                </div>
            </div>
            <div class="entry-content">
                {!! $haber->body !!}
                @if ($haber->iframe())
                        <br>
                    {!! $haber->iframe() !!}
                    <br>
                    <br>
                @endif
                @if ($haber->video)
                    <br>
                    <video src="{{ asset($haber->video) }}" width="100%" height=300px controls></video>
                    <br>
                    <br>
                @endif
            </div>
        </div>
        <div class="widget-box">
            @foreach($digerHaberler as $haber)
                <a href="{{ $haber->url()  }}" class="small-listing-item">
                    <div class="entry-thumb">
                        <img src="{{ Voyager::Image($haber->thumbnail('yorum')) }}" alt="{{ $haber->title }}”">
                    </div>
                    <div class="entry-content">
                        <h2>{{ $haber->title }}</h2>
                    </div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
@stop