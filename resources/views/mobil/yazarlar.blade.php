@extends("mobil.template")

@section("content")
    <div class="content-container animated fadeInUp">
        <h3 class="widget-title"><span>{{ $title }}</span></h3>
        <div class="koseyazarlari-list">
            @foreach($yazarlar as $kose)
            <a href="{{ $kose->url()  }}" class="content">
                <div class="image">
                    <img src="{{ Voyager::Image($kose->yazar()->avatar) }}">
                </div>
                <div class="title">{{ $kose->yazar()->name }}</div>
                <div class="article">
                    {{ $kose->title }}
                </div>
            </a>
            @endforeach
        </div>
        <div class="pagination">
            {{ $yazarlar->links()  }}
        </div>
    </div>
@stop