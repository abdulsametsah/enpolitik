@php
    $cokOkunanHaberler = \App\Haber::latest()->limit(5)->get();
    $cokYorumlananHaberler = \App\Haber::latest()->limit(5)->get();
@endphp

<!DOCTYPE html>
<html lang=tr>
    <head>
        <title>Haber, Son Dakika, Hava Durumu, Ekonomi, Teknoloji, Bilim, Sa&#x11F;l&#x131;k, K&#xFC;lt&#xFC;r Sanat, Politika, Spor, Gezi, T&#xFC;rkiye ve D&#xFC;nya Haberleri - Enpolitik.com</title>
        <meta charset=utf-8>
        <meta content="IE=edge" http-equiv=x-ua-compatible>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name=viewport>
        <meta content=yes name=apple-mobile-web-app-capable>
        <meta content=yes name=apple-touch-fullscreen>
        <link rel=stylesheet href="//fonts.googleapis.com/css?family=Hind:300,400,500,700">
        <link rel=stylesheet href=/css/site.mobile.min.css>
        <link rel="shortcut icon" href=/favicon.ico>
        <style>
            .page-item {
                padding: 5px 10px;
            }
            .pagination li {
                vertical-align: super !important;
            }
        </style>
    <body>
        <div id=main>
            <div id=slide-out-left class=side-nav>
                <div class=top-left-nav>
                    <form action=/haberler>
                        <div class=searchbar><input name=search placeholder="Arama Yapmak İçin Tıklayın"></div>
                    </form>
                </div>
                <ul class=collapsible data-collapsible=accordion>
                    @foreach (json_decode(menu('site', '_json')) as $menu)
                        <li>
                            <a href={{ url($menu->url) }}>{{ $menu->title }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div id=slide-out-right class=side-nav>
                <div class=sidebar-tabs>
                    <ul class=tabs>
                        <li class="tab col s3"><a class=active href=#popular>Çok Okunanlar</a>
                        <li class="tab col s3"><a href=#commented>Çok Yorumlanan</a>
                    </ul>
                    <div id=popular class="col s12">
                    	@foreach($cokOkunanHaberler as $haber)
                        <div class=items>
                            <div class=thumb>
                            	<img class="lazy image-center" data-src="{{ Voyager::Image($haber->thumbnail('yorum')) }}" 
                            	alt="{{ $haber->title }}">
                            </div>
                            <div class=content>
                                <h4>
                                	<a href="{{ $haber->url() }}">
	                                	{{ $haber->title }}
	                                </a>
	                            </h4>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div id=commented class="col s12">
                        @foreach($cokYorumlananHaberler as $haber)
                        <div class=items>
                            <div class=thumb>
                            	<img class="lazy image-center" data-src="{{ Voyager::Image($haber->thumbnail('yorum')) }}" 
                            	alt="{{ $haber->title }}">
                            </div>
                            <div class=content>
                                <h4>
                                	<a href="{{ $haber->url() }}">
	                                	{{ $haber->title }}
	                                </a>
	                            </h4>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div id=page>
                <div class=top-navbar>
                    <div class=top-navbar-left><a href=# id=menu-left data-activates=slide-out-left> <i class="fa fa-bars"></i> </a></div>
                    <div class=top-navbar-right><a href=# id=menu-right data-activates=slide-out-right> <i class="fa fa-th-large"></i> </a></div>
                    <div class=site-title>
                        <a href=/mobil>
                            <h1><img src=/images/logo.png></h1>
                        </a>
                    </div>
                </div>
                @yield("content")
                <div class=footer>
                    <div class=social-footer><a target=_blank href=//www.facebook.com/enpolitik class=facebook><i class="fab fa-facebook-f"></i></a> <a target=_blank href=//twitter.com/enpolitik class=twitter><i class="fab fa-twitter"></i></a></div>
                    <div class=navigation><a href=/mobil/yazarlar>Yazarlar</a> <a href=/mobil/galeriler>Galeriler</a> <a href=/mobil/videolar>Videolar</a> <a href=/mobil/sayfa/kunye>K&#xFC;nye</a> <a href=/mobil/sayfa/iletisim>&#x130;leti&#x15F;im</a></div>
                    <div class=copyright>&copy; 2019 Enpolitik.com</div>
                </div>
                <div id=to-top class=main-bg><i class="fas fa-angle-up"></i></div>
            </div>
        </div>
        <script async src="//www.googletagmanager.com/gtag/js?id=UA-72726744-1"></script><script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}
            gtag('js',new Date());gtag('config','UA-72726744-1');
        </script>
        <script src=/js/assets.mobile.min.js></script><script src=/js/site.mobile.min.js></script>
    </body>
</html>