@extends("mobil.template")

@section("content")
    <div class="post-featured-video animated fadeInRight">
        <video id=player controls src="{{ asset($video->video) }}"> Tarayıcınız Video Özelliğini Desteklemiyor </video>

        @if ($video->iframe())
            {!! $video->iframe() !!}
        @endif
    </div>
    <div class="content-container animated fadeInUp">
        <div class="entry-main">
            <h1 class="entry-title">{{ $video->name }}</h1>
            <div class="entry-meta"><span class="date"> {{ date('d.m.Y H:i', strtotime($video->created_at)) }} </span></div>
            <div class="entry-footer">
                <div class="social-share"><a class="facebook" target="_blank"
                                             href="//www.facebook.com/sharer.php?u={{ $video->url() }}">
                        <i class="fab fa-facebook-square"></i> </a> <a class="twitter" target="_blank"
                                                                       href="//twitter.com/intent/tweet?url={{ $video->url() }}&amp;text={{ $video->name }}">
                        <i class="fab fa-twitter-square"></i> </a> <a class="gplus" target="_blank"
                                                                      href="//plus.google.com/share?url={{ $video->url() }}&amp;text={{ $video->name }}&amp;hl=tr">
                        <i class="fab fa-google-plus-square"></i> </a> <a class="whatsapp"
                                                                          href="whatsapp://send?text={{ $video->name }}: {{ $video->url() }}">
                        <i class="fab fa-whatsapp-square"></i> </a></div>
            </div>
            <div class="entry-content">
                {!! $video->body !!}
            </div>
        </div>
        <div class="widget-box">
            @foreach($sonHaberler as $haber)
                <a href="{{ $haber->url()  }}" class="small-listing-item">
                    <div class="entry-thumb">
                        <img src="{{ Voyager::Image($haber->thumbnail('yorum')) }}" alt="{{ $haber->title }}”">
                    </div>
                    <div class="entry-content">
                        <h2>{{ $haber->title }}</h2>
                    </div>
                </a>
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
@stop
