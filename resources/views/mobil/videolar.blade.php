@extends("mobil.template")

@section("content")
    <div class="content-container animated fadeInUp"><h3 class="widget-title"><span>{{ $title }}</span></h3>
        <div class="widget-box">
            @foreach($videolar as $video)
            <a href="{{ $video->url()  }}"
                                   class="small-listing-item">
                <div class="entry-thumb"><img class="lazy"
                                              data-src="{{ Voyager::Image($video->thumbnail('kare')) }}"
                                              alt="{{ $video->name }}">
                </div>
                <div class="entry-content"><h2>{{ $video->name  }}</h2></div>
            </a>
            @endforeach
            <div class="clear"></div>
        </div>
        <div class="pagination">
            {{ $videolar->links()  }}
        </div>
    </div>
@stop