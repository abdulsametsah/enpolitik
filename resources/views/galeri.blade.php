@extends('layouts.app')

@section('contentTitle', $galeri->name)
@section('title', $galeri->name . " - ")
@section('aciklama', $galeri->summary)
@section('contentUrl', $galeri->url())
@section('image', Voyager::Image($galeri->kapak))

@section('content')
<div class="section-2">
    <div class="leftside">
        <div class="news">
            <h1 class="title">{{ $galeri->name }}</h1>
            <ol class="breadcrumb">
                <li><a href="/">Anasayfa</a></li>
                <li><a href="/galeriler/{{ $galeri->kategori()->slug }}">{{ $galeri->kategori()->name }}</a></li>
                <li class="active">{{ $galeri->name }}</li>
            </ol>
            <div class="image">
                <a href="?p={{ $p+2 > count($galeri->photos()) ? $p+1:$p+2 }}">
                    <img src="{{ Voyager::Image($foto->thumbnail('cropped', 'img')) }}">
                    <div class="counter">{{ $p+1 }} / {{ count($galeri->photos()) }}</div>
                </a>
            </div>
            <div class="nextimage"><img src="{{ Voyager::Image($foto->thumbnail('cropped', 'img')) }}"></div>
            <div class="navigation">
            	@if ($p+2 <= count($galeri->photos()))
                <div class="next"><a href="?p={{ $p+2 > count($galeri->photos()) ? $p+1:$p+2 }}"> Sonraki Fotoğraf <i class="fas fa-chevron-right"></i> </a></div>
                @endif
            	@if ($p > 0)
                <div class="prev"><a href="?p={{ $p }}"> <i class="fas fa-chevron-left"></i> Önceki Fotoğraf  </a></div>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="spacer-20"></div>
            <div class="text">{{ $foto->desc }}</div>
            <div class="spacer-20"></div>
            <div class="info">
                <div class="date">{{ date('d.m.Y H:i', strtotime($galeri->created_at)) }}</div>
                <div class="share"><span class="tab-right"> <a target="_blank" href="//www.facebook.com/sharer.php?u={{ $galeri->url() }}"> <i class="fab fa-facebook"></i> </a> </span> <span class="tab-right"> <a target="_blank" href="//twitter.com/intent/tweet?url={{ $galeri->url() }}&amp;text= PİSTTEN ÇIKAN UÇAK BÖYLE KURTARILDI"> <i class="fab fa-twitter"></i> </a> </span> <span class="tab-right"> <a target="_blank" href="//plus.google.com/share?url={{ $galeri->url() }}&amp;text= PİSTTEN ÇIKAN UÇAK BÖYLE KURTARILDI&amp;hl=tr"> <i class="fab fa-google-plus-g"></i> </a> </span> <span class="tab-right"> <a href="mailto:?body={{ $galeri->url() }}&amp;subject= PİSTTEN ÇIKAN UÇAK BÖYLE KURTARILDI"> <i class="fa fa-envelope"></i> </a> </span></div>
            </div>
            <div class="text" style="min-height: 400px; font-size: 16px;">
                <div class="buttons">
                    <div class="inner">
                        <div class="button font-minus"><i class="fas fa-font"></i> <i class="fas fa-minus"></i></div>
                        <div class="button font-reset"><i class="fas fa-font"></i></div>
                        <div class="button font-plus"><i class="fas fa-font"></i> <i class="fas fa-plus"></i></div>
                        <div class="button print"><i class="fas fa-print"></i></div>
                    </div>
                </div>
                <div class="advertisement">
                    <a href="//turdan.com.tr/" target="_blank"><img src="//enpolitik.com/haberler/2018/12/21/05eed2d7-f97f-491a-9211-a0785aa69f65.gif"></a> <br><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-1858947299980333" data-ad-slot="1189265304"></ins><script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
                </div>
                {!! $galeri->body !!}
                <div class="visible-print-block">{{ $galeri->url()  }}</div>
            </div>
        </div>
        <div class="spacer-20"></div>
        <div class="newcomment">
            <form method="post" class="recaptcha" action="" novalidate="novalidate">
                @csrf
                <input type="hidden" name="type" value="galeri">

                <div class="text-danger validation-summary-valid" data-valmsg-summary="true">
                    <ul>
                        @if (session('error'))
                            <li>{{ session('error') }}</li>
                        @endif
                    </ul>
                </div>
                <div class="text-success validation-summary-valid" data-valmsg-summary="true">
                    <ul>
                        @if (session('success'))
                            <li>{{ session('success') }}</li>
                        @endif
                    </ul>
                </div>
                <div class="row">
                    <div class="form-group col-xs-6">
                        <label for="Comment_Name">Adı Soyadı</label>*
                        <input class="form-control" data-val="true" data-val-required="Adı Soyadı Alanı Zorunludur"
                               id="Comment_Name" name="Comment.Name">
                    </div>
                    <div class="form-group col-xs-6">
                        <label for="Comment_Email">E-Posta</label>
                        <input class="form-control" type="email" data-val="true"
                               data-val-email="E-Posta Geçerli Değil" id="Comment_Email" name="Comment.Email">
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="Comment_Text">Yorum</label>*
                        <textarea class="form-control" data-val="true" data-val-required="Yorum Alanı Zorunludur"
                                  id="Comment_Text" name="Comment.Text"></textarea>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class="g-recaptcha" data-sitekey="{{ env('reCAPTCHA_KEY') }}"></div>
                    </div>
                    <div class="form-group col-xs-12">
                        <button class="btn btn-default">Gönder</button>
                    </div>
                </div>
            </form>
        </div>
        @if (count($galeri->comments()))
            <div class="spacer-20"></div>
            <h3 class="custom-title"><span>Yorumlar</span></h3>
            <div class="comments">
                @foreach($galeri->comments() as $comment)
                    <div class="content">
                        <div class="name">{{ $comment->isim }}</div>
                        <div class="date">{{ date('d.m.Y H:i', strtotime($comment->created_at)) }}</div>
                        <div class="clearfix"></div>
                        <div class="text">
                            {{ $comment->yorum }}
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        @include("layouts.sonHaberler")
    </div>
    @include("layouts.aside")
</div>
@stop