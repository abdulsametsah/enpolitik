@extends('layouts.app')

@section('contentTitle', $haber->title)
@section('title', $haber->title . " - ")
@section('aciklama', $haber->summary)
@section('contentUrl', $haber->url())
@section('image', Voyager::Image($haber->thumbnail('kare')))

@section('content')
    <div class="section-2">
        <div class="leftside">
            <div class="news">
                <h1 class="title">{{ $haber->title }}</h1>
                <div class="summary">
                    {{ $haber->summary }}
                </div>
                <div class="spacer-20"></div>
                <ol class="breadcrumb">
                    <li><a href="/">Anasayfa</a></li>
                    @if ($haber->kategori())
                    <li><a href="/haberler/{{ $haber->kategori()->slug }}">{{ $haber->kategori()->name }}</a></li>
                    @endif
                    <li class="active">{{ $haber->title }}</li>
                </ol>
                <div class="image"><img src="{{ $haber->image_url() }}"></div>
                <div class="spacer-20"></div>
                <div class="info">
                    <div class="date">
                        Eklenme Tarihi: {{ date('d.m.Y H:i:s', strtotime($haber->created_at)) }} -
                        Güncellenme Tarihi: {{ date('d.m.Y H:i:s', strtotime($haber->updated_at)) }}
                    </div>
                    <div class="share"><span class="tab-right"> <a target="_blank"
                                                                   href="//www.facebook.com/sharer.php?u={{ $haber->url() }}"> <i
                                        class="fab fa-facebook"></i> </a> </span> <span class="tab-right"> <a
                                    target="_blank"
                                    href="//twitter.com/intent/tweet?url={{ $haber->url() }}&amp;text=CHP'nin Ankara adayı Mansur Yavaş, mal varlığını açıkladı"> <i
                                        class="fab fa-twitter"></i> </a> </span> <span class="tab-right"> <a
                                    target="_blank"
                                    href="//plus.google.com/share?url={{ $haber->url() }}&amp;text=CHP'nin Ankara adayı Mansur Yavaş, mal varlığını açıkladı&amp;hl=tr"> <i
                                        class="fab fa-google-plus-g"></i> </a> </span> <span class="tab-right"> <a
                                    href="mailto:?body={{ $haber->url() }}&amp;subject=CHP'nin Ankara adayı Mansur Yavaş, mal varlığını açıkladı"> <i
                                        class="fa fa-envelope"></i> </a> </span></div>
                </div>
                <div class="spacer-20"></div>
                <div class="text" style="min-height: 400px">
                    <div class="buttons">
                        <div class="inner">
                            <div class="button font-minus"><i class="fas fa-font"></i> <i class="fas fa-minus"></i>
                            </div>
                            <div class="button font-reset"><i class="fas fa-font"></i></div>
                            <div class="button font-plus"><i class="fas fa-font"></i> <i class="fas fa-plus"></i></div>
                            <div class="button print"><i class="fas fa-print"></i></div>
                        </div>
                    </div>
                    <div class="advertisement">
                        <a href="//turdan.com.tr/" target="_blank"><img
                                    src="//enpolitik.com/haberler/2018/12/21/05eed2d7-f97f-491a-9211-a0785aa69f65.gif"></a>
                        <br>
                        <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px"
                             data-ad-client="ca-pub-1858947299980333" data-ad-slot="1189265304"></ins>
                        <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                    <p>
                        {!! $haber->body !!}
                    </p>

                    @if ($haber->iframe())
                        <br>
                        {!! $haber->iframe() !!}
                        <br>
                        <br>
                    @endif
                    @if ($haber->video)
                        <br>
                        <video src="{{ asset($haber->video) }}" width="100%" height=300px controls></video>
                        <br>
                        <br>
                    @endif
                    <div class="visible-print-block">{{ $haber->url() }}</div>
                </div>
                <div class="tags">
                    @foreach($haber->etiketler() as $etiket)
                        <a href="/etiket/{{ $etiket }}"><span class="tag">{{ $etiket }}</span></a>
                    @endforeach
                </div>
            </div>
            <div class="spacer-20"></div>
            <h3 class="custom-title">
                <span>Sizin Yorumunuz:</span>
            </h3>
            <div class="newcomment">
                <form method="post" class="recaptcha" action="" novalidate="novalidate">
                    @csrf
                    <input type="hidden" name="type" value="haber">

                    <div class="text-danger validation-summary-valid" data-valmsg-summary="true">
                        <ul>
                            @if (session('error'))
                                <li>{{ session('error') }}</li>
                            @endif
                        </ul>
                    </div>
                    <div class="text-success validation-summary-valid" data-valmsg-summary="true">
                        <ul>
                            @if (session('success'))
                                <li>{{ session('success') }}</li>
                            @endif
                        </ul>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="Comment_Name">Adı Soyadı</label>*
                            <input class="form-control" data-val="true" data-val-required="Adı Soyadı Alanı Zorunludur"
                                   id="Comment_Name" name="Comment.Name">
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="Comment_Email">E-Posta</label>
                            <input class="form-control" type="email" data-val="true"
                                   data-val-email="E-Posta Geçerli Değil" id="Comment_Email" name="Comment.Email">
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="Comment_Text">Yorum</label>*
                            <textarea class="form-control" data-val="true" data-val-required="Yorum Alanı Zorunludur"
                                      id="Comment_Text" name="Comment.Text"></textarea>
                        </div>
                        <div class="form-group col-xs-12">
                            <div class="g-recaptcha" data-sitekey="{{ env('reCAPTCHA_KEY') }}"></div>
                        </div>
                        <div class="form-group col-xs-12">
                            <button class="btn btn-default">Gönder</button>
                        </div>
                    </div>
                </form>
            </div>
            @if (count($haber->comments()))
            <div class="spacer-20"></div>
            <h3 class="custom-title"><span>Yorumlar</span></h3>
            <div class="comments">
                @foreach($haber->comments() as $comment)
                <div class="content">
                    <div class="name">{{ $comment->isim }}</div>
                    <div class="date">{{ date('d.m.Y H:i', strtotime($comment->created_at)) }}</div>
                    <div class="clearfix"></div>
                    <div class="text">
                        {{ $comment->yorum }}
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            @include("layouts.sonHaberler")
        </div>
        @include("layouts.aside")
    </div>
@stop

@section('js')
<script>
    $.getJSON('{{ url("/hit/" . $haber->id) }}', function(data){
        console.log(data);
    });
</script>
@stop