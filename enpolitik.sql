-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2019 at 03:47 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enpolitik`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-02-25 17:38:30', '2019-02-25 17:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `corner_posts`
--

CREATE TABLE `corner_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `etiketler` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"kare\",\"crop\":{\"width\":\"200\",\"height\":\"200\"}}]}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(57, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(58, 7, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'summary', 'text', 'Özet', 0, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'kategori_id', 'select_dropdown', 'Kategori', 0, 1, 1, 1, 1, 1, '{}', 6),
(61, 7, 'body', 'rich_text_box', 'İçerik', 0, 0, 1, 1, 1, 1, '{}', 7),
(62, 7, 'tags', 'text', 'Etiketler', 0, 0, 1, 1, 1, 1, '{\"description\":\"A helpful description text here for your future self.\"}', 8),
(63, 7, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 9),
(64, 7, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 10),
(65, 7, 'image', 'image', 'Fotoğraf', 0, 1, 1, 1, 1, 1, '{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"slider\",\"crop\":{\"width\":\"980\",\"height\":\"400\"}},{\"name\":\"yorum\",\"crop\":{\"width\":\"117\",\"height\":\"75\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"200\",\"height\":\"200\"}}]}', 11),
(67, 7, 'haber_belongsto_category_relationship', 'relationship', 'Kategori', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Kategory\",\"table\":\"kategories\",\"type\":\"belongsTo\",\"column\":\"kategori_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 1),
(68, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(69, 8, 'name', 'text', 'Kategori Adı', 0, 1, 1, 1, 1, 1, '{}', 3),
(70, 8, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 4),
(71, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(72, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(73, 8, 'sira', 'number', 'Sira', 0, 1, 1, 1, 1, 1, '{\"default\":1}', 1),
(74, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(75, 9, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 3),
(76, 9, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{\"slugify\":{\"origin\":\"title\"}}', 4),
(77, 9, 'body', 'rich_text_box', 'İçerik', 0, 1, 1, 1, 1, 1, '{}', 5),
(78, 9, 'etiketler', 'text', 'Etiketler', 0, 1, 1, 1, 1, 1, '{}', 6),
(79, 9, 'user_id', 'select_dropdown', 'Yazar', 0, 1, 1, 1, 1, 1, '{}', 7),
(80, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(81, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(82, 9, 'corner_post_belongsto_user_relationship', 'relationship', 'Yazar', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 1),
(83, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 6),
(84, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(85, 10, 'name', 'text', 'Galeri Adı', 0, 1, 1, 1, 1, 1, '{}', 3),
(86, 10, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{\"slugify\":{\"origin\":\"name\"}}', 4),
(87, 10, 'kapak', 'image', 'Kapak Fotoğrafı', 0, 1, 1, 1, 1, 1, '{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"800\",\"height\":\"450\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"300\",\"height\":\"300\"}}]}', 5),
(88, 10, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 6),
(89, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(90, 10, 'foto_galery_hasmany_photo_relationship', 'relationship', 'Fotoğraflar', 0, 0, 1, 0, 0, 1, '{\"model\":\"App\\\\Photo\",\"table\":\"photos\",\"type\":\"hasMany\",\"column\":\"foto_galeri_id\",\"key\":\"id\",\"label\":\"img\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(91, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 11, 'foto_galeri_id', 'text', 'Foto Galeri Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(93, 11, 'img', 'image', 'Foto', 0, 1, 1, 1, 1, 1, '{\"upsize\":true,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"800\",\"height\":\"450\"}}]}', 4),
(94, 11, 'desc', 'text', 'Açıklama', 0, 1, 1, 1, 1, 1, '{}', 5),
(95, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(96, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(97, 11, 'sira', 'number', 'Sira', 0, 1, 1, 1, 1, 1, '{}', 8),
(98, 11, 'photo_belongsto_foto_galery_relationship', 'relationship', 'Galeri', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\FotoGalery\",\"table\":\"foto_galeries\",\"type\":\"belongsTo\",\"column\":\"foto_galeri_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(99, 10, 'foto_galery_belongsto_kategory_relationship', 'relationship', 'Kategori', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Kategory\",\"table\":\"kategories\",\"type\":\"belongsTo\",\"column\":\"kategori_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(100, 10, 'kategori_id', 'text', 'Kategori Id', 0, 1, 1, 1, 1, 1, '{}', 9),
(101, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(102, 12, 'haber_id', 'text', 'Haber Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(103, 12, 'onaylandi', 'checkbox', 'Onaylandi', 0, 1, 1, 1, 1, 1, '{}', 4),
(104, 12, 'isim', 'text', 'Ad-Soyad', 0, 1, 1, 1, 1, 1, '{}', 5),
(105, 12, 'mail', 'text', 'E-Posta', 0, 0, 1, 1, 1, 1, '{}', 6),
(106, 12, 'yorum', 'text', 'Yorum', 0, 0, 1, 1, 1, 1, '{}', 7),
(107, 12, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 8),
(108, 12, 'updated_at', 'timestamp', 'Güncelleme Tarihi', 0, 0, 0, 0, 0, 0, '{}', 9),
(109, 12, 'yorum_belongsto_haber_relationship', 'relationship', 'Haber', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Haber\",\"table\":\"habers\",\"type\":\"belongsTo\",\"column\":\"haber_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(110, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(111, 13, 'kategori_id', 'select_dropdown', 'Kategori Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(112, 13, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(113, 13, 'body', 'rich_text_box', 'İçerik', 0, 0, 1, 1, 1, 1, '{}', 5),
(114, 13, 'image', 'image', 'Kapak Fotoğrafı', 0, 1, 1, 1, 1, 1, '{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"slider\",\"crop\":{\"width\":\"980\",\"height\":\"400\"}},{\"name\":\"yorum\",\"crop\":{\"width\":\"117\",\"height\":\"75\"}},{\"name\":\"kare\",\"crop\":{\"width\":\"200\",\"height\":\"200\"}}]}', 6),
(115, 13, 'video', 'file', 'Video', 0, 0, 1, 1, 1, 1, '{}', 7),
(116, 13, 'tags', 'text', 'Etiketler', 0, 0, 1, 1, 1, 1, '{}', 8),
(117, 13, 'created_at', 'timestamp', 'Tarih', 0, 1, 1, 1, 0, 1, '{}', 9),
(118, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(119, 13, 'video_belongsto_kategory_relationship', 'relationship', 'Kategori', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Kategory\",\"table\":\"kategories\",\"type\":\"belongsTo\",\"column\":\"kategori_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(120, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(121, 14, 'name', 'text', 'Gazete Adı', 0, 1, 1, 1, 1, 1, '{}', 2),
(122, 14, 'slug', 'text', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 3),
(123, 14, 'image', 'image', 'Manşet', 0, 1, 1, 1, 1, 1, '{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"kucuk\",\"crop\":{\"width\":\"100\",\"height\":\"152\"}}]}', 4),
(124, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 1, '{}', 5),
(125, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(126, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(127, 15, 'title', 'text', 'Başlık', 0, 1, 1, 1, 1, 1, '{}', 2),
(128, 15, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(129, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(130, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(131, 15, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(132, 7, 'sehir', 'text', 'Şehir', 0, 1, 1, 1, 1, 1, '{\"placeholder\":\"Yerel haber ise \\u015fehir yaz\\u0131n\\u0131z. De\\u011filse bo\\u015f b\\u0131rak\\u0131n\\u0131z\"}', 5),
(133, 7, 'editor', 'checkbox', 'Editorün Seçtiği?', 0, 1, 1, 1, 1, 1, '{}', 11),
(134, 10, 'body', 'rich_text_box', 'Haber İçeriği', 0, 1, 1, 1, 1, 1, '{}', 8),
(135, 7, 'hit', 'number', 'Okunma', 0, 1, 1, 1, 0, 1, '{}', 12),
(136, 7, 'yorum', 'number', 'Yorum Sayısı', 0, 0, 0, 0, 0, 1, '{}', 13);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}', '2019-02-25 17:38:29', '2019-02-26 21:37:59'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(7, 'habers', 'haberler', 'Haber', 'Haberler', NULL, 'App\\Haber', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"created_at\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":null}', '2019-02-25 18:18:43', '2019-03-05 00:57:39'),
(8, 'kategories', 'kategories', 'Kategori', 'Kategoriler', NULL, 'App\\Kategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-02-26 15:32:43', '2019-03-03 19:47:01'),
(9, 'corner_posts', 'corner-posts', 'Köşe Yazısı', 'Köşe Yazıları', NULL, 'App\\CornerPost', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-02-26 21:33:03', '2019-02-26 21:34:50'),
(10, 'foto_galeries', 'foto-galeries', 'Foto Galeri', 'Foto Galeriler', NULL, 'App\\FotoGalery', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-01 19:27:39', '2019-03-04 23:48:31'),
(11, 'photos', 'photos', 'Fotoğraf', 'Fotoğraflar', NULL, 'App\\Photo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-01 19:29:55', '2019-03-01 19:49:14'),
(12, 'yorums', 'yorums', 'Yorum', 'Yorumlar', NULL, 'App\\Yorum', NULL, NULL, 'Haberlere yapılan yorumlar. Buradan yorumları onaylayabilirsiniz.', 1, 0, '{\"order_column\":null,\"order_display_column\":\"onaylandi\",\"order_direction\":\"asc\",\"default_search_key\":\"yorum\"}', '2019-03-02 22:43:15', '2019-03-02 22:54:29'),
(13, 'videos', 'videos', 'Video', 'Videolar', NULL, 'App\\Video', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-03 14:45:43', '2019-03-03 14:50:58'),
(14, 'gazetes', 'gazetes', 'Gazete', 'Gazeteler', NULL, 'App\\Gazete', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-03 15:33:17', '2019-03-03 15:59:43'),
(15, 'sayfas', 'sayfas', 'Sayfa', 'Sayfalar', NULL, 'App\\Sayfa', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-03 19:40:21', '2019-03-03 19:46:36');

-- --------------------------------------------------------

--
-- Table structure for table `foto_galeries`
--

CREATE TABLE `foto_galeries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kapak` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gazetes`
--

CREATE TABLE `gazetes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gazetes`
--

INSERT INTO `gazetes` (`id`, `name`, `slug`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Türkiye', 'turkiye', 'gazetes\\March2019\\oPHyrkRheaNrPdR1hUtL.jpeg', '2019-03-03 16:07:30', '2019-03-03 16:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `habers`
--

CREATE TABLE `habers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `kategori_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `sehir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` tinyint(4) DEFAULT '0',
  `hit` int(11) DEFAULT NULL,
  `yorum` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategories`
--

CREATE TABLE `kategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sira` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategories`
--

INSERT INTO `kategories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `sira`) VALUES
(1, 'Dünya', 'dunya', '2019-02-26 15:33:00', '2019-02-26 20:58:31', 0),
(2, 'Spor', 'spor', '2019-02-26 15:33:00', '2019-02-26 20:58:23', 0),
(3, 'TV', 'tv', '2019-02-26 20:49:00', '2019-02-26 20:58:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(2, 'site', '2019-02-25 17:43:57', '2019-02-25 17:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 13, '2019-02-25 17:38:29', '2019-03-03 21:59:55', 'voyager.media.index', NULL),
(3, 1, 'Kullanıcılar', '', '_self', 'voyager-person', '#000000', NULL, 2, '2019-02-25 17:38:29', '2019-03-05 02:15:16', 'voyager.users.index', 'null'),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 12, '2019-02-25 17:38:29', '2019-03-05 02:15:06', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 15, '2019-02-25 17:38:29', '2019-03-03 21:59:55', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-02-25 17:38:29', '2019-03-03 21:56:51', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-02-25 17:38:29', '2019-03-03 21:56:51', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-02-25 17:38:29', '2019-03-03 21:56:51', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-02-25 17:38:29', '2019-03-03 21:56:51', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-02-25 17:38:29', '2019-03-03 21:59:55', 'voyager.settings.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-02-25 17:38:30', '2019-03-03 21:56:51', 'voyager.hooks', NULL),
(15, 2, 'ANASAYFA', '/', '_self', NULL, '#000000', NULL, 15, '2019-02-25 17:44:50', '2019-02-25 17:45:58', NULL, ''),
(16, 2, 'ROPÖRTAJ', '/haberler/roportaj', '_self', NULL, '#000000', NULL, 16, '2019-02-25 17:45:27', '2019-02-25 17:45:27', NULL, ''),
(17, 2, 'EKONOMİ', '/haberler/ekonomi', '_self', NULL, '#000000', NULL, 17, '2019-02-25 17:45:49', '2019-02-25 17:45:49', NULL, ''),
(18, 2, 'POLİTİKA', '/haberler/politika', '_self', NULL, '#000000', NULL, 18, '2019-02-25 17:46:18', '2019-02-25 17:46:18', NULL, ''),
(19, 2, 'SPOR', '/haberler/spor', '_self', NULL, '#000000', NULL, 19, '2019-02-25 17:46:31', '2019-02-25 17:46:31', NULL, ''),
(20, 2, 'MAGAZİN', '/haberler/magazin', '_self', NULL, '#000000', NULL, 20, '2019-02-25 17:46:42', '2019-02-25 17:46:42', NULL, ''),
(21, 2, 'EĞİTİM', '/haberler/egitim', '_self', NULL, '#000000', NULL, 21, '2019-02-25 17:46:55', '2019-02-25 17:46:55', NULL, ''),
(22, 2, 'DÜNYA', '/haberler/dunya', '_self', NULL, '#000000', NULL, 22, '2019-02-25 17:47:10', '2019-02-25 17:47:10', NULL, ''),
(23, 2, 'YAZARLAR', 'yazarlar', '_self', NULL, '#000000', NULL, 23, '2019-02-25 17:47:31', '2019-02-25 17:47:31', NULL, ''),
(24, 2, 'GALERİLER', 'galeriler', '_self', NULL, '#000000', NULL, 24, '2019-02-25 17:47:43', '2019-02-25 17:47:43', NULL, ''),
(25, 2, 'VİDEOLAR', '/videolar', '_self', NULL, '#000000', NULL, 25, '2019-02-25 17:47:52', '2019-02-25 17:47:52', NULL, ''),
(26, 1, 'Haberler', '', '_self', 'voyager-news', '#000000', NULL, 3, '2019-02-25 18:18:43', '2019-03-05 02:15:11', 'voyager.haberler.index', 'null'),
(27, 1, 'Kategoriler', '', '_self', 'voyager-list', '#000000', NULL, 4, '2019-02-26 15:32:43', '2019-03-05 02:15:11', 'voyager.kategories.index', 'null'),
(28, 1, 'Köşe Yazıları', '', '_self', 'voyager-file-text', '#000000', NULL, 6, '2019-02-26 21:33:03', '2019-03-05 02:15:11', 'voyager.corner-posts.index', 'null'),
(29, 1, 'Foto Galeriler', '', '_self', 'voyager-photos', '#000000', NULL, 7, '2019-03-01 19:27:39', '2019-03-05 02:15:11', 'voyager.foto-galeries.index', 'null'),
(30, 1, 'Fotoğraflar', '', '_self', 'voyager-photo', '#000000', NULL, 8, '2019-03-01 19:29:56', '2019-03-05 02:15:06', 'voyager.photos.index', 'null'),
(31, 1, 'Yorumlar', '', '_self', 'voyager-bubble', '#000000', NULL, 5, '2019-03-02 22:43:15', '2019-03-05 02:15:11', 'voyager.yorums.index', 'null'),
(32, 1, 'Videolar', '', '_self', 'voyager-video', '#000000', NULL, 10, '2019-03-03 14:45:43', '2019-03-05 02:15:06', 'voyager.videos.index', 'null'),
(33, 1, 'Gazeteler', '', '_self', 'voyager-dot-3', '#000000', NULL, 9, '2019-03-03 15:33:17', '2019-03-05 02:15:06', 'voyager.gazetes.index', 'null'),
(34, 1, 'Sayfalar', '', '_self', 'voyager-folder', '#000000', NULL, 11, '2019-03-03 19:40:21', '2019-03-05 02:15:06', 'voyager.sayfas.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(27, '2019_03_05_051311_create_categories_table', 0),
(28, '2019_03_05_051311_create_corner_posts_table', 0),
(29, '2019_03_05_051311_create_data_rows_table', 0),
(30, '2019_03_05_051311_create_data_types_table', 0),
(31, '2019_03_05_051311_create_foto_galeries_table', 0),
(32, '2019_03_05_051311_create_gazetes_table', 0),
(33, '2019_03_05_051311_create_habers_table', 0),
(34, '2019_03_05_051311_create_kategories_table', 0),
(35, '2019_03_05_051311_create_menu_items_table', 0),
(36, '2019_03_05_051311_create_menus_table', 0),
(37, '2019_03_05_051311_create_pages_table', 0),
(38, '2019_03_05_051311_create_password_resets_table', 0),
(39, '2019_03_05_051311_create_permission_role_table', 0),
(40, '2019_03_05_051311_create_permissions_table', 0),
(41, '2019_03_05_051311_create_photos_table', 0),
(42, '2019_03_05_051311_create_posts_table', 0),
(43, '2019_03_05_051311_create_roles_table', 0),
(44, '2019_03_05_051311_create_sayfas_table', 0),
(45, '2019_03_05_051311_create_settings_table', 0),
(46, '2019_03_05_051311_create_translations_table', 0),
(47, '2019_03_05_051311_create_user_roles_table', 0),
(48, '2019_03_05_051311_create_users_table', 0),
(49, '2019_03_05_051311_create_videos_table', 0),
(50, '2019_03_05_051311_create_yorums_table', 0),
(51, '2019_03_05_051313_add_foreign_keys_to_categories_table', 0),
(52, '2019_03_05_051313_add_foreign_keys_to_data_rows_table', 0),
(53, '2019_03_05_051313_add_foreign_keys_to_menu_items_table', 0),
(54, '2019_03_05_051313_add_foreign_keys_to_permission_role_table', 0),
(55, '2019_03_05_051313_add_foreign_keys_to_user_roles_table', 0),
(56, '2019_03_05_051313_add_foreign_keys_to_users_table', 0),
(57, '2019_03_05_054007_create_categories_table', 0),
(58, '2019_03_05_054007_create_corner_posts_table', 0),
(59, '2019_03_05_054007_create_data_rows_table', 0),
(60, '2019_03_05_054007_create_data_types_table', 0),
(61, '2019_03_05_054007_create_foto_galeries_table', 0),
(62, '2019_03_05_054007_create_gazetes_table', 0),
(63, '2019_03_05_054007_create_habers_table', 0),
(64, '2019_03_05_054007_create_kategories_table', 0),
(65, '2019_03_05_054007_create_menu_items_table', 0),
(66, '2019_03_05_054007_create_menus_table', 0),
(67, '2019_03_05_054007_create_pages_table', 0),
(68, '2019_03_05_054007_create_password_resets_table', 0),
(69, '2019_03_05_054007_create_permission_role_table', 0),
(70, '2019_03_05_054007_create_permissions_table', 0),
(71, '2019_03_05_054007_create_photos_table', 0),
(72, '2019_03_05_054007_create_posts_table', 0),
(73, '2019_03_05_054007_create_roles_table', 0),
(74, '2019_03_05_054007_create_sayfas_table', 0),
(75, '2019_03_05_054007_create_settings_table', 0),
(76, '2019_03_05_054007_create_translations_table', 0),
(77, '2019_03_05_054007_create_user_roles_table', 0),
(78, '2019_03_05_054007_create_users_table', 0),
(79, '2019_03_05_054007_create_videos_table', 0),
(80, '2019_03_05_054007_create_yorums_table', 0),
(81, '2019_03_05_054008_add_foreign_keys_to_categories_table', 0),
(82, '2019_03_05_054008_add_foreign_keys_to_data_rows_table', 0),
(83, '2019_03_05_054008_add_foreign_keys_to_menu_items_table', 0),
(84, '2019_03_05_054008_add_foreign_keys_to_permission_role_table', 0),
(85, '2019_03_05_054008_add_foreign_keys_to_user_roles_table', 0),
(86, '2019_03_05_054008_add_foreign_keys_to_users_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-02-25 17:38:30', '2019-02-25 17:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(2, 'browse_bread', NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(3, 'browse_database', NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(4, 'browse_media', NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(5, 'browse_compass', NULL, '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(6, 'browse_menus', 'menus', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(7, 'read_menus', 'menus', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(8, 'edit_menus', 'menus', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(9, 'add_menus', 'menus', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(10, 'delete_menus', 'menus', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(11, 'browse_roles', 'roles', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(12, 'read_roles', 'roles', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(13, 'edit_roles', 'roles', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(14, 'add_roles', 'roles', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(15, 'delete_roles', 'roles', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(16, 'browse_users', 'users', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(17, 'read_users', 'users', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(18, 'edit_users', 'users', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(19, 'add_users', 'users', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(20, 'delete_users', 'users', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(21, 'browse_settings', 'settings', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(22, 'read_settings', 'settings', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(23, 'edit_settings', 'settings', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(24, 'add_settings', 'settings', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(25, 'delete_settings', 'settings', '2019-02-25 17:38:29', '2019-02-25 17:38:29'),
(26, 'browse_categories', 'categories', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(27, 'read_categories', 'categories', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(28, 'edit_categories', 'categories', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(29, 'add_categories', 'categories', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(30, 'delete_categories', 'categories', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(31, 'browse_posts', 'posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(32, 'read_posts', 'posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(33, 'edit_posts', 'posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(34, 'add_posts', 'posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(35, 'delete_posts', 'posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(36, 'browse_pages', 'pages', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(37, 'read_pages', 'pages', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(38, 'edit_pages', 'pages', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(39, 'add_pages', 'pages', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(40, 'delete_pages', 'pages', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(41, 'browse_hooks', NULL, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(42, 'browse_habers', 'habers', '2019-02-25 18:18:43', '2019-02-25 18:18:43'),
(43, 'read_habers', 'habers', '2019-02-25 18:18:43', '2019-02-25 18:18:43'),
(44, 'edit_habers', 'habers', '2019-02-25 18:18:43', '2019-02-25 18:18:43'),
(45, 'add_habers', 'habers', '2019-02-25 18:18:43', '2019-02-25 18:18:43'),
(46, 'delete_habers', 'habers', '2019-02-25 18:18:43', '2019-02-25 18:18:43'),
(47, 'browse_kategories', 'kategories', '2019-02-26 15:32:43', '2019-02-26 15:32:43'),
(48, 'read_kategories', 'kategories', '2019-02-26 15:32:43', '2019-02-26 15:32:43'),
(49, 'edit_kategories', 'kategories', '2019-02-26 15:32:43', '2019-02-26 15:32:43'),
(50, 'add_kategories', 'kategories', '2019-02-26 15:32:43', '2019-02-26 15:32:43'),
(51, 'delete_kategories', 'kategories', '2019-02-26 15:32:43', '2019-02-26 15:32:43'),
(52, 'browse_corner_posts', 'corner_posts', '2019-02-26 21:33:03', '2019-02-26 21:33:03'),
(53, 'read_corner_posts', 'corner_posts', '2019-02-26 21:33:03', '2019-02-26 21:33:03'),
(54, 'edit_corner_posts', 'corner_posts', '2019-02-26 21:33:03', '2019-02-26 21:33:03'),
(55, 'add_corner_posts', 'corner_posts', '2019-02-26 21:33:03', '2019-02-26 21:33:03'),
(56, 'delete_corner_posts', 'corner_posts', '2019-02-26 21:33:03', '2019-02-26 21:33:03'),
(57, 'browse_foto_galeries', 'foto_galeries', '2019-03-01 19:27:39', '2019-03-01 19:27:39'),
(58, 'read_foto_galeries', 'foto_galeries', '2019-03-01 19:27:39', '2019-03-01 19:27:39'),
(59, 'edit_foto_galeries', 'foto_galeries', '2019-03-01 19:27:39', '2019-03-01 19:27:39'),
(60, 'add_foto_galeries', 'foto_galeries', '2019-03-01 19:27:39', '2019-03-01 19:27:39'),
(61, 'delete_foto_galeries', 'foto_galeries', '2019-03-01 19:27:39', '2019-03-01 19:27:39'),
(62, 'browse_photos', 'photos', '2019-03-01 19:29:56', '2019-03-01 19:29:56'),
(63, 'read_photos', 'photos', '2019-03-01 19:29:56', '2019-03-01 19:29:56'),
(64, 'edit_photos', 'photos', '2019-03-01 19:29:56', '2019-03-01 19:29:56'),
(65, 'add_photos', 'photos', '2019-03-01 19:29:56', '2019-03-01 19:29:56'),
(66, 'delete_photos', 'photos', '2019-03-01 19:29:56', '2019-03-01 19:29:56'),
(67, 'browse_yorums', 'yorums', '2019-03-02 22:43:15', '2019-03-02 22:43:15'),
(68, 'read_yorums', 'yorums', '2019-03-02 22:43:15', '2019-03-02 22:43:15'),
(69, 'edit_yorums', 'yorums', '2019-03-02 22:43:15', '2019-03-02 22:43:15'),
(70, 'add_yorums', 'yorums', '2019-03-02 22:43:15', '2019-03-02 22:43:15'),
(71, 'delete_yorums', 'yorums', '2019-03-02 22:43:15', '2019-03-02 22:43:15'),
(72, 'browse_videos', 'videos', '2019-03-03 14:45:43', '2019-03-03 14:45:43'),
(73, 'read_videos', 'videos', '2019-03-03 14:45:43', '2019-03-03 14:45:43'),
(74, 'edit_videos', 'videos', '2019-03-03 14:45:43', '2019-03-03 14:45:43'),
(75, 'add_videos', 'videos', '2019-03-03 14:45:43', '2019-03-03 14:45:43'),
(76, 'delete_videos', 'videos', '2019-03-03 14:45:43', '2019-03-03 14:45:43'),
(77, 'browse_gazetes', 'gazetes', '2019-03-03 15:33:17', '2019-03-03 15:33:17'),
(78, 'read_gazetes', 'gazetes', '2019-03-03 15:33:17', '2019-03-03 15:33:17'),
(79, 'edit_gazetes', 'gazetes', '2019-03-03 15:33:17', '2019-03-03 15:33:17'),
(80, 'add_gazetes', 'gazetes', '2019-03-03 15:33:17', '2019-03-03 15:33:17'),
(81, 'delete_gazetes', 'gazetes', '2019-03-03 15:33:17', '2019-03-03 15:33:17'),
(82, 'browse_sayfas', 'sayfas', '2019-03-03 19:40:21', '2019-03-03 19:40:21'),
(83, 'read_sayfas', 'sayfas', '2019-03-03 19:40:21', '2019-03-03 19:40:21'),
(84, 'edit_sayfas', 'sayfas', '2019-03-03 19:40:21', '2019-03-03 19:40:21'),
(85, 'add_sayfas', 'sayfas', '2019-03-03 19:40:21', '2019-03-03 19:40:21'),
(86, 'delete_sayfas', 'sayfas', '2019-03-03 19:40:21', '2019-03-03 19:40:21');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(56, 3),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1);

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `foto_galeri_id` int(11) DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sira` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-02-25 17:38:30', '2019-02-25 17:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Yönetici', '2019-02-25 17:38:29', '2019-02-26 21:26:44'),
(2, 'user', 'Normal Kullanıcı (Okur)', '2019-02-25 17:38:29', '2019-02-26 21:26:58'),
(3, 'yazar', 'Yazar', '2019-02-26 21:26:33', '2019-02-26 21:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `sayfas`
--

CREATE TABLE `sayfas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sayfas`
--

INSERT INTO `sayfas` (`id`, `title`, `body`, `created_at`, `updated_at`, `slug`) VALUES
(1, 'Deneme sayfa', '<p>asdkjnalsdmalskdmlasmkdlasd</p>', '2019-03-03 19:44:00', '2019-03-03 19:47:19', 'deneme-sayfa');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'ENPOLİTİK', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'ENPOLİTİK', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\February2019\\vjo6iykgCwhkSJtm3dEC.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'ENPOLİTİK', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'ENPOLİTİK', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\February2019\\jsMHDyDjGZ6yAibQqVAj.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.karikatur', 'Günün Karikatürü', 'settings\\March2019\\CV6dmERhDzOkdyPySv3g.jpg', NULL, 'image', 6, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-02-25 17:38:30', '2019-02-25 17:38:30'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-02-25 17:38:30', '2019-02-25 17:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users\\February2019\\gxQGqEauL37HyDF21hRs.jpeg', NULL, '$2y$10$xR.dq9lkibrza4E84.WMP.QBTikqnl8FrhK5z10Plc8wFbo4gLrUu', 'sBE0UABXRAx5NbCl6awy9z2umHkC3rLpgXFm4iJl5UEcCqFYfhu8azsoc3Vn', '{\"locale\":\"tr\"}', '2019-02-25 17:38:30', '2019-02-27 10:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `video` text COLLATE utf8mb4_unicode_ci,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `yorums`
--

CREATE TABLE `yorums` (
  `id` int(10) UNSIGNED NOT NULL,
  `haber_id` int(11) DEFAULT NULL,
  `onaylandi` int(11) DEFAULT '0',
  `isim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yorum` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `corner_posts`
--
ALTER TABLE `corner_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `foto_galeries`
--
ALTER TABLE `foto_galeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gazetes`
--
ALTER TABLE `gazetes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `habers`
--
ALTER TABLE `habers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategories`
--
ALTER TABLE `kategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sayfas`
--
ALTER TABLE `sayfas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yorums`
--
ALTER TABLE `yorums`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `corner_posts`
--
ALTER TABLE `corner_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `foto_galeries`
--
ALTER TABLE `foto_galeries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gazetes`
--
ALTER TABLE `gazetes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `habers`
--
ALTER TABLE `habers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategories`
--
ALTER TABLE `kategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sayfas`
--
ALTER TABLE `sayfas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `yorums`
--
ALTER TABLE `yorums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
