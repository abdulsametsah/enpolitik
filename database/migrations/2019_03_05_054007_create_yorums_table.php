<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateYorumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('yorums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('haber_id')->nullable();
			$table->integer('onaylandi')->nullable()->default(0);
			$table->string('isim', 191)->nullable();
			$table->string('mail', 191)->nullable();
			$table->text('yorum', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('yorums');
	}

}
