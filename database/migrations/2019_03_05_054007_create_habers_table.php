<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHabersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('habers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 191)->nullable();
			$table->text('summary', 65535)->nullable();
			$table->integer('kategori_id')->nullable();
			$table->text('body')->nullable();
			$table->text('tags', 65535)->nullable();
			$table->timestamps();
			$table->text('image', 65535)->nullable();
			$table->string('sehir', 191)->nullable();
			$table->boolean('editor')->nullable()->default(0);
			$table->integer('hit')->nullable();
			$table->integer('yorum')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('habers');
	}

}
