<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// API
Route::get('api/CategoryNews', 'ODataController@categoryHaber');
Route::get('api/Manset1', 'ODataController@manset');
Route::get('api/Manset2', 'ODataController@manset');
Route::get('api/Manset3', 'ODataController@manset');
Route::get('api/Manset4', 'ODataController@manset');
Route::get('api/Manset5', 'ODataController@manset');
Route::get('api/Leagues', 'ODataController@Leagues');
Route::get('api/LeagueStage', 'ODataController@LeagueStage');
Route::get('api/LeagueStage', 'ODataController@LeagueStage');
Route::get('api/MostRead', 'ODataController@MostRead');
Route::get('api/MostCommented', 'ODataController@MostCommented');
Route::get('api/Weather', 'ODataController@Weather');
Route::get('api/Articles', 'ODataController@Articles');
Route::get('api/Provinces', 'ODataController@Provinces');
Route::get('api/Categories', 'ODataController@Categories');
// ## API

// ODATA
Route::get('odata/{modelName}', 'ODataController@getModel');
// ## ODATA

Route::post('yeni-haber-ekle', 'HaberController@create');
Route::post('yeni-haber-guncelle', 'HaberController@update');
Route::get('delete-video/{haber}', 'HaberController@deleteVideo');

Route::get('/', 'PagesController@home');
Route::get('/clearCache', 'PagesController@updateHome');

Route::get('/hit/{haber}', 'PagesController@updateHit');

Route::get('/etiket/{etiket}', 'PagesController@etiket');

Route::get('/yazarlar', 'PagesController@yazarlar');
Route::get('/kose-yazisi/{id}/{slug}', 'PagesController@koseYazisi');
Route::post('/kose-yazisi/{id}/{slug}', 'YorumController@store');

Route::get('/galeriler', 'PagesController@galeriler');
Route::get('/galeri/{id}/{slug}', 'PagesController@galeri');
Route::post('/galeri/{id}/{slug}', 'YorumController@store');


Route::get('/gazeteler', 'PagesController@gazeteler');
Route::get('/gazete/{slug}', 'PagesController@gazete');

Route::get('/videolar', 'PagesController@videolar');
Route::get('/videolar/{kategori}', 'PagesController@videolar');
Route::get('/video/{id}/{slug}', 'PagesController@video');
Route::post('/video/{id}/{slug}', 'YorumController@store');

Route::get('/tum-haberler', 'PagesController@haberler');
Route::get('/haberler', 'PagesController@haberler');
Route::get('/haberler/{kategori}', 'PagesController@haberler');
Route::get('/haber/{id}/{slug}', 'PagesController@haber');
Route::post('/haber/{id}/{slug}', 'YorumController@store');

Route::get('/sayfa/{slug}', 'PagesController@sayfa');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/odata/{data}', 'WeatherController@data');

Route::get('/admin/users', function(){
    return redirect(url('admin/users2'));
})->name('admin.users.index');

Route::post('/yeni-video-ekle', 'VideoController@store');
Route::post('/yeni-video-guncelle', 'VideoController@update');

//Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(["prefix" => "mobil"], function () {

    Route::get('/', 'PagesController@home');

    Route::get('/etiket/{etiket}', 'PagesController@etiket');

    Route::get('/yazarlar', 'PagesController@yazarlar');
    Route::get('/kose-yazisi/{id}/{slug}', 'PagesController@koseYazisi');
    Route::post('/kose-yazisi/{id}/{slug}', 'YorumController@store');

    Route::get('/galeriler', 'PagesController@galeriler');
    Route::get('/galeri/{id}/{slug}', 'PagesController@galeri');
    Route::post('/galeri/{id}/{slug}', 'YorumController@store');


    Route::get('/gazeteler', 'PagesController@gazeteler');
    Route::get('/gazete/{slug}', 'PagesController@gazete');

    Route::get('/videolar', 'PagesController@videolar');
    Route::get('/videolar/{kategori}', 'PagesController@videolar');
    Route::get('/video/{id}/{slug}', 'PagesController@video');
    Route::post('/video/{id}/{slug}', 'YorumController@store');

    Route::get('/haberler', 'PagesController@haberler');
    Route::get('/haberler/{kategori}', 'PagesController@haberler');
    Route::get('/haber/{id}/{slug}', 'PagesController@haber');
    Route::post('/haber/{id}/{slug}', 'YorumController@store');

    Route::get('/sayfa/{slug}', 'PagesController@sayfa');
    Route::get('/home', 'HomeController@index')->name('home');
});
